<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllowedEmails extends Model
{
    //
    protected $fillable = ['email'];
}
