<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class BookChapters extends Model
{
    use Notifiable;
    
    /**
        * The attributes that are mass assignable.
        *
        * @var array
        */
    protected $fillable = [
        'title', 'book_id', 'sort'
    ];

    public function sections_all()
    {
        return $this->hasMany('App\BookSections','chapters_id');
    }

    public function sections()
    {
        return $this->sections_all()->where('status',0)->orderBy('sort');
    }
    
    protected static function boot() {
        parent::boot();

        static::deleting(function($model) { // before delete() method call this
            $ids = $model->sections_all()->pluck('id')->all();
		    BookSections::destroy($ids);
        });
    }
}
