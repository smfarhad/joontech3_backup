<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class BookSections extends Model
{
    use Notifiable;

    /**
        * The attributes that are mass assignable.
        *
        * @var array
        */
    protected $fillable = [
        'title', 'chapter_id', 'sort'
    ];
    
    public function photos()
    {
        return $this->has_many('Photo');
    }

    public function subSections_all()
    {
        return $this->hasMany('App\BookSubsections','sections_id');
    }

    public function subSections()
    {
        return $this->subSections_all()->where('status',0)->orderBy('sort');
    }

    public function chapter()
    {
        return $this->belongsTo('App\BookChapters','chapters_id');
    }

    public function book(){
        return $this->belongsTo('App\Books','books_id');
    }
    
    protected static function boot() {
        parent::boot();

        static::deleting(function($model) { // before delete() method call this
            $ids = $model->subSections_all()->pluck('id')->all();
		    BookSubsections::destroy($ids);
        });
    }
}
