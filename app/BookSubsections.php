<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Auth;

class BookSubsections extends Model
{
    use Notifiable;

    /**
        * The attributes that are mass assignable.
        *
        * @var array
        */
    protected $fillable = [
        'title', 'section_id', 'data', 'sort', 'created_by', 'updated_by'
    ];

    public function topics_all(){
        return $this->hasMany('App\Topics','subsections_id');
    }

    public function topics(){
        return $this->topics_all()->where('status',0)->orderBy('created_at','DESC');
    }

    public function section(){
        return $this->hasOne('App\BookSections','id','sections_id');
    }

    public function book(){
        return $this->hasOne('App\Books','id','books_id');
    }

    public function forum_all()
    {
        return $this->hasMany('App\ForumCategories','subsection_id');
    }

    public static function boot()
    {
        parent::boot();

        static::updating(function ($model) {
            $model->updated_by = Auth::User()->id;
        });

        static::creating(function ($model) {
            $model->created_by = Auth::user()->id;
        });

        static::deleting(function($model) { // before delete() method call this
            $ids = $model->forum_all()->pluck('id')->all();
            ForumCategories::destroy($ids);
        });
    }
}
