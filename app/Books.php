<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Auth;

class Books extends Model
{
    use Notifiable;
    use SearchableTrait;
    /**
        * The attributes that are mass assignable.
        *
        * @var array
        */
    protected $fillable = [
        'title'
    ];

    protected $searchable = [
        'columns' => [
            'books.title' => 1,
            'book_chapters.title' => 2,
            'book_sections.title' => 5,
            'book_subsections.title' => 10,
            'book_subsections.data' => 10,
        ],
        'joins' => [
            'book_chapters' => ['books.id','book_chapters.books_id'],
            'book_sections' => ['book_chapters.id','book_sections.chapters_id'],
            'book_subsections' => ['book_sections.id','book_subsections.sections_id'],
        ],
    ];

    
    public function chapters_all()
    {
        return $this->hasMany('App\BookChapters');
    }

    public function chapters()
    {
        return $this->chapters_all()->where('status',0)->orderBy('sort');;
    }

    public function chapter($slug)
    {
        return $this->chapters()->where(['status'=>0,'slug'=>$slug])->first();
    }

    public static function boot()
    {
        parent::boot();

        static::updating(function ($model) {
            $model->updated_by = Auth::User()->id;
        });

        static::creating(function ($model) {
            $model->created_by = Auth::user()->id;
        });
        
        static::deleting(function($model) { // before delete() method call this
            $ids = $model->chapters_all()->pluck('id')->all();
		    BookChapters::destroy($ids);
        });
    }
}
