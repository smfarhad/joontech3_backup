<?php namespace App;

use App\Books;
use App\BookChapters;
use App\BookSections;
use App\BookSubsections;
use Riari\Forum\Models\Category;

class CategoryExtension extends Category
{
    public function getChildrenAttribute() {
        if($this->slug == "books"){
            $books = Books::all();
            return $this->prepareCollectionsForView($books, "chapters");
        } else if($this->slug == "chapters"){
            $chapters = BookChapters::where("books_id", $this->id)->get();
            return $this->prepareCollectionsForView($chapters, "sections");
        } else if($this->slug == "sections"){
            $sections = BookSections::where("chapters_id", $this->id)->get();
            return $this->prepareCollectionsForView($sections, "subsections");
        } else if($this->slug == "subsections"){
            $subSections = BookSubsections::where("sections_id", $this->id)->get();
            return $this->prepareCollectionsForView($subSections, "forums");
        } else if($this->slug == "forums"){
            $forums = ForumCategories::where("subsection_id", $this->id)->get();
            return $this->prepareCollectionsForView($forums, "forum");
        } else {
            return parent::getChildrenAttribute();
        }
    }

    private function prepareCollectionsForView($collections, $childSlug) {
        $i = 0;
        foreach ($collections as $key => $value) {
            $collections[] = $this->getNewCategoryExtension($value, $childSlug);
            unset($collections[$i]);
            $i++;
        }
        return $collections;
    }

    private function getNewCategoryExtension($value, $slug) {
        $category = new CategoryExtension();
        $category->id = $value->id;
        $category->title = $value->title;
        $category->slug = $slug;
        $category->is_public = 1;
        $category->is_deleted = 0;
        return $category;
    }

    public function getThreadsPaginatedAttribute(){

    }
}
