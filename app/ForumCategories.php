<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Riari\Forum\Models\Category;
use Riari\Forum\Models\Thread;

class ForumCategories extends Model
{
    //
    protected $fillable = [
        'category_id',
        'title',
        'description',
        'image',
        'extra_class',
        'is_book',
        'is_public',
        'is_forum',
        'enable_threads'
    ];

    public function chapters()
    {
        return $this->hasMany(Category::class, 'category_id', 'id');
    }

    public function subcategories()
    {
        return $this->hasMany(Category::class, 'category_id');
    }

    public function categoryParent(){
        return $this->belongsTo('App\ForumCategories', 'category_id');
    }

    public function categoryChildren(){
        return $this->hasMany('App\ForumCategories', 'category_id', 'id');
    }

    public function threads()
    {
        return $this->hasMany(Thread::class, 'category_id');
    }

    public static function boot()
    {
        static::deleting(function($model) { // before delete() method call this
            $ids = $model->threads()->pluck('id')->all();
		    Thread::destroy($ids);
        });
    }
}
