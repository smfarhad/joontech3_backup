<?php

namespace App;

use Riari\Forum\Models\Thread;

class HelperClass
{
    public static function createSlug($str, $class, $before = '')
    {
        $delimiter = '-';
        $counter = 0;

        while (true) {
            $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
            $ext = ($counter == 0) ? '' : '-' . $counter;
            $slug = $before . $slug . $ext;

            if ($class == '') break; // return slug directly with out checking in database

            if ($class::where('slug', $slug)->first() == null) break; // return slug directly with out checking in database
            $counter += 1;
        }
        return $slug;
    }


    public static function createBooksMenu($categories, $menu='')
    {
        $menu .= '';
        foreach($categories as $key => $category) {
            if(isset($category['title'])){
                $button = '';
                $menu .= '<li id="'.$category['id'].'" class="folder expanded '.$category['extra_class'].'">' . $category['title'] . $button ;
            }
            if(isset($category['sub_categories']) && !empty($category['sub_categories'])) {
                $menu .= '<ul>';
                $menu .= HelperClass::createBooksMenu($category['sub_categories']);
                $menu .= '</ul>';
            }

            if(!is_array($category)) {
                $menu .= '</li>' ;
            }
        }

        return $menu;
    }

    public function createBooksViewMenu($bookHierarchyArray, $menu="")
    {
        if(is_array($bookHierarchyArray)){
            foreach ($bookHierarchyArray as $value) {
                $url = "";
                if($value->extra_class != "tree-chapter"){
                    $url = url('admin/book/subsection-content/' . $value->element_id ."_".$value->extra_class);
                }

                //create list item
                $menu .= '<li data-link="' .$url  . '" id="'.$value->element_id.'" class="folder expanded '
                            .$value->extra_class.'">' . $value->title ;
                $subMenu = "";
                //if children available create children under <ul>
                if(isset($value->children)){
                    //exclude forum as toc does not contain forum
                    if(!$this->isChildForum($value->children)){
                        $subMenu = '<ul>';
                        $subMenu .= $this->createBooksViewMenu($value->children, $subMenu);
                        $subMenu .= '</ul>';
                    }
                }

                //add the submenus to main list
                $menu .= $subMenu;
                $menu .= '</li>' ;
            }
        }
        // foreach($categories as $key => $category) {
        //     if(isset($category['title']) ){
        //         $url = '';
        //         if($category['extra_class'] == 'tree-section' || $category['extra_class'] == 'tree-subSection' ) {
        //             $url = url('admin/book/subsection-content/' . $category['id']);
        //         }
        //         $menu .= '<li data-link="' .$url  . '" id="'.$category['id'].'" class="folder expanded '.$category['extra_class'].'">' . $category['title'] ;
        //     }
        //
        //     if(isset($category['sub_categories']) && !empty($category['sub_categories'])) {
        //         $menu .= '<ul>';
        //         $menu .= HelperClass::createBooksViewMenu($category['sub_categories']);
        //         $menu .= '</ul>';
        //     }
        //
        //     if(!is_array($category)) {
        //         $menu .= '</li>' ;
        //     }
        // }

        //replacing double <ul> wih single <ul>
        $menu = str_replace("<ul><ul>", "<ul>", $menu);
        return $menu;
    }

    private function isChildForum($array){
        $forum = false;
        foreach ($array as $valueChild) {
            if($valueChild->extra_class == "tree-forum"){
                $forum = true;
                break;
            }
        }

        return $forum;
    }

    public function createTOCMenu($bookHierarchyArray, $bookSlug='', $menu = '')
    {
        if(is_array($bookHierarchyArray)){
            foreach ($bookHierarchyArray as $value) {
                $button = '';
                if($value->extra_class == 'tree-subSection') {
                $menu .= '<li id="'.$value->element_id.'" class="folder expanded '.$value->extra_class.'">' .
                    '<a href="' . url('/book').'/'.$bookSlug.'?subSection='.$value->slug.'">' . $value->title . $button . '</a>';
                } else {
                    $menu .= '<li id="'.$value->element_id.'" class="folder expanded '.$value->extra_class.'">' .
                        '<a href="#">' . $value->title . $button . '</a>';
                }

                $subMenu = "";
                //if children available create children under <ul>
                if(isset($value->children)){
                    //exclude forum as toc does not contain forum
                    if(!$this->isChildForum($value->children)){
                        $subMenu = '<ul>';
                        $subMenu .= $this->createTOCMenu($value->children, $bookSlug, $subMenu);
                        $subMenu .= '</ul>';
                    }
                }

                //add the submenus to main list
                $menu .= $subMenu;
                $menu .= '</li>' ;
            }
        }

        $menu = str_replace("<ul><ul>", "<ul>", $menu);
        // echo $menu;
        return $menu;
        //dd($categories);
        // $menu .= '';
        // foreach($categories as $key => $category) { //echo '<pre>'; print_r($category); echo '</pre>';
        //     if(isset($category['title'])){
        //         $button = '';
        //         /*if($category['extra_class'] == 'tree-subSection') {
        //             $button = '<span data-subsection-id="'.$category['id'].'" class="text-right btn btn-success btn-xs btn-forum" style="float:right;">Forum</span>';
        //         }*/
        //
        //         if($category['extra_class'] == 'tree-subSection') {
        //         $menu .= '<li id="'.$category['id'].'" class="folder expanded '.$category['extra_class'].'">' .
        //             '<a href="' . url('/book').'/'.$bookSlug.'?subSection='.$category['slug'].'">' . $category['title'] . $button . '</a>';
        //         } else {
        //             $menu .= '<li id="'.$category['id'].'" class="folder expanded '.$category['extra_class'].'">' .
        //                 '<a href="#">' . $category['title'] . $button . '</a>';
        //         }
        //     }
        //
        //     if(isset($category['sub_categories']) && !empty($category['sub_categories'])) {
        //         $menu .= '<ul>';
        //         $menu .= HelperClass::createTOCMenu($category['sub_categories'], $bookSlug);
        //         $menu .= '</ul>';
        //     }
        //
        //     if(!is_array($category)) {
        //         $menu .= '</li>' ;
        //     }
        // }
        //
        // return $menu;
    }

    public static function createTOCContent($categories, $bookSlug='', $menu = '')
    {
        //dd($categories);
        $menu .= '';
        foreach($categories as $key => $category) { //echo '<pre>'; print_r($category); echo '</pre>';
            if(isset($category['title'])){
                $button = '';
                $menu .= '<li id="'.$category['id'].'" class="folder expanded '.$category['extra_class'].'">
                <h3>' . $category['title'] . $button . '</h3>';
                $menu .= '<div class="description">'.$category['description'];

                if($category['extra_class'] == 'tree-subSection') {
                    $menu .= ' <a class="btn-forum" href="#" data-subsection-id="'.$category['id'].'">' . HelperClass::getCommentsCount($category['id']) . ' Comments</a>';
                }

                $menu .= '</div>';
            }

            if(isset($category['sub_categories']) && !empty($category['sub_categories'])) {
                $menu .= '<ul class="sub-section">';
                $menu .= HelperClass::createTOCContent($category['sub_categories'], $bookSlug);
                $menu .= '</ul>';
            }

            //if(!is_array($category)) {
                $menu .= '</li>' ;
            //}
        }

        return $menu;
    }


    public static function getCommentsCount($categoryId)
    {
        $comments = [];
        $category = ForumCategories::where("subsection_id", $categoryId)->get();
        if($category) {
            foreach($category as $forum) {
                $threads = $forum->threads;
                if($threads) {
                    foreach($threads as $thread) {
                        $comments[] = count($thread->posts);
                    }
                }
            }
        }

        $comments = array_sum($comments);
        // var_dump($comments);
        return isset($comments) && !empty($comments) ? $comments : 0 ;
    }

    public static function deleteCategoryRecursively($categories, $menu='')
    {
        //dd($categories);
        $menu .= '';
        foreach($categories as $key => $category) {
            if(isset($category['id'])){
                $cat = ForumCategories::find($category['id']);
                $cat->delete();
            }
            if(isset($category['sub_categories']) && !empty($category['sub_categories'])) {
                HelperClass::deleteCategoryRecursively($category['sub_categories']);
            }

            if(!is_array($category)) {
                //echo 'category: '. $category . '<br>';
            }
        }

        return $menu;
    }

}
