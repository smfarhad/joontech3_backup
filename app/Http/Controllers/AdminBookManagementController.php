<?php

namespace App\Http\Controllers;

use App\ForumCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Books;
use App\BookChapters;
use App\BookSections;
use App\BookSubsections;
use App\HelperClass;
use Illuminate\Support\Facades\DB;
use Riari\Forum\Models\Thread;

class AdminBookManagementController extends Controller
{
    public function bookList()
    {
        $books = Books::paginate(6);
        $userThreads = Thread::where('author_id', Auth::id())->orderBy('id', 'DESC')->paginate(1);

        $pageTitle = 'Books List';
        return view('admin.books-list', compact('books', 'userThreads', 'pageTitle'));
    }

    public function books()
    {
        $pageTitle = 'Books';
        return view('admin.books', compact('pageTitle'));
    }

    public function booksListDataTables(Request $request)
    {
        $books = Books::all();
        return datatables()->of($books)->toJson();
    }

    public function bookView($id)
    {
        $book = Books::find($id);
        $categoryTree = [];
        //get the book hierarchy tree
        if ($book) {
            $categoryTree = $this->booksTreeView($book->id);
        }

        //create menu based on book hierarchy
        $helperClass = new HelperClass();
        $treeMenu = $helperClass -> createBooksViewMenu(json_decode($categoryTree));
        $pageTitle = 'Book - View';
        return view('admin.book-view', compact('book', 'pageTitle', 'treeMenu'));
    }

    public function bookAdd()
    {
        $pageTitle = 'Add BooK';
        return view('admin.book-add', compact('pageTitle'));
    }

    public function bookEdit($id)
    {
        $book = Books::find($id);
        $bookTree = [];
        if ($book) {
        	$bookTree = $this->booksTreeView($book->id);
        }

        $pageTitle = 'Book - Edit';
        return view('admin.book-edit', compact('book', 'bookTree', 'pageTitle'));
    }

    public function update(Request $request)
    {
        $book = (array)json_decode($request['data']);
        $book = json_decode(json_encode($book), true);

        //validating file extension.
        $imageName = '';
        $bookCategory = ForumCategories::find($book['key']);

        if ($request->file != 'undefined') {
            $this->validate($request, [
                'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            //uploading book image.
            $image = $request->file;
            $imageName = time() . '-' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $imageName);

            //updating image.
            $bookCategory->image = $imageName;
        }

        $bookCategory->title = $book['title'];
        $bookCategory->slug = HelperClass::createSlug($book['title'], $bookCategory);
        $bookCategory->save();

        //creating subcategories recursively.
        $this->updateSubCategories($book, $bookCategory->id);
        return response()->json(['success' => 'Book created successfully']);
    }

    public function updateSubCategories($forumCategories, $parent_id)
    {
        if (isset($forumCategories['children'])) {
            foreach ($forumCategories['children'] as $children) {
                $subCat = ForumCategories::where('id', $children['key'])->first();

                if (!$subCat) {
                    $subCat = new ForumCategories();
                    $subCat->title = $children['title'];
                    $subCat->slug = HelperClass::createSlug($children['title'], $subCat);
                    $subCat->category_id = $parent_id;
                    $subCat->is_public = 1;
                    $subCat->extra_class = $children['extraClasses'];

                    if ($children['key'] == 'forum') {
                        $subCat->is_forum = 1;
                        $subCat->enable_threads = 1;
                    } else {
                        $subCat->is_forum = 0;
                        $subCat->enable_threads = 0;
                    }

                } else {
                    $subCat->title = $children['title'];
                    $subCat->slug = HelperClass::createSlug($children['title'], '');
                    $subCat->is_public = 1;
                    $subCat->extra_class = $children['extraClasses'];
                }

                $subCat->save();
                if (isset($children['children'])) {
                    $this->updateSubCategories($children, $subCat->id);
                }
            }
        }
    }

    public function doRemove($book, $all = false)
    {
        $book_id = $book['key'];
        if ($all === true) {
            $this->removeAll($book_id);
        } else {
            // Check Chapter if has removed key
            // Remove from chapter to subsection
            $chapter_check = collect($book['children'])->pluck('key');
            $chapter_update = BookChapters::where('books_id', $book_id)->whereNotIn('id', $chapter_check)->update(['status' => 1]);
            if ($chapter_update > 0) {
                // removing section
                $chapters = BookChapters::where('books_id', $book_id)->whereNotIn('id', $chapter_check)->get();
                foreach ($chapters as $chapter) {
                    $section_update = BookSections::where('chapters_id', $chapter->id)->update(['status' => 1]);
                    if ($section_update > 0) {
                        // removing subsection
                        $sections = BookSections::where('chapters_id', $chapter->id)->get();
                        $this->removeAllSubsections($sections);
                    }
                }
            }
            // Check Section if has removed key
            // Remove from Section to subsection
            if (isset($book['children'])) {
                foreach ($book['children'] as $chapters_2) {
                    $section_check = collect($chapters_2['children'])->pluck('key');
                    $section_update = BookSections::where('chapters_id', $chapters_2['key'])->whereNotIn('id', $section_check)->update(['status' => 1]);
                    if ($section_update > 0) {
                        $sections = BookSections::where('chapters_id', $chapters_2['key'])->whereNotIn('id', $section_check)->get();
                        $this->removeAllSubsections($sections);
                    }

                    //Check Subsection if has removed key
                    // Remove subsection
                    if (isset($chapters_2['children'])) {
                        foreach ($chapters_2['children'] as $sub_sections) {
                            $sub_childs = isset($sub_sections['children']) ? $sub_sections['children'] : [];
                            $subsection_check = collect($sub_childs)->pluck('key');
                            $subsection_update = BookSubsections::where('sections_id', $sub_sections['key'])->whereNotIn('id', $subsection_check)->update(['status' => 1]);
                        }
                    }
                }
            }
        }
    }

    public function removeAllSubsections($sections)
    {
        foreach ($sections as $section) {
            BookSubsections::where('sections_id', $section->id)->update(['status' => 1]);
        }
    }

    public function removeAll($id)
    {
        BookChapters::where('books_id', $id)->update(['status' => 1]);
        BookSections::where('books_id', $id)->update(['status' => 1]);
        BookSubsections::where('books_id', $id)->update(['status' => 1]);
    }

    public function updateBook(Request $request)
    {
        $bookJson = json_decode($request['data']);

		$book = Books::find($bookJson->key);
		$book->title = $bookJson->title;
		$book->slug = HelperClass::createSlug($bookJson->title, $book);

		if ($request->file != 'undefined') {
            $this->validate($request, [
                'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            //uploading book image.
            $image = $request->file;
            $imageName = time() . '-' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $imageName);

            //updating image.
            $book->image = $imageName;
        }

        DB::beginTransaction();
        try {
            $book->save();
            if(isset($bookJson->children)){
		        foreach($bookJson->children as $key=>$childrenChapter){
		        	$chapter = null;
		        	if(isset($childrenChapter->data)){
						$chapter = BookChapters::find($childrenChapter->data->element_id);
					} else {
						$chapter = new BookChapters();
					}

		        	$chapter->title = $childrenChapter->title;
		        	$chapter->books_id = $book->id;
				    $chapter->sort = $key + 1;
				    $chapter->slug = HelperClass::createSlug($childrenChapter->title, $chapter);
				    $chapter->save();

				    if(isset($childrenChapter->children)){
						foreach($childrenChapter->children as $key=>$childrenSection){
							$section = null;
							if(isset($childrenSection->data)){
								$section = BookSections::find($childrenSection->data->element_id);
							} else {
								$section = new BookSections();
							}

							$section->title = $childrenSection->title;
							$section->chapters_id = $chapter->id;
				        	$section->books_id = $book->id;
							$section->sort = $key + 1;
							$section->slug = HelperClass::createSlug($childrenSection->title, $section);
							$section->save();

							if(isset($childrenSection->children)){
								foreach($childrenSection->children as $key=>$childrenSubSection){
									$subSection = null;
									if(isset($childrenSubSection->data)){
										$subSection = BookSubsections::find($childrenSubSection->data->element_id);
									} else {
										$subSection = new BookSubsections();
									}

									$subSection->title = $childrenSubSection->title;
									$subSection->sections_id = $section->id;
									$subSection->books_id = $book->id;
									$subSection->sort = $key + 1;
									$subSection->slug = HelperClass::createSlug($childrenSubSection->title, $subSection);
									$subSection->save();

                                    if(isset($childrenSubSection->children)){
        								foreach($childrenSubSection->children as $key=>$childrenForum){
        									$forum = null;
        									if(isset($childrenForum->data)){
        										$forum = ForumCategories::find($childrenForum->data->element_id);
        									} else {
        										$forum = new ForumCategories();
        									}

        									$forum->title = $childrenForum->title;
        									$forum->subsection_id = $subSection->id;
                                            $forum->category_id = -1;
                                            $forum->is_public = 1;
                                            $forum->is_forum = 1;
                                            $forum->enable_threads = 1;
        									$forum->slug = HelperClass::createSlug($childrenForum->title, $forum);
        									$forum->save();
        								}
        							}
								}
							}
						}
				    }

		        }
		        DB::commit();
		        return $this->booksTreeView($book->id);
            }
        } catch (\Exception $e) {
            DB::rollback();
//            return response()->json(['errors' => 'Book creation failed']);
        }
    }

    public function updateChapter($id, $data, $key)
    {
        $chapter = BookChapters::find($data['key']);
        if ($chapter == '') {
            $chapter = new BookChapters;
        }
        $chapter->title = isset($data['title']) ? $data['title'] : '';
        $chapter->books_id = $id;
        $chapter->sort = $key;
        $chapter->slug = HelperClass::createSlug($data['title'], $chapter);
        if ($chapter->save()) {
            return $chapter;
            return false;
        }
    }

    public function updateSection($id, $data, $key, $books_id)
    {
        $section = BookSections::find($data['key']);
        if ($section == '') {
            $section = new BookSections;
            $section->books_id = $books_id;
        }
        $section->title = isset($data['title']) ? $data['title'] : '';
        $section->chapters_id = $id;
        $section->sort = $key;
        $section->slug = HelperClass::createSlug($data['title'], $section);
        if ($section->save()) {
            return $section;
            return false;
        }
    }

    public function updateSubsection($id, $data, $key, $books_id)
    {
        $subsection = BookSubsections::find($data['key']);
        if ($subsection == '') {
            $subsection = new BookSubsections;
            $subsection->books_id = $books_id;
            $subsection->created_by = Auth::id();
        }
        $subsection->title = isset($data['title']) ? $data['title'] : '';
        $subsection->sections_id = $id;
        $subsection->sort = $key;
        $subsection->slug = HelperClass::createSlug($data['title'], $subsection);
        $subsection->updated_by = Auth::id();
        if ($subsection->save()) {
            return $subsection;
            return false;
        }
    }

    public function create(Request $request)
    {
        $bookJson = json_decode($request['data'], true);
        $this->validate($request, [
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $image = $request->file;
        $imageName = time() . '-' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $imageName);

        DB::beginTransaction();
        try {
            $this->saveBook($bookJson, $imageName);
            DB::commit();
            return response()->json(['success' => 'Book created successfully']);
        } catch (\Exception $e) {
            DB::rollback();
//            return response()->json(['errors' => 'Book creation failed']);
        }
    }

    public function saveSubCategories($forumCategories, $parent_id)
    {
        if (isset($forumCategories['children'])) {
            foreach ($forumCategories['children'] as $children) {
                $subCat = new ForumCategories();
                $subCat->title = $children['title'];
                $subCat->slug = HelperClass::createSlug($children['title'], $subCat);
                $subCat->category_id = $parent_id;
                $subCat->is_public = 1;
                $subCat->extra_class = $children['extraClasses'];

                if ($children['key'] == 'forum') {
                    $subCat->is_forum = 1;
                    $subCat->enable_threads = 1;
                } else {
                    $subCat->is_forum = 0;
                    $subCat->enable_threads = 0;
                }
                $subCat->save();

                /**
                 * if subSection dont have a forum then it will be created automatically.
                 **/

                if ($children['extraClasses'] == 'tree-subSection') {
                    if (!isset($children['children'])) {
                        $catForum = new ForumCategories();
                        $catForum->title = 'General Forum';
                        $catForum->slug = HelperClass::createSlug('General Forum', $catForum);
                        $catForum->category_id = $subCat->id;

                        $catForum->extra_class = 'tree-forum';
                        $catForum->is_public = 1;
                        $catForum->is_forum = 1;
                        $catForum->enable_threads = 1;
                        $catForum->save();
                    }
                }

                if (isset($children['children'])) {
                    $this->saveSubCategories($children, $subCat->id);
                }
            }
        }
    }


    public function delete(Request $request)
    {
        if (Auth::user()->isAdmin()) {
            $book = Books::find($request->id);
            if ($book == null) {
                return redirect()->route('pageNotFound');
            } else {
                $book->status = 1;
                $book->save();

                $request->session()->flash('book-delete', 'Remove successful');
                return redirect()->route('bookList');
            }
        } else {
            return redirect()->route()->route('forbiddenError');
        }
    }

    public function deleteBook(Request $request)
    {
        $book = Books::find($request->id);
        DB::beginTransaction();
        try {
            $book->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    public function deleteNode(Request $request)
    {
    	$model = null;
    	if($request->extra_class == "tree-chapter"){
    		$model = BookChapters::find($request->id);
    	} else if($request->extra_class == "tree-section"){
    		$model = BookSections::find($request->id);
    	} else if($request->extra_class == "tree-subSection"){
    		$model = BookSubsections::find($request->id);
    	} else if($request->extra_class == "tree-forum"){
            $model = ForumCategories::find($request->id);
        }

    	DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            return response()->json(['success' => 'Book created successfully']);
        } catch (\Exception $e) {
            echo($e->getMessage());
            DB::rollback();
        }
    }

    private function saveBook($data, $bookImage)
    {
        $bookModel = new Books();
        $bookModel->title = $data['title'];
        $bookModel->slug = HelperClass::createSlug('Books', $bookModel);
        $bookModel->image = $bookImage;
        $bookModel->status = 1;
        $bookModel->save();
        $this->saveChapter($bookModel->id, $data['children']);
    }

    public function saveChapter($bookId, $data)
    {
        foreach ($data as $key => $value) {
            $chapter = new BookChapters();
            $chapter->title = $value['title'];
            $chapter->books_id = $bookId;
            $chapter->sort = $key + 1;
            $chapter->status = 1;
            $chapter->slug = HelperClass::createSlug($value['title'], $chapter);
            $chapter->save();
            $this->saveSection($bookId, $chapter->id, $value['children']);
        }
    }

    private function saveSection($bookId, $chapterId, $data)
    {
        foreach ($data as $key => $value) {
            $section = new BookSections();
            $section->title = $value['title'];
            $section->books_id = $bookId;
            $section->chapters_id = $chapterId;
            $section->sort = $key + 1;
            $section->status = 1;
            $section->slug = HelperClass::createSlug($value['title'], $section);
            $section->save();
            $this->saveSubsection($bookId, $section->id, $value['children']);
        }
    }

    private function saveSubsection($bookId, $sectionId, $data)
    {
        foreach ($data as $key => $value) {
            $subsection = new BookSubsections();
            $subsection->title = $value['title'];
            $subsection->books_id = $bookId;
            $subsection->sections_id = $sectionId;
            $subsection->sort = $key + 1;
            $subsection->status = 1;
            $subsection->slug = HelperClass::createSlug($value['title'], $subsection);
            $subsection->save();
            $this->saveForum($subsection->id, $value['children']);
        }
    }

    private function saveForum($subsectionId, $data)
    {
        foreach ($data as $key => $value) {
            $forum= new ForumCategories();
            $forum->title = $value['title'];
            $forum->subsection_id = $subsectionId;
            $forum->category_id = -1;
            $forum->slug = HelperClass::createSlug($value['title'], $forum);
            $forum->is_public = 1;
            $forum->is_forum = 1;
            $forum->enable_threads = 1;
            $forum->save();
        }
    }

    public function subsectionContent(Request $request)
    {
        $pageTitle = 'SubSection Description';

        //separate id and class
        $idAndClass = explode("_", $request->id);

        $subSections = null;
        $section = -1;

        //get subsection data from section or subsection
        if($idAndClass[1] == "tree-section"){
            $section = $idAndClass[0];
            $subSections = BookSubsections::where("sections_id", $section)->orderBy("sort")->get();
        } else {
            $subSection = BookSubsections::find($idAndClass[0]);
            $section = $subSection->sections_id;
            $subSections = BookSubsections::where('sections_id', $section)->orderBy("sort")->get();
        }

        //get book id
        $parentBookId = $subSections->first()->books_id;

        //populate subsection id array
        $subsectionIds = [];
        foreach ($subSections as $key => $value) {
            $subsectionIds[] = $value->id;
        }
        $subsectionIds = json_encode($subsectionIds);

        return view('admin.subsection', compact('pageTitle', 'section', 'subSections', 'subsectionIds', 'parentBookId'));
    }

    public function saveSubSectionContent(Request $request)
    {
        foreach ($request['editor'] as $key => $editor) {

            //get form data
            $data = array_values($editor)[0];
            $title = array_values($request['title'][$key])[0];
            $key_id = key($editor);
            $subsection = null;

            DB::beginTransaction();
            try {
                //if new subsection
                if ($key_id == 'new') {
                    // $maxSort = DB::table('book_subsections')->max('sort');
                    $subsection = new BookSubsections();
                    $subsection->sort = $key + 1;
                    $subsection->status = 1;
                //subsection already exists.. fetch from db
                } else {
                    $subsection = BookSubsections::find($key_id);
                }

                $subsection->title = $title;
                $subsection->sections_id = $request['section_id'];
                $subsection->books_id = $request['book_id'];
                $subsection->data = $data;
                $subsection->slug = HelperClass::createSlug($title, $subsection);
                $subsection->save();
                DB::commit();
            } catch (\Exception $e) {
                // dd($e->getMessage());
                DB::rollback();
            }
        }
        return redirect('admin/book-view/' . $request['book_id']);
    }

}
