<?php

namespace App\Http\Controllers;

use App\ForumCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\BookSubsections;
use App\BookSections;

class AdminSubsectionController extends Controller
{
    public function index($section){
        $section = ForumCategories::find($section);
        //dd($section);
        return view('admin.subsection',compact('section'));
    }

    public function action(Request $request){
        $title = $request['title'];
        $section = BookSections::find($request['subsection_id']);
        foreach ($request['editor'] as $key => $editor) {
            $data = array_values($editor)[0];
            $sub_title = array_values($title[$key])[0];
            $key_id = key($editor);

            if( $key_id == 'new' ){
                $subsection = new BookSubsections;
            }else{
                $subsection = BookSubsections::find($key_id);
            }

            $subsection->title = $sub_title;
            $subsection->books_id = $section->book->id;
            $subsection->sections_id = $section->id;
            $subsection->data = $data;
            $subsection->sort = $key;
            $subsection->created_by = Auth::id();
            $subsection->updated_by = Auth::id();
            $subsection->save();
        }
        return redirect('admin/book/subsection/'.$section->id);
    }
}
