<?php
    
    namespace App\Http\Controllers;
    
    use Illuminate\Http\Request;
    use App\User;
    use App\UserLevels;
    use App\Rules\Password;
    
    class AdminUserController extends Controller
    {
        public function index()
        {
            $pageTitle = 'Users List';
            return view('admin.user-list', compact('pageTitle'));
        }
        
        public function activeUsersDataTable()
        {
            return datatables()->of(User::ActiveUsers())->toJson();
        }
        
        public function showUserInfoFields(Request $request)
        {
            $user = User::find($request->id);
            $user_levels = UserLevels::where('id', '>', 2)->get();
            $user_level_dropdown = dropDown($user_levels, $user->level);
            return view('admin.user-info-fields', compact('user', 'user_level_dropdown'));
        }
        
        public function update(Request $request)
        {
            
            $validatedData = $request->validate([
                'first_name' => 'required|max:35',
                'last_name' => 'required|max:35',
                'user_level' => 'required',
                'password' => new Password
            ]);
            
            $user_id = $request->user_id;
            
            if ($this->doUpdate($validatedData, $user_id)) {
                return response()->json(['success' => 'Update successful']);
            } else {
                return response()->json(['error' => 'Something went wrong.']);
            }
        }
        
        public function doUpdate($data, $id)
        {
            $user = User::find($id);
            if ($user == null) {
                return false;
            }
            
            $user->first_name = $data['first_name'];
            $user->last_name = $data['last_name'];
            $user->level = $data['user_level'];
            
            if ($data['password'] != '') {
                $user->password = bcrypt($data['password']);
            }
            
            return $user->save();
        }
        
        public function enableUser(Request $request)
        {
            if ($this->doEnable($request->id)) {
                return response()->json(['success', 'User Enabled Successfully']);
            } else {
                return response()->json(['success', 'Something went wrong, Please try again.'], 500);
            }
            
        }
        
        public function delete(Request $request)
        {
            if ($this->doDelete($request->id)) {
                return response()->json(['success', 'User successfuly deleted.']);
            } else {
                return response()->json('Something went wrong, Please try again.', 500);
            }
        }
        
        public function doDelete($id)
        {
            $user = User::find($id);
            if ($user == null) {
                return false;
            }
            
            $user->status = 1;
            return $user->save();
        }
        
        public function doEnable($id)
        {
            $user = User::find($id);
            if ($user == null) {
                return false;
            }
            
            $user->status = 0;
            return $user->save();
        }
    }
