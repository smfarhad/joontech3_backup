<?php

namespace App\Http\Controllers;

use App\AllowedEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AllowedEmailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Allowed Emails';
        return view('admin.allowed-emails.index', compact('pageTitle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.allowed-emails.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:allowed_emails,email|min:4'
        ]);

        AllowedEmails::create($request->all());
        Session::flash('allow-email-success', 'Email pattern created successfully!');
        return redirect()
            ->route('listAllowedEmails');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if($allowedEmails = AllowedEmails::find($request->id)) {
            $allowedEmails->delete();
            return response()->json(['success', 'Email pattern deleted successfully']);
        } else {
            return response()->json(['Something went wrong, Please try again', 500]);
        }
        return response()->json();
    }

    public function allowedEmailsDataTable(Request $request)
    {
        $allowedEmails = AllowedEmails::get();
        return datatables()->of($allowedEmails)->toJson();
    }
}
