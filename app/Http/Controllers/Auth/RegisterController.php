<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Rules\emailDomains;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Rules\RegistrationKey;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:35',
            'last_name' => 'required|string|max:35',
            'email' => ['required', 'string', 'email', 'max:62', 'unique:users', new emailDomains],
            'password' => 'required|string|min:6|confirmed',
            'terms' => 'required',
            'token' => ['required','max:62',new RegistrationKey]
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'level' => 3,
            'status' => 1,
            'token' => str_random(25),
            'password' => bcrypt($data['password']),
        ]);

        $user->sendVerificationEmail();
        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        Session::flash('reg_success', 'Your account has been created successfully. Please check your email to verify your account.');

        return $this->registered($request, $user)
            ?: redirect()->route('login');
    }
}
