<?php

namespace App\Http\Controllers;

use App\ForumCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Books;
use App\BookSections;
use App\BookSubsections;
use App\Topics;
use App\Replies;
use App\HelperClass;
use App\User;
use Riari\Forum\Frontend\Support\Forum;

class BookController extends Controller
{
    // public function index($slug, Request $request)
    // {
    //     $book = ForumCategories::where('slug', $slug)->first();
    //
    //     $result = true;
    //     $tocContent = '';
    //     $categoryTree = $this->treeViewTOC($book->chapters);
    //     $treeMenu = HelperClass::createBooksMenu($categoryTree);
    //     $tocMenu = HelperClass::createTOCMenu($categoryTree, $slug);
    //
    //     if ($request->subSection && $subSection = ForumCategories::where('slug', $request->subSection)->first()) {
    //         //echo 'inside IF<br>';
    //         $chapters = $book->chapters;
    //         $anchor = 'subSection-' . $subSection->id;
    //
    //         $subSectionChapter[] = $subSection->categoryParent;
    //         $subSectionTree = $this->treeViewContent($subSectionChapter);
    //         //dd($subSectionTree);
    //         $tocContent = HelperClass::createTOCContent($subSectionTree, $slug);
    //
    //     } elseif ($request->section && $section = ForumCategories::where('slug', $request->section)->first()) {
    //         //echo 'inside ELSE IF 1<br>';
    //         $chapters = $section->chapters;
    //         $anchor = 'section-' . $section->id;
    //     } elseif ($request->chapters && $chapters = $book->chapter($request->chapters)) {
    //         //echo 'inside ELSE IF 2<br>';
    //         $anchor = 'chapter-' . $chapters->id;
    //     } elseif ($slug != '' && empty($_GET)) {
    //         //echo 'inside ELSE IF 3<br>';
    //         $pageTitle = 'Table of content - '.$book->title;
    //         return view('book-toc', compact('book', 'pageTitle', 'result', 'chapters', 'anchor', 'tocMenu' ,'treeMenu', 'tocContent', 'subSectionTree'));
    //     } else {
    //         $result = false;
    //     }
    //
    //     if ($result) {
    //         $pageTitle = 'Table of content - ' . $book->title . ' - ';
    //         return view('book', compact('book', 'categoryTree', 'request', 'pageTitle', 'tocMenu', 'result', 'chapters', 'anchor', 'treeMenu', 'tocContent', 'subSectionTree'));
    //     }
    // }

    public function index($slug, Request $request)
    {
        $book = Books::where('slug', $slug)->first();
        $result = true;

        //create books hierarchy and toc menu
        $categoryTree = $this->booksTreeView($book->id);
        $helper = new HelperClass();
        $treeMenu = $helper->createBooksViewMenu(json_decode($categoryTree));
        $tocMenu = $helper->createTOCMenu(json_decode($categoryTree), $book->slug);

        if ($request->subSection && $subSection = BookSubsections::where('slug', $request->subSection)->first()) {
            // echo 'inside if<br>';
            $chapter = $subSection->section->chapter;
            $anchor = 'subSection-' . $subSection->id;

        } elseif ($request->section && $section = BookSections::where('slug', $request->section)->first()) {
            // echo 'inside ELSE IF 1<br>';
            $chapter = $section->chapter;
            $anchor = 'section-' . $section->id;
        } elseif ($request->chapter && $chapter = $book->chapter($request->chapter)) {
            // echo 'inside ELSE IF 2<br>';
            $anchor = 'chapter-' . $chapter->id;
        } elseif ($slug != '' && empty($_GET)) {
            //redirect to inside book view with hierarchy views
            // echo 'inside ELSE IF 3<br>';
            $pageTitle = 'Table of content - '.$book->title;
            return view('book-toc', compact('book', 'pageTitle', 'result', 'chapter', 'treeMenu', 'tocMenu', 'anchor'));
        } else {
            $result = false;
        }

        //go to view if inside subsection
        if ($slug != '' && !empty($_GET)) {
            $pageTitle = 'Table of content - ' . $book->title;
            $subSection = BookSubsections::where('slug', $_GET['subSection'])->first();
            $sectionTree = $this->booksTreeViewSpecificSection($subSection->sections_id);
            return \View::make('book', array('subSectionTree' => json_decode($sectionTree),
                                        'request' => $request,
                                        'pageTitle' => $book->title,
                                        'tocMenu' => $tocMenu));
        } else {
            $result = false;
        }

        if ($result) {
            $pageTitle = 'Table of content - ' . $book->title . ' - ';
            return view('book', compact('book', 'categoryTree', 'request', 'pageTitle', 'tocMenu', 'result', 'chapter', 'anchor'));
        }
    }

    public function addTopic(Request $request)
    {
        $form = $request['formData'];
        $data = array();
        parse_str($form, $data);

        if ($data['title'] == '') {
            return response()->json(['title' => 'Title is required.'], 500);
        }

        $topic = new Topics;
        $topic->subsections_id = $data['id'];
        $topic->title = $data['title'];
        $topic->content = $request['topic'];
        $topic->category_id = 1;
        $topic->created_by = Auth::user()->id;
        $topic->slug = HelperClass::createSlug($data['title'], $topic, 'top-');

        if ($topic->save()) {
            $subSection = BookSubsections::find($data['id']);
            $topics = $subSection->topics()->paginate(3);
            return response()->json(view('topics', compact('subSection', 'topics', 'request'))->render());
        } else {
            return response()->json('error');
        }

    }

    public function addReply(Request $request)
    {
        $reply = new Replies;
        $reply->topics_id = $request['id'];
        $reply->reply = $request['data'];
        $reply->created_by = Auth::user()->id;
        $reply->slug = HelperClass::createSlug(uniqid(rand()), $reply, 'rep-');

        if ($reply->save()) {
            $topic = Topics::find($reply->topics_id);
            $subSection = $topic->subSection;
            $replies = $topic->replies()->paginate(3);
            return response()->json(view('topic-replies', compact('subSection', 'topic', 'replies', 'request'))->render());
        } else {
            return response()->json('error');
        }
    }

    public function bookContent($id, $subSection)
    {
        //dd($id, $subSection);
        $book = ForumCategories::where('slug', $subSection)->first();

        $categoryTree = $this->treeViewTOC($book->chapters);
        $treeMenu = HelperClass::createBooksMenu($categoryTree);

        $pageTitle = '';
        return view('book-content', compact('pageTitle', 'book', 'categoryTree', 'treeMenu'));
    }

}
