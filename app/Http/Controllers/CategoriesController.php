<?php

namespace App\Http\Controllers;

use App\Category;
use App\ForumCategories;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = 'Create Category';
        return view('admin.categories.create', compact('pageTitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $forumCategory = new ForumCategories();
        $forumCategory->title = $request->title;
        $forumCategory->category_id = 0;

        //saving main category
        $forumCategory->save();

        //creating subcategories recursively.
        $this->saveSubCategories($request, $forumCategory->id);
        return response()->json(['success' => 'category created successfully']);
    }

    public function saveSubCategories($forumCategories, $parent_id)
    {
        if(isset($forumCategories['children'])) {
            foreach($forumCategories['children'] as $children) {
                $subCat = new ForumCategories();
                $subCat->title = $children['title'];
                $subCat->category_id = $parent_id;

                $subCat->is_public = 1;

                if($children['key'] == 'forum') {
                    $subCat->is_forum = 1;
                    $subCat->enable_threads = 1;
                } else {
                    $subCat->is_forum = 0;
                    $subCat->enable_threads = 0;
                }
                $subCat->save();

                if(isset($children['children'])) {
                    $this->saveSubCategories($children, $subCat->id);
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
