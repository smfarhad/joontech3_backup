<?php

namespace App\Http\Controllers;

use App\ForumCategories;
use App\Books;
use App\BookChapters;
use App\BookSections;
use App\BookSubsections;
use App\TrashString;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function treeView($categories)
    {
        $data = [];
        foreach ($categories as $category) {
            $subcategories = ForumCategories::where('category_id', $category->id)->where('extra_class', '!=', 'tree-forum')->get();
            //dd($subcategories);
            $data[] = [
                'id' => $category->id,
                'title' => $category->title,
                'extra_class' => $category->extra_class
            ];
            if ($subcategories) {
                $data[]['sub_categories'] = $this->treeView($subcategories);
            }
        }

        return $data;
    }

    //to get complete hierarchy of books under books heading
    public function booksTreeViewWithBooks() {
        $books = Books::all();
        $data = [];
        $i = 0;

        //get chapters hierrarchy
        foreach($books as $book){
            $data[$i] = array('title' => $book->title,
        			 'element_id' => $book->id,
                     'slug' => $book->slug,
        			 'extra_class' => 'tree-book');

            //add the chapters hierarchy under book name
            $bookTreeData = $this->booksTreeView($book->id);
            $data[$i]['children'] = json_decode($bookTreeData);
        	$i++;
        }

        //add chapters hierarchy under books title
        $ret_data = array('title' => "Books",
                 'element_id' => 1,
                 'extra_class' => 'tree-top-layer, tree-book-0',
                 'children' => $data
             );
        return $ret_data;
    }

    //to get complete hierarchy of books under books heading
    public function categoriesTreeViewAll() {
        //get all books
        $bookTree = [$this->booksTreeViewWithBooks()];

        //get all categories
        $categoryTree = $this->getCategoryElements([], 0, 0, 0);
        // $categoryTreeTrash = $this->getCategoryElements([], 1, 0, 0);
        $final = array_merge($bookTree, $categoryTree);

        return $final;
    }

    //get all trashed items
    public function trashTreeView() {
        // $categoryTreeTrash = $this->getCategoryElements([], 1, 0, 0);
        // return $categoryTreeTrash;
        $lastRow = TrashString::orderBy('id', 'desc')->first();
        if($lastRow){
            return json_decode($lastRow->trash_string);
        }
        return "";
    }

    private function getCategoryElements($data = [],  $isDeleted, $i = 0, $parent = 0){
        $rootCategories = ForumCategories::where("category_id", $parent)
                                        ->where('is_deleted', $isDeleted)
                                        ->get();
        $class = "";
        if($i == 0){
            $class = "tree-category-0";
        } else {
            $class = "tree-category";
        }
        foreach ($rootCategories as $root) {
            $data[] = array('title' => $root->title,
                     'element_id' => $root->id,
                     'extraClass' => $root->is_forum? "tree_forum" : $class,
                     'children' => $this->getCategoryElements([], $isDeleted, $i++, $root->id)
                 );
        }

        return $data;
    }


    public function booksTreeViewSpecificSection($sectionId, $data = [])
    {
        $sections = BookSections::where('id', $sectionId)->orderBy('sort', 'ASC')->get();
        $i = 0;
        $j = 0;

        foreach ($sections as $section) {
            $data[$i]['children'][$j] = array('title' => $section->title,
                     'element_id' => $section->id,
                     'key' => "_".$section->sort,
                     'description' =>$section->data,
                     'extra_class' => 'tree-section');

            $data = $this->getSubSectionTree($data, $i, $j, $section);

            $i++;
            $j++;
        }
        return json_encode($data[0]["children"]);
    }

    public function booksTreeView($bookId, $data = [])
    {
        $chapters = BookChapters::where('books_id', $bookId)->orderBy('sort', 'ASC')->get();
        $i = 0;

        foreach ($chapters as $chapter) {
        	$data[$i] = array('title' => $chapter->title,
        			 'element_id' => $chapter->id,
        			 'key' => "_".$chapter->sort,
                     'description' =>$chapter->data,
        			 'extra_class' => 'tree-chapter');

            $data = $this->getSectionsTree($data, $i, $chapter);

        	$i++;
        }
        /*foreach ($categories as $category) {
            $subcategories = ForumCategories::where('category_id', $category->id)->get();
            //dd($subcategories);
            $data[] = [
                'id' => $category->id,
                'title' => $category->title,
                'extra_class' => $category->extra_class
            ];
            if ($subcategories) {
                $data[]['sub_categories'] = $this->booksTreeView($subcategories);
            }
        }

print_r($data);*/

	/*foreach ($chapters as $chapter) {
            //$data = $this->getDataArray($chapter, $class, $data);
	    $data[] = [
                'id' => $chapter->id,
                'title' => $chapter->title,
                'extra_class' => 'tree-chapter'
            ];

//print_r($data);

            $sections = BookSections::where('chapters_id', $chapter->id)->get();
	    foreach ($sections as $section) {
  		$data[]['sub_categories'] = [
		    'id' => $section->id,
        	    'title' => $section->title,
        	    'extra_class' => 'tree-section'
		];

		$subSections = BookSubsections::where('sections_id', $section->id)->get();
		foreach ($subSections as $subSection) {
  		    $data[]['sub_categories'] = [
		        'id' => $section->id,
        	        'title' => $section->title,
        	        'extra_class' => 'tree-subSection'
		    ];
            	}
	    }
        }*/


//print_r($data);
//die();

        return json_encode($data);
    }

    public function getSectionsTree($data = [], $i = 0, $chapter){
        $j = 0;
        $sections = BookSections::where('chapters_id', $chapter->id)->orderBy('sort', 'ASC')->get();
        if($sections){
            $data[$i]['folder'] = true;
        }
        foreach ($sections as $section) {
            $data[$i]['children'][$j] = array('title' => $section->title,
                 'element_id' => $section->id,
                 'key' => "_".$section->sort,
                 'description' =>$section->data,
                 'extra_class' => 'tree-section');

            $data = $this->getSubSectionTree($data, $i, $j, $section);
            $j++;
        }

        return $data;
    }

    public function getSubSectionTree($data = [], $i = 0, $j = 0, $section){
        $k=0;
        $subSections = BookSubsections::where('sections_id', $section->id)->orderBy('sort', 'ASC')->get();
        if($subSections){
            $data[$i]['children'][$j]['folder'] = true;
        }
        foreach ($subSections as $subSection) {
            $data[$i]['children'][$j]['children'][$k] = array('title' => $subSection->title,
                 'element_id' => $subSection->id,
                 'key' => "_".$subSection->sort,
                 'description' =>$subSection->data,
                 'slug' => $subSection->slug,
                 'extra_class' => 'tree-subSection');

            $data = $this->getForumTree($data, $i, $j, $k, $subSection);

            $k++;
        }

        return $data;
    }

    //get forums under book subsections
    public function getForumTree($data = [], $i = 0, $j = 0, $k = 0, $subSection){
        $l=0;
        $forums = ForumCategories::where('subsection_id', $subSection->id)->get();
        if($forums){
            $data[$i]['children'][$j]['children'][$k]["folder"] = true;
        }

        foreach ($forums as $forum) {
            $data[$i]['children'][$j]['children'][$k]['children'][$l] = array('title' => $forum->title,
                 'element_id' => $forum->id,
                 // 'key' => "_".$subSection->sort,
                 'slug' => $forum->slug,
                 'description' =>$forum->description,
                 'extra_class' => 'tree-forum');

            $l++;
        }

        return $data;
    }


    public function treeViewTOC($categories)
    {
        $data = [];
        foreach ($categories as $category) {
            $subcategories = ForumCategories::where('category_id', $category->id)->where('extra_class', '!=', 'tree-forum')->get();
            //dd($subcategories);
            $data[] = [
                'id' => $category->id,
                'title' => $category->title,
                'extra_class' => $category->extra_class,
                'slug' => $category->slug,
                'description' => $category->description
            ];
            if ($subcategories) {
                $data[]['sub_categories'] = $this->treeViewTOC($subcategories);
            }
        }

        return $data;
    }

    public function treeViewContent($categories)
    {
        $data = [];
        foreach ($categories as $category) {
            if (isset($category->id)) {
                $subcategories = ForumCategories::where('category_id', $category->id)->where('extra_class', '!=', 'tree-forum')->get();
                //dd($subcategories);
                $data[] = [
                    'id' => $category->id,
                    'title' => $category->title,
                    'extra_class' => $category->extra_class,
                    'slug' => $category->slug,
                    'description' => $category->description
                ];
                if ($subcategories) {
                    $data[]['sub_categories'] = $this->treeViewContent($subcategories);
                }
            }
        }


        return $data;

    }


}
