<?php

namespace App\Http\Controllers;

use App\BookChapters;
use App\Books;
use App\BookSections;
use App\Category;
use App\ForumCategories;
use Illuminate\Http\Request;

class DiscussionController extends Controller
{
    //
    public function index()
    {
        $pageTitle = 'Discussions';
        $categories = ForumCategories::where(['category_id' => 0])->get();
        return view('admin.discussion.index', compact('pageTitle', 'categories'));
    }

    public function books($id)
    {
        $pageTitle = 'Books Categories';
        if($id && empty($subCatId)) {
            $books = BookChapters::where( [ 'status'=> '0', 'books_id' => $id] )->orderBy('id', 'DESC')->get();
        } /*elseif(!empty($subCatId)){
            $books = BookSections::where( [ 'status'=> '0', 'books_id' => $id, 'chapter_id' => $subCatId] )->orderBy('id', 'DESC')->get();
        }*/else {
            $books = Books::where('status', '0')->orderBy('id', 'DESC')->get();
        }

        return view('admin.discussion.books', compact('pageTitle', 'books', 'categories'));
    }

    public function categories($id)
    {
        $pageTitle = 'Categories';
        $categories = ForumCategories::where('category_id', $id)->get();
        return view('admin.discussion.index', compact('pageTitle', 'categories'));
    }


    public function deleteForumThread(Request $request)
    {
        dump($request);
    }
}
