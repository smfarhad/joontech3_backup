<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Riari\Forum\Frontend\Http\Controllers\CategoryController;
use Riari\Forum\Frontend\Events\UserViewingIndex;
use Riari\Forum\Frontend\Events\UserViewingCategory;
use App\Books;
use App\CategoryExtension;
use Riari\Forum\Models\Category;
use Riari\Forum\Models\Thread;
use Illuminate\Support\Facades\Gate;

class DiscussionViewController extends CategoryController
{
    public function index(Request $request) {
        $categories = $this->api('category.index')
                           ->parameters(['where' => ['category_id' => 0], 'orderBy' => 'weight', 'orderDir' => 'asc', 'with' => ['categories', 'threads']])
                           ->get();

        $categories[] = $categories[0] ;
        $categories[0] = $this->getBooksCategory(0, "Books", "books");

        event(new UserViewingIndex);
        $pageTitle = trans('forum::general.index');

        return view('forum::category.index', compact('categories', 'pageTitle'));
    }

    private function getBooksCategory($id, $title, $slug){
        $forumCategory = new CategoryExtension();
        $forumCategory->id = $id;
        $forumCategory->slug = $slug;
        $forumCategory->title = $title;
        $forumCategory->is_public = 1;
        $forumCategory->is_deleted = 0;
        return $forumCategory;
    }

    public function show(Request $request) {
        if($request["slug"] == "books"){
            $category = $this->getBooksCategory($request["id"], "Books", "books");
        } else if($request["slug"] == "chapters"){
            $category = $this->getBooksCategory($request["id"], "Chapters", "chapters");
        } else if($request["slug"] == "sections"){
            $category = $this->getBooksCategory($request["id"], "Sections", "sections");
        } else if($request["slug"] == "subsections"){
            $category = $this->getBooksCategory($request["id"], "Subsections", "subsections");
        }  else if($request["slug"] == "forums"){
            $category = $this->getBooksCategory($request["id"], "Forums", "forums");
        } else {
            $category = Category::where("slug", $request["slug"])->first();
            if(!$category){
                $category = Category::find($request["id"]);
                if(!$category){
                    $category = $this->getBooksCategory($request["id"], "Forums", "forums");
                }
            }
        }

        return $this->redirectToView($category);
    }

    private function redirectToView($category) {
        event(new UserViewingCategory($category));
        $categories = [];
        if (Gate::allows('moveCategories')) {
            $categories = $this->api('category.index')->parameters(['where' => ['category_id' => 0]])->get();
        }
        $pageTitle = $category->title;
        $threads = $category->threadsPaginated;

        return view('forum::category.show', compact('categories', 'category', 'threads', 'pageTitle'));
    }
}
