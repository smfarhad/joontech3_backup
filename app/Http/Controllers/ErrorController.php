<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorController extends Controller
{
    public function page_404(){
        return view('errors.error_404');
    }

    public function page_403(){
        return view('errors.error_403');
    }
}
