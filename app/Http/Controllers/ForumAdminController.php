<?php

namespace App\Http\Controllers;
use App\HelperClass;
use Illuminate\Http\Request;
use Riari\Forum\API\Dispatcher;
use App\TrashString;
use App\ForumCategories;
use App\Books;
use App\BookChapters;
use App\BookSections;
use App\BookSubsections;
use Illuminate\Support\Facades\DB;

class ForumAdminController extends Controller
{
    public function manageForum(){
        $books = json_encode($this->categoriesTreeViewAll()); //all categories with books
        $trash = json_encode($this->trashTreeView()); //all trashed items
        $pageTitle = 'Add Category';
        return view('admin.category-create', compact('pageTitle', 'books', 'trash'));
    }

    //get properties of category
    public function getProperties(Request $request){
        $category = ForumCategories::find(json_decode($request["data"]));
        return $category;
    }

    public function saveForumFromPanel(Request $request){
        //save the entire forum admin state
        $data = json_decode($request['data'], true);

        //save the added categories
        foreach($data['children'] as $category){
            if(array_key_exists("extraClass", $category['data'])){
                $this->saveCategory($category, $data["description"], $data["is_private"]);
            }
        }

        //save the trashed items
        if(isset($data['trash']['children'])){
            foreach($data['trash']['children'] as $trash){
                $this->updateDeleted($trash, 1);
            }
        }

        //update trash string
        $this->updateTrashString(json_encode($data['trash']));
    }

    //save while removing node to recycle bin
    public function moveNodeToRecycle(Request $request){
        $data = json_decode($request['data'], true);

        //return if no id present
        if(!isset($data["data"]["element_id"])){
            return;
        }
        $this->updateDeleted($data, 1);
    }

    //to update trash string after permanently deleting
    public function updateTrashStringAjax(Request $request){
        $this->updateTrashString($request["data"]);
        if(!isset($data["data"]["element_id"])){
            return;
        }
        $this->updateDeleted($data, 1);
    }

    public function updateTrashString($trashString){
        //update blank string if undefined node is returned from trash
        if($trashString == "undefined" || $trashString == '{"expanded":true,"key":"root_1","title":"root"}'){
            $trashString = "";
        }

        // DB::table('forum_categories')->update(['trash_string' => ""]);
        $lastId = TrashString::orderBy('id', 'desc')->first();
        if(!$lastId){
            $lastId = new TrashString();
        }
        $lastId->trash_string = $trashString;

        try {
            $lastId->save();
            DB::commit();
        } catch (\Exception $e) {
            echo($e->getMessage());
            DB::rollback();
//            return response()->json(['errors' => 'Book creation failed']);
        }
    }

    private function updateDeleted($category, $deleteFlag){

        //do not do anything if element id does not exits
        if(!isset($category['data']["element_id"])){
            return;
        }

        //update is_deleted of trashed items
        $cat = ForumCategories::find($category['data']["element_id"]);
        if($cat){
            $cat->is_deleted = $deleteFlag;
            DB::beginTransaction();
            try {
                $cat->save();
                if(isset($category['children'])){
                    foreach ($category['children'] as $children) {
                        $this->updateDeleted($children, $deleteFlag);
                    }
                }
                DB::commit();
            } catch (\Exception $e) {
                echo($e->getMessage());
                DB::rollback();
    //            return response()->json(['errors' => 'Book creation failed']);
            }
        }
    }

    //delete premanently deleted rows from database
    public function hardDeleteElement(Request $request){
        $data = json_decode($request["data"]);
        if(!isset($data->data->element_id)){
            return;
        }

        $this->hardDelete($data);
    }

    // restore from recycle bin
    public function undeleteCategory(Request $request){
        $data = json_decode($request["data"], true);
        //return if no id present
        if(!isset($data["data"]["element_id"])){
            return;
        }
        $this->updateDeleted($data, 0);
    }

    //delete from database. called when deleted permanently
    private function hardDelete($data){
        DB::beginTransaction();
        try{
            ForumCategories::where('id', $data->data->element_id)->delete();
            if(isset($data->children)){
                foreach ($data->children as $children) {
                    $this->hardDelete($children);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            echo($e->getMessage());
            DB::rollback();
        }
    }

    private function saveCategory($category, $description, $isPrivate, $parent = 0){
        if(isset($category['data']["element_id"])){
            $cat = ForumCategories::find($category['data']["element_id"]);
        } else {
            $cat = new ForumCategories();
            $cat->is_public = 1;
        }

        // get description text from array
        foreach ($description as $key => $value) {
            if(isset($category['data']["element_id"]) && $key == $category['data']["element_id"]){
                $cat->description = $value;
                break;
            }
        }

        // get isPrivate parameter from array
        foreach ($isPrivate as $key => $value) {
            if(isset($category['data']["element_id"]) && $key == $category['data']["element_id"]){
                $cat->is_public = $value ? 0 :1 ;
                break;
            }
        }

        $cat->title = $category["title"];
        $cat->category_id = $parent;
        $cat->slug = HelperClass::createSlug($category["title"], $cat);

        if($category['data']['extraClass'] == "tree-forum"){
            $cat->is_forum = 1;
            $cat->enable_threads = 1;
        }

        DB::beginTransaction();
        try {
            $cat->save();
            if(isset($category['children'])){
                foreach ($category['children'] as $children) {
                    $this->saveCategory($children, $description, $isPrivate, $cat->id);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            echo($e->getMessage());
            DB::rollback();
        }
    }

    //updating permission of forum
    public function updatePermission(Request $request){
        $data = json_decode($request["data"], true);
        $cat = ForumCategories::find($data["id"]);
        $cat->is_public = $data["checked"] ? 0 : 1;
        DB::beginTransaction();
        try {
            $cat->save();
            DB::commit();
        } catch (\Exception $e) {
            echo($e->getMessage());
            DB::rollback();
        }
    }

    private function getForums($id, $class, $catArray = array()){
        if($class == "tree-category" || $class == "tree-category-0"){
            $cat = ForumCategories::where("category_id", $id)->get();
            foreach ($cat as $key => $value) {
                if($value->is_forum == 1){
                    $catArray[] = $value;
                } else {
                    $catArray = $this->getForums($value->id, $class, $catArray);
                }
            }
        } else if($class != "tree-book-0"){
            if($class == "tree-book"){
                $hierarchy = json_decode($this->booksTreeView($id));
                $catArray = $this->getAllSubCategoryForums($hierarchy);
            } else if($class == "tree-chapter"){
                $chapter = BookChapters::find($id);
                $hierarchy = json_decode(json_encode($this->getSectionsTree([], 0, $chapter)));
                $catArray = $this->getAllSubCategoryForums($hierarchy[0]->children);
            } else if($class == "tree-section"){
                $section = BookSections::find($id);
                $hierarchy = json_decode(json_encode($this->getSubSectionTree([], 0, 0, $section)));
                $catArray = $this->getAllSubCategoryForums($hierarchy[0]->children[0]->children);
            } else if($class == "tree-subSection"){
                $catArray = $this->getForumsOfSubsecrion($id);
            }
        }

        return $catArray;
    }

    private function getForumsOfSubsecrion($subsection_id, $forumsArray = array()){
        $forums = ForumCategories::where("subsection_id", $subsection_id)->get();
        foreach ($forums as $key => $value) {
            $forumsArray[] = $value;
        }

        return $forumsArray;
    }

    private function getAllSubCategoryForums($hierarchy, $forumsArray = array()){
        // var_dump($hierarchy);
        // die();
        foreach ($hierarchy as $key => $value) {
            if($value->extra_class != "tree-subSection" && isset($value->children)){
                $forumsArray =  $this->getAllSubCategoryForums($value->children, $forumsArray);
            } else {
                // echo $value["element_id"];
                $forumsArray = $this->getForumsOfSubsecrion($value->element_id, $forumsArray);
            }
        }
        return $forumsArray;
    }

    //updating thread permission of forum
    public function updateThreadPermission(Request $request){
        $data = json_decode($request["data"], true);
        if($data["class"] == "tree-forum" || $data["class"] == "tree_forum"){
            $cat = ForumCategories::find($data["id"]);
            if($cat->is_forum != 1){
                $cat = $this->getForums($data["id"], $data["class"]);
            }
        } else {
            $cat = $this->getForums($data["id"], $data["class"]);
        }


        DB::beginTransaction();
        try {
            if(is_array($cat)){
                foreach ($cat as $key => $value) {
                    $value->enable_threads = $data["checked"] ? 0 : 1;
                    $value->save();
                }
            } else {
                $cat->enable_threads = $data["checked"] ? 0 : 1;
                $cat->save();
            }
            DB::commit();
        } catch (\Exception $e) {
            echo($e->getMessage());
            DB::rollback();
        }
    }
}
