<?php

namespace App\Http\Controllers;

use App\ForumCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\BookSubsections;
use App\Topics;
use App\Replies;
use Riari\Forum\Models\Post;
use Riari\Forum\Models\Thread;

class ForumController extends Controller
{
    public function showTopics(Request $request)
    {
       
        $subSection = BookSubsections::where('slug', $request['slug'])->first();
        $topics = $subSection->topics()->paginate(3);
        return response()->json(view('topics', compact('subSection', 'topics', 'request'))->render());
    }

    public function showReplies(Request $request)
    {
        $topic = Topics::where('slug', $request['slug'])->first();
        $subSection = $topic->subSection;
        $replies = $topic->replies()->paginate(3);
        return response()->json(view('topic-replies', compact('subSection', 'topic', 'replies', 'request'))->render());
    }

    public function topicEdit(Request $request)
    {
        $topic = Topics::where('slug', $request['slug'])->first();
        return response()->json(view('topic-edit', compact('topic'))->render());
    }

    public function topicUpdate(Request $request)
    {
        $form = $request['formData'];
        $data = array();
        parse_str($form, $data);

        $topic = Topics::find($data['id']);
        if (Auth::user()->canEdit($topic->created_by)) {
            $topic->title = $data['title'];
            $topic->content = $request->topic;
            //$topic->edited_by = add here
            if ($topic->save()) {
                $subSection = $topic->subSection;
                $topics = $subSection->topics()->paginate(3);
                return response()->json(view('topics', compact('subSection', 'topics', 'request'))->render());
            }
        }
        return response()->json('error', 500);
    }

    public function replyEdit(Request $request)
    {
        $reply = Replies::find($request->id);
        return response()->json(view('reply-edit', compact('reply'))->render());
    }

    public function replyUpdate(Request $request)
    {
        $reply = Replies::find($request['id']);
        if (Auth::user()->canEdit($reply->created_by)) {
            $reply->reply = $request->reply;
            //$topic->edited_by = add here
            if ($reply->save()) {
                $topic = Topics::find($reply->topics_id);
                $subSection = $topic->subSection;
                $replies = $topic->replies()->paginate(3);
                return response()->json(view('topic-replies', compact('subSection', 'topic', 'replies', 'request'))->render());
            }
        }
        return response()->json('error', 500);
    }

    public function getForums(Request $request)
    {
       
        
        $forums = ForumCategories::where('category_id', $request->id)->where('is_forum', 1)->get();

        $pageTitle = 'Forums';
        $parent = '';
        if (count($forums)) {
            $parent = $forums[0]->categoryParent->title;
        }
        return response()->json(view('toc-forums', compact('pageTitle', 'parent', 'forums'))->render());

    }

    public function getThreads(Request $request)
    {
        $category = ForumCategories::where("subsection_id", $request->id)->get();
        $threads = '';
        $forumId = 0;

        if ($category) {
            foreach ($category as $forum) {
                $forumId = $forum->id;
                $threads = Thread::join('users', 'users.id', '=', 'forum_threads.author_id')
                                  ->select('forum_threads.*', 'users.first_name', 'users.last_name')
                                  ->where('category_id', $forum->id)->orderBy('pinned', 'DESC')->withTrashed()->get();
            }
            $pageTitle = 'Threads';
        }
        return response()->json(view('toc-threads', compact('pageTitle', 'parent', 'forumId', 'threads'))->render());
    }

    public function getPosts(Request $request)
    {
        
        $pageTitle = 'Threads';
        $mainPost = '';
        $comments = '';
        $threadId = $request->id;
        $totalComments = [];

        $post = Post::where('thread_id', $request->id)->whereNull('post_id')->withTrashed()->get();
        // dd($request->all(), $post);

        if ($post) {
            $mainPost = $post[0];
            $comments = Post::where('thread_id', $request->id)->Where('post_id', '0')->withTrashed()->get();
            $totalComments[] = count($comments);
            foreach ($comments as $comment) {
                $totalComments[] = count($comment->children);
            }
            //dd($totalComments);
        }

        $totalComments = array_sum($totalComments);
        return response()->json(view('toc-posts', compact('pageTitle', 'post', 'comments', 'mainPost', 'totalComments', 'threadId'))->render());
    }

    public function postReply(Request $request)
    {
        //dd($request);
        parse_str($request->data, $post);
        if (isset($post['post_id']) && $post['post_id'] != 0) {
            $comment = Post::find($post['post_id']);
            $comment->content = $request->reply;
        } else {
            $comment = new Post();
            $comment->thread_id = $post['thread_id'];
            $comment->author_id = Auth::id();
            $comment->content = $request->reply;
            $comment->post_id = $post['comment_id'];
        }

        $comment->save();
        return response()->json(['status' => true, 'message' => 'done', 'thread_id' => $post['thread_id'], 'post_id' => $post['post_id']]);
    }

    public function pinThread(Request $request)
    {
        $thread = Thread::withTrashed()->find($request->threadId);
        $response = [
            'status' => false,
            'message' => 'failed'
        ];

        if ($thread) {
            switch ($request->action) {
                case 'pinned' :
                    $thread->pinned = 0;
                    $thread->save();
                    break;

                case 'pin' :
                    $thread->pinned = 1;
                    $thread->save();
                    break;

                case 'lock' :
                    $thread->locked = 1;
                    $thread->save();
                    break;

                case 'unlock' :
                    $thread->locked = 0;
                    $thread->save();
                    break;

                case 'restore' :
                    $thread->restore();
                    break;

                case 'delete' :
                    $thread->delete();
                    break;

                case 'permadelete' :
                    $category = ForumCategories::where('title', 'Recycled')->first();
                    $thread->category_id = $category->id;
                    $thread->save();
                    break;
            }

            $response = [
                'status' => true,
                'message' => 'done'
            ];
        }

        return response()->json($response);
    }

    public function deleteThread(Request $request)
    {
        dd($request);
    }

    public function deletePost(Request $request)
    {
        $post = Post::withTrashed()->find($request->postId);

        dump($post);
        switch ($request->action) {
            case 'delete' :
                $post->delete();
                break;

            case 'restore':
                $post->restore();
                break;

            case 'permadelete' :
                Thread::withTrashed()->find($request->threadId)->delete();
                $thread = Thread::withTrashed()->find($request->threadId);
                $thread->forceDelete();

                break;

            case 'permadeletepost' :
                $post->forceDelete();
                break;
        }

        return response()->json(['status' => true, 'message' => 'success']);
    }


    public function createThread(Request $request)
    {
        $forumId = $request->forumId;
        return response()->json(view('toc-new-thread', compact('forumId'))->render());
    }

    public function storeThread(Request $request)
    {
        parse_str($request['data'], $post);
        $thread = new Thread();
        $thread->category_id = $post['forumId'];
        $thread->author_id = Auth::id();
        $thread->title = $post['title'];

        //dd($post);
        $thread->save();
        if ($thread->id) {
            $postModel = new Post();
            $postModel->thread_id = $thread->id;
            $postModel->author_id = Auth::id();
            $postModel->content = $post['description'];

            //dd($postModel);
            $postModel->save();
        }

        return response()->json(['status' => true, 'message' => 'done', 'subSectionId' => $thread->category->category_id]);
    }
}
