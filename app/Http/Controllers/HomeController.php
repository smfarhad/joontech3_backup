<?php

namespace App\Http\Controllers;

use App\ForumCategories;
use Illuminate\Http\Request;
use App\Books;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $books = ForumCategories::where('is_book',1)->orderBy('id', 'DESC')->paginate(6);
        $books = Books::paginate(6);
        $pageTitle = 'Home';
        return view('home',compact('books', 'pageTitle'));
    }
}
