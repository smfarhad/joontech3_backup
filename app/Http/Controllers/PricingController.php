<?php
    /**
     * Created by PhpStorm.
     * User: msherax
     * Date: 2/9/18
     * Time: 12:18 PM
     */
    
    namespace App\Http\Controllers;
    
    use App\Pricing;
    
    class PricingController
    {
        public function pricing()
        {
            return view('pricing.index');
        }
    }