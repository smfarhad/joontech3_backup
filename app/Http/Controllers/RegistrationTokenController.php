<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RegistrationToken;

class RegistrationTokenController extends Controller
{
    public function index(){
        return view('admin.registration-keys');
    }

    public function registrationTokensDataTable(){
        return datatables()->of(RegistrationToken::all())->toJson();
    }
}
