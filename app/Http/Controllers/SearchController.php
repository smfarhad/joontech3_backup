<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BookSubsections;
use App\Books;
use App\User;
use DB;

class SearchController extends Controller
{
    public function index(){
        return view('search');
    }

    public function doSearch(Request $request){

        $form_data = $request->formData;
        $search = isset($form_data['keyword']) ? $form_data['keyword'] : '';
        $user_name = isset($form_data['user_name']) ? $form_data['user_name'] : '';
        $advance = isset($form_data['advance']) ? $form_data['advance'] : '';
        $categories = isset($form_data['categories']) ? $form_data['categories'] : '';

        $book_ids = isset($categories['books']) ? $categories['books'] : '';
        $chapter_ids = isset($categories['chapters']) ? $categories['chapters'] : '';
        $section_ids = isset($categories['sections']) ? $categories['sections'] : '';
        $subsection_ids = isset($categories['subsections']) ? $categories['subsections'] : '';

        $query = DB::table('books')
            ->select(
                'books.title as book_title', 'books.slug as book_slug',
                'book_chapters.title as chapter_title', 'book_chapters.slug as chapter_slug',
                'book_sections.title as section_title', 'book_sections.slug as section_slug',
                'book_subsections.*'
                )
            ->join("book_chapters","books.id","=","book_chapters.books_id")
            ->join("book_sections","book_chapters.id","=","book_sections.chapters_id")
            ->join("book_subsections","book_sections.id","=","book_subsections.sections_id")
            ->where("book_subsections.data","LIKE","%$search%")
            ->groupBy("book_subsections.books_id");
        
        // Include book id in search from selected book category
        if( $book_ids ){
            $query->whereIn("books.id",$book_ids);
        }

        // Include chapter id in search from selected chapter category
        if( $chapter_ids ){
            $query->whereIn("book_chapters.id",$chapter_ids);
        }
        
        // Include section id in search from selected section category
        if( $section_ids ){
            $query->whereIn("book_sections.id",$section_ids);
        }
        
        // Include subsection id in search from selected subsection category
        if( $subsection_ids ){
            $query->whereIn("book_subsections.id",$subsection_ids);
        }

        // Include search by username
        if( $user_name != '' && $advance == 'true' ){
            $user = User::search($user_name, null, true)->get();
            $query->whereIn("books.created_by",$user->pluck('id'));
        }
            
        $results = $query->paginate(5);

        return response()->json(view('search-result',compact('results','search'))->render());
    }

    public function advanceSearch(){
        $books = Books::where('status',0)->get();
        return response()->json(view('advance_search',compact('books'))->render());
    }
}
