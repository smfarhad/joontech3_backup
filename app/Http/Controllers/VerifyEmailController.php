<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VerifyEmailController extends Controller
{
    public function verify($token)
    {
        $user = User::where('token', $token)->first();
        //dd($user);

        if($user) {
            $user->update(['token' => null, 'status' => 0]);
            Session::flash('email_verified', 'Congratulations! you account has been activated. You can login Now.');
            return redirect()->route('login');
        }
    }

}
