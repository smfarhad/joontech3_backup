<?php

function dropDown($result,$selected=false){
    $dropdown = '';
    if( $result != '' ){
        foreach ($result as $key => $value) {
            $select = ( $selected == $value->id ) ? 'selected' : '';
            $dropdown .= "<option value='$value->id' $select>$value->name</option>";
        }
    }
    return $dropdown;
}

function dateNameFormat($date){
    
    $d1=new DateTime($date); 
    $d2=new DateTime(date('Y-m-d H:i:s'));
    $diff=$d2->diff($d1);
    $result = null;

    if( $diff->y > 0 ){
        // Year ago
        $word = ( $diff->y>1 ) ? ' years' : ' year';
        $result = $diff->y.$word;
    }elseif( $diff->m > 0 && $diff->y == 0 ){
        // Month ago
        $word = ( $diff->m>1 ) ? ' months' : ' month';
        $result = $diff->m.$word;
    }elseif( $diff->d > 0 && $diff->m == 0 && $diff->y == 0 ){
        // Days ago
        $word = ( $diff->d>1 ) ? ' days' : ' day';
        $result = $diff->d.$word;
    }elseif( $diff->h > 0 && $diff->d == 0 && $diff->m == 0 && $diff->y == 0 ){
        // Hours ago
        $word = ( $diff->h>1 ) ? ' hours' : ' hour';
        $result = $diff->h.$word;
    }elseif( $diff->i > 0 && $diff->h == 0 && $diff->d == 0 && $diff->m == 0 && $diff->y == 0 ){
        // Minutes ago
        $word = ( $diff->i>1 ) ? ' minutes' : ' minute';
        $result = $diff->i.$word;
    }elseif( $diff->s > 0 && $diff->i == 0 && $diff->h == 0 && $diff->d == 0 && $diff->m == 0 && $diff->y == 0 ){
        // Secons ago
        $word = ( $diff->s>1 ) ? ' seconds' : ' second';
        $result = $diff->s.$word;
    }else{
        $result = $date;
    }
    
    return $result;
}

function replies($count){
    return ($count>1) ? $count.' replies': '';
}

function sidebarActive($request,$id,$depth){
    if( $request->subSection == $id && $depth == 'subSection' ){
        $result = true;
    }elseif( $request->section == $id && $depth == 'section' ){
        $result = true;
    }elseif( $request->chapter == $id && $depth == 'chapter' ){
        $result = true;
    }else{
        $result = false;
    }
    
    return $result ? 'class="active"' : '';
}

function bookImage($link){
    return ($link != '') ? $link : 'assets/images/books/book-default.png';
}

function searchResult($h,$n,$w=10,$tag='match') {
	$n = explode(" ",trim(strip_tags($n)));	//needles words
    $b = explode(" ",trim(strip_tags($h)));	//haystack words
	$c = array();						//array of words to keep/remove
	for ($j=0;$j<count($b);$j++) $c[$j]=false;
	for ($i=0;$i<count($b);$i++) 
		for ($k=0;$k<count($n);$k++) 
			if (stristr($b[$i],$n[$k])) {
				$b[$i]=preg_replace("/".$n[$k]."/i","<$tag>\\0</$tag>",$b[$i]);
				for ( $j= max( $i-$w , 0 ) ;$j<min( $i+$w, count($b)); $j++) $c[$j]=true; 
			}	
	$o = "";	// reassembly words to keep
	for ($j=0;$j<count($b);$j++) if ($c[$j]) $o.=" ".$b[$j]; else $o.=".";
    return preg_replace("/\.{3,}/i","...",$o);
}

function ifPrint($check,$print,$else=''){
    if( $check != '' ){
        return str_replace('%s',$check,$print);
    }else{
        return $else;
    }
}