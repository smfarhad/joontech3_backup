<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistrationToken extends Model
{
    public static function isAllowedKey($key){
        return RegistrationToken::select('id')
            ->where('key',$key)
            ->where('status',0)
            ->get();
    }
}
