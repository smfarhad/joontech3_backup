<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Replies extends Model
{
    public function createdBy(){
        return $this->hasOne('App\User','id','created_by');
    }
}
