<?php

namespace App\Rules;

use App\AllowedEmails;
use Illuminate\Contracts\Validation\Rule;

class emailDomains implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $allowedEmails = AllowedEmails::all();
        $domainsArr = [];
        if ($allowedEmails) {
            foreach ($allowedEmails as $email) {
                $domainsArr[] = $email->email;
            }
        }

        $valArr = explode('@', $value);
        $valArr[1] = '@'.$valArr[1];
        return in_array($valArr[1], $domainsArr) !== false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "This email address is not allowed in our system.";
    }
}
