<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topics extends Model
{
    public function replies_all(){
        return $this->hasMany('App\Replies');
    }

    public function replies(){
        return $this->replies_all()->where('status',0)->orderBy('created_at','ASC');
    }

    public function createdBy(){
        return $this->hasOne('App\User','id','created_by');
    }

    public function subSection(){
        return $this->hasOne('App\BookSubsections','id','subsections_id');
    }
}
