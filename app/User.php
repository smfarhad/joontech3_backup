<?php

namespace App;

use App\Notifications\VerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Nicolaslopezj\Searchable\SearchableTrait;

class User extends Authenticatable
{
    use Notifiable;
    use SearchableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'level', 'token', 'status'
    ];

    protected $searchable = [
        'columns' => [
            'first_name' => 20,
            'last_name' => 10
        ]
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    static public function author($user){
        return $user->first_name.' '.$user->last_name;
    }

    public function user_level(){
        return $this->hasOne('App\UserLevels','id','level');
    }

    static public function ActiveUsers(){
        return User::with('user_level')
            ->where('level','>',2)
            //->where('status',0)
            ->get();
    }

    public function isAdmin(){
        return $this->level <= 2;
    }

    public function fullName(){
        return $this->first_name.' '.$this->last_name;
    }

    public function canEdit($id){
        return ($this->level <= 2 || $this->id == $id);
    }

    public function sendVerificationEmail()
    {
        return $this->notify(new VerifyEmail($this));
    }
}
