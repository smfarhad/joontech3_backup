<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookSubsectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_subsections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',128)->nullable();
            $table->tinyInteger('books_id');
            $table->tinyInteger('sections_id');
            $table->longText('data')->nullable();
            $table->integer('sort');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_subsections');
    }
}
