<?php

use Illuminate\Database\Seeder;

class UserLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
                ['name' => 'Super Admin'],
                ['name' => 'Admin'],
                ['name' => 'Author'],
                ['name' => 'User']
            ];
        DB::table('user_levels')->insert($users);
    }
}