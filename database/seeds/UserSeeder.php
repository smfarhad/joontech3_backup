<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'email' => 'super@user.com',
                'first_name' => 'Super',
                'last_name' => 'Admin',
                'password' => bcrypt('secret'),
                'level' => 1
            ],
            [
                'email' => 'admin@user.com',
                'first_name' => 'Admin',
                'last_name' => 'user',
                'password' => bcrypt('secret'),
                'level' => 2
            ],
            [
                'email' => 'author@user.com',
                'first_name' => 'Author',
                'last_name' => 'user',
                'password' => bcrypt('secret'),
                'level' => 3
            ],
            [
                'email' => 'user@user.com',
                'first_name' => 'User',
                'last_name' => 'user',
                'password' => bcrypt('secret'),
                'level' => 3
            ]
        ];
        DB::table('users')->insert($users);
    }
}