-- Database: `textbooks`
--
CREATE DATABASE IF NOT EXISTS `textbooks` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `textbooks`;

-- --------------------------------------------------------

--
-- Table structure for table `book_chapters`
--

CREATE TABLE `book_chapters` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `books_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_chapters`
--

INSERT INTO `book_chapters` (`id`, `title`, `books_id`, `sort`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Chapter 1', 1, 0, 1, 'chp-chapter-1', '2018-01-04 17:12:52', '2018-01-05 01:03:06'),
(2, 'Chapter 2', 1, 2, 1, 'chp-chapter-2', '2018-01-04 17:12:52', '2018-01-05 01:03:06'),
(3, '[Chapter 1: Title]', 2, 0, 0, 'chapter-1-title', '2018-01-04 17:19:03', '2018-01-25 05:18:23'),
(4, 'Chapter 2', 2, 1, 1, 'chp-chapter-2-1', '2018-01-04 17:19:04', '2018-01-25 05:18:23'),
(5, 'testNew chapter', 1, 1, 1, '', '2018-01-05 00:55:52', '2018-01-05 01:03:06'),
(6, 'Chapter 1', 3, 0, 0, 'chp-chapter-1-2', '2018-01-13 04:16:32', '2018-01-13 04:16:32'),
(7, 'Chapter 2', 3, 1, 0, 'chp-chapter-2-2', '2018-01-13 04:16:32', '2018-01-13 04:16:32'),
(8, 'Chapter 1', 4, 0, 0, 'chp-chapter-1-3', '2018-01-13 04:17:08', '2018-01-13 04:17:08'),
(9, 'Chapter 2', 4, 1, 0, 'chp-chapter-2-3', '2018-01-13 04:17:08', '2018-01-13 04:17:08'),
(10, 'Chapter 1', 5, 0, 0, 'chapter-1-1', '2018-01-25 13:37:58', '2018-01-25 17:52:57'),
(11, 'Chapter 2', 5, 1, 0, 'chapter-2-1', '2018-01-25 13:37:58', '2018-01-25 17:52:57');

-- --------------------------------------------------------

--
-- Table structure for table `book_sections`
--

CREATE TABLE `book_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `books_id` int(11) NOT NULL,
  `chapters_id` int(11) NOT NULL,
  `sort` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_sections`
--

INSERT INTO `book_sections` (`id`, `title`, `books_id`, `chapters_id`, `sort`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Section 1.1', 1, 1, '0', 1, 'sec-section-1-1', '2018-01-04 17:12:52', '2018-01-05 01:03:06'),
(2, 'Section 2.1', 1, 2, '0', 1, 'sec-section-2-1', '2018-01-04 17:12:52', '2018-01-05 01:03:06'),
(3, 'Section 1.1', 2, 3, '0', 0, 'section-1-1', '2018-01-04 17:19:04', '2018-01-25 05:18:23'),
(4, 'Section 2.1', 2, 4, '0', 1, 'sec-section-2-1-1', '2018-01-04 17:19:04', '2018-01-25 05:18:23'),
(6, 'Section 1.1', 3, 6, '0', 0, 'sec-section-1-1-2', '2018-01-13 04:16:32', '2018-01-13 04:16:32'),
(7, 'Section 2.1', 3, 7, '0', 0, 'sec-section-2-1-2', '2018-01-13 04:16:32', '2018-01-13 04:16:32'),
(8, 'Section 1.1', 4, 8, '0', 0, 'sec-section-1-1-3', '2018-01-13 04:17:08', '2018-01-13 04:17:08'),
(9, 'Section 2.1', 4, 9, '0', 0, 'sec-section-2-1-3', '2018-01-13 04:17:08', '2018-01-13 04:17:08'),
(10, 'Section 1.1', 5, 10, '0', 0, 'section-1-1-2', '2018-01-25 13:37:58', '2018-01-25 17:52:57'),
(11, 'Section 2.1', 5, 11, '0', 0, 'section-2-1-1', '2018-01-25 13:37:58', '2018-01-25 17:52:57');

-- --------------------------------------------------------

--
-- Table structure for table `book_subsections`
--

CREATE TABLE `book_subsections` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `books_id` tinyint(4) NOT NULL,
  `sections_id` tinyint(4) NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci,
  `sort` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_subsections`
--

INSERT INTO `book_subsections` (`id`, `title`, `books_id`, `sections_id`, `data`, `sort`, `created_by`, `updated_by`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Sub-section 1.1.1', 1, 1, NULL, 0, 1, 1, 1, 'sub-sub-section-1-1-1', '2018-01-04 17:12:52', '2018-01-05 01:03:07'),
(2, 'Sub-section 1.1.2', 1, 1, NULL, 1, 1, 1, 1, 'sub-sub-section-1-1-2', '2018-01-04 17:12:52', '2018-01-05 01:03:07'),
(3, 'Sub-section 2.1.1', 1, 2, NULL, 0, 1, 1, 1, 'sub-sub-section-2-1-1', '2018-01-04 17:12:52', '2018-01-05 01:03:07'),
(4, 'Sub-section 1.1.1', 2, 3, '<p>asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds&nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds&nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds&nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds&nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;</p>', 0, 1, 1, 0, 'sub-section-1-1-1', '2018-01-04 17:19:04', '2018-01-25 05:18:23'),
(5, 'Sub-section 1.1.2', 2, 3, '<p>asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds&nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;</p>', 1, 1, 1, 1, 'sub-sub-section-1-1-2-1', '2018-01-04 17:19:04', '2018-01-25 05:18:23'),
(6, 'Sub-section 2.1.1', 2, 4, NULL, 0, 1, 1, 1, 'sub-sub-section-2-1-1-1', '2018-01-04 17:19:04', '2018-01-25 05:18:23'),
(8, 'Sub-section 1.1.2', 3, 6, '<p>asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds&nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;</p>', 0, 1, 1, 0, 'sub-sub-section-1-1-2-2', '2018-01-13 04:16:32', '2018-01-16 08:01:04'),
(9, 'Sub-section 1.1.1', 3, 6, '<p>asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds&nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;asdfasdfafds &nbsp;</p>', 1, 1, 1, 0, 'sub-sub-section-1-1-1-2', '2018-01-13 04:16:32', '2018-01-16 08:01:04'),
(10, 'Sub-section 2.1.1', 3, 7, NULL, 0, 1, 1, 0, 'sub-sub-section-2-1-1-2', '2018-01-13 04:16:32', '2018-01-16 08:00:38'),
(11, 'Sub-section 1.1.1', 4, 8, NULL, 0, 1, NULL, 0, 'sub-sub-section-1-1-1-3', '2018-01-13 04:17:08', '2018-01-13 04:17:08'),
(12, 'Sub-section 1.1.2', 4, 8, NULL, 1, 1, NULL, 0, 'sub-sub-section-1-1-2-3', '2018-01-13 04:17:08', '2018-01-13 04:17:08'),
(13, 'Sub-section 2.1.1', 4, 9, NULL, 0, 1, NULL, 0, 'sub-sub-section-2-1-1-3', '2018-01-13 04:17:08', '2018-01-13 04:17:08'),
(20, 'test', 2, 3, '<p>test</p>', 1, 1, 1, 0, '', '2018-01-25 13:35:16', '2018-01-25 13:35:16'),
(21, 'Sub-section 1.1.1', 5, 10, '<p>test</p>', 0, 1, 1, 0, 'sub-section-1-1-1-2', '2018-01-25 13:37:58', '2018-01-25 17:52:57'),
(22, 'Sub-section 1.1.2', 5, 10, '<p>test</p>', 1, 1, 1, 0, 'sub-section-1-1-2-1', '2018-01-25 13:37:58', '2018-01-25 17:52:57'),
(23, 'Sub-section 2.1.1', 5, 11, NULL, 0, 1, 1, 0, 'sub-section-2-1-1-1', '2018-01-25 13:37:58', '2018-01-25 17:52:57');
INSERT INTO `book_subsections` (`id`, `title`, `books_id`, `sections_id`, `data`, `sort`, `created_by`, `updated_by`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(24, 'test1231', 5, 10, '<p>test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2</p>', 2, 1, 1, 1, '', '2018-01-25 13:40:10', '2018-01-25 17:52:57'),
(25, 'test2test2test2', 5, 10, '<p>test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 test2 v</p>', 2, 1, 1, 0, 'test2test2test2', '2018-01-25 13:40:53', '2018-01-25 17:52:57'),
(26, 'Item below here will be deleted', 5, 11, NULL, 1, 1, 1, 0, 'item-below-here-will-be-deleted-1', '2018-01-25 17:52:50', '2018-01-25 17:52:57'),
(27, 'Delete me', 5, 11, NULL, 2, 1, 1, 1, 'delete-me', '2018-01-25 17:52:50', '2018-01-25 17:52:57');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `image`, `created_by`, `updated_by`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Book 1', NULL, 1, NULL, 1, 'title-for-the-book', '2018-01-04 17:12:52', '2018-01-06 12:36:11'),
(2, 'Book Test 1', NULL, 1, NULL, 1, 'book-test-1', '2018-01-04 17:19:03', '2018-01-25 13:37:44'),
(3, 'asdfasdf', NULL, 1, NULL, 1, 'asdfasdf', '2018-01-13 04:16:32', '2018-01-25 13:37:35'),
(4, 'test', NULL, 1, NULL, 1, 'test', '2018-01-13 04:17:08', '2018-01-13 04:18:12'),
(5, 'New Test', NULL, 1, NULL, 0, 'new-test-1', '2018-01-25 13:37:58', '2018-01-25 17:52:57');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_23_125100_user_level', 1),
(4, '2017_11_24_073054_create_book_subsections_table', 1),
(5, '2017_11_25_161231_create_books_table', 1),
(6, '2017_11_25_161512_create_book_chapters_table', 1),
(7, '2017_11_25_161743_create_book_sections_table', 1),
(8, '2017_12_06_052146_create_replies_table', 1),
(9, '2017_12_06_223505_update_replies', 1),
(10, '2017_12_06_235733_create_topics_table', 1),
(11, '2017_12_11_044515_UpdateBookChapterWithPermalink', 1),
(12, '2017_12_11_050753_UpdateBookSectionWithPermalink', 1),
(13, '2017_12_11_050939_UpdateBookSubSectionWithPermalink', 1),
(14, '2017_12_11_051449_UpdateTopicsWithPermalink', 1),
(15, '2017_12_11_051512_UpdateRepliesWithPermalink', 1),
(16, '2017_12_13_144627_UpdateBookInfo', 1),
(17, '2017_12_31_004719_UpdateUserWithStatus', 1),
(18, '2018_01_02_143417_create_registration_tokens_table', 1),
(19, '2018_01_02_145319_UpdateUserWithRegistrationKey', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `registration_tokens`
--

CREATE TABLE `registration_tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(62) COLLATE utf8mb4_unicode_ci NOT NULL,
  `limit` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `registration_tokens`
--

INSERT INTO `registration_tokens` (`id`, `name`, `key`, `limit`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Sample Token', 'n7Nj6B7UTaVLyj6fkZ9CeBQfDMUjCRmBNZwvstfatMMCVuuS6u', 99, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

CREATE TABLE `replies` (
  `id` int(10) UNSIGNED NOT NULL,
  `topics_id` int(11) NOT NULL,
  `reply` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `replies`
--

INSERT INTO `replies` (`id`, `topics_id`, `reply`, `created_by`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(1, 31, '<p>test</p>', 2, 0, 'rep-21410551275a593a4ebdb51', '2018-01-13 03:44:30', '2018-01-13 03:44:30'),
(2, 67, '<p>test</p>', 2, 0, 'rep-14662036995a593c09d96e5', '2018-01-13 03:51:53', '2018-01-13 03:51:53'),
(3, 26, '<p>test</p>', 1, 0, 'rep-3886730885a5940a89f9cb', '2018-01-13 04:11:36', '2018-01-13 04:11:36'),
(4, 26, '<p>asdfaf</p>', 1, 0, 'rep-8098382245a5940af14f38', '2018-01-13 04:11:43', '2018-01-13 04:11:43'),
(5, 26, '<p>safasdgasasdgasd</p>', 1, 0, 'rep-646602355a5940b65728c', '2018-01-13 04:11:50', '2018-01-13 04:11:50'),
(6, 27, '<p>asdfasdfsd</p>', 1, 0, 'rep-6578910535a5940cef3ce0', '2018-01-13 04:12:14', '2018-01-13 04:12:14'),
(7, 27, '<p>fasdfasdf</p>', 1, 0, 'rep-5420559185a5940df76dfe', '2018-01-13 04:12:31', '2018-01-13 04:12:31'),
(8, 66, '<p>sdfsdfsdfsdfsdfsdfsdfsdf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf</p>', 1, 0, 'rep-1248893045a5943cf73ff1', '2018-01-13 04:25:03', '2018-01-14 08:35:39'),
(9, 66, '<p>testasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdftestasdf</p>', 1, 0, 'rep-9521405315a5943db917d6', '2018-01-13 04:25:15', '2018-01-14 08:35:51'),
(10, 66, '<p>fasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfd</p>', 1, 0, 'rep-5898141525a59444be1cf5', '2018-01-13 04:27:07', '2018-01-15 12:36:57'),
(11, 66, '<p>sdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdf</p>', 1, 0, 'rep-7203067555a59444e90397', '2018-01-13 04:27:10', '2018-01-14 08:34:02'),
(12, 66, '<p>23</p>', 1, 0, 'rep-20655014565a594451518c3', '2018-01-13 04:27:13', '2018-01-13 04:27:13'),
(13, 66, '<p>fasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfaffsasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfdfasdfasdfsfd</p>', 1, 0, 'rep-10030764585a5c5a3c3c64e', '2018-01-15 12:37:32', '2018-01-15 12:37:32'),
(14, 68, '<p>asf</p>', 1, 0, 'rep-8402390815a5c643988f95', '2018-01-15 13:20:09', '2018-01-15 13:20:09'),
(15, 68, '<p>asdf</p>', 1, 0, 'rep-15087279585a5c64418d6fc', '2018-01-15 13:20:17', '2018-01-15 13:20:17'),
(16, 68, '<p>asf</p>', 1, 0, 'rep-6640627795a5c644489188', '2018-01-15 13:20:20', '2018-01-15 13:20:20'),
(17, 68, '<p>aasf</p>', 1, 0, 'rep-14051879225a5c6448bf97a', '2018-01-15 13:20:24', '2018-01-15 13:20:24'),
(18, 65, '<p>asdasd</p>', 1, 0, 'rep-21402501185a5ccb260df69', '2018-01-15 20:39:18', '2018-01-15 20:39:18'),
(19, 65, '<p>asdasd</p>', 1, 0, 'rep-10242197825a5ccb38da5a4', '2018-01-15 20:39:36', '2018-01-15 20:39:36'),
(20, 65, '<p>asdasd</p>', 1, 0, 'rep-8136431425a5ccb3d8002f', '2018-01-15 20:39:41', '2018-01-15 20:39:41'),
(21, 65, '<p>adasd</p>', 1, 0, 'rep-19385907535a5ccb40e4f9b', '2018-01-15 20:39:44', '2018-01-15 20:39:44'),
(22, 68, '<p>xxx</p>', 1, 0, 'rep-14925811295a5ccb46431b3', '2018-01-15 20:39:50', '2018-01-15 20:39:50'),
(23, 31, '<p>2</p>', 1, 0, 'rep-16335907905a5efc014aa0e', '2018-01-17 12:32:17', '2018-01-17 12:32:17'),
(24, 31, '<p>3</p>', 1, 0, 'rep-539851325a5efc05bb120', '2018-01-17 12:32:21', '2018-01-17 12:32:21'),
(25, 31, '<p>4</p>', 1, 0, 'rep-4114975085a5efc09aa0c2', '2018-01-17 12:32:25', '2018-01-17 12:32:25'),
(26, 66, '<p>4</p>', 1, 0, 'rep-11935933965a5efc640fa2a', '2018-01-17 12:33:56', '2018-01-17 12:33:56'),
(27, 66, '<p>5</p>', 1, 0, 'rep-16241933275a5efc67e9be8', '2018-01-17 12:33:59', '2018-01-17 12:33:59'),
(28, 66, '<p>6</p>', 1, 0, 'rep-18018020485a5efc6b1ea1f', '2018-01-17 12:34:03', '2018-01-17 12:34:03'),
(29, 66, '<p>7</p>', 1, 0, 'rep-18923425695a5efc6e720fe', '2018-01-17 12:34:06', '2018-01-17 12:34:06'),
(30, 31, '<p>testas</p>', 1, 0, 'rep-17592630065a5f4607eb001', '2018-01-17 17:48:07', '2018-01-17 17:48:07'),
(31, 66, '<p>asdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfv</p>', 1, 0, 'rep-19280777145a67676e8902c', '2018-01-23 21:48:46', '2018-01-23 21:48:46'),
(32, 77, '<p>test</p>', 1, 0, 'rep-7083781645a6d3bb9b00fb', '2018-01-28 07:55:53', '2018-01-28 07:55:53'),
(33, 74, '<p>test</p>', 1, 0, 'rep-12790774205a6d3cef52a35', '2018-01-28 08:01:03', '2018-01-28 08:01:03'),
(34, 74, '<p>test</p>', 1, 0, 'rep-11715814745a6d3cf41b3fb', '2018-01-28 08:01:08', '2018-01-28 08:01:08');

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `id` int(10) UNSIGNED NOT NULL,
  `subsections_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`id`, `subsections_id`, `title`, `content`, `category_id`, `created_by`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(1, 1, 'test', '<p>test</p>', 1, 1, 0, 'top-test', '2018-01-05 01:00:48', '2018-01-05 01:00:48'),
(2, 1, 'test', '<p>test</p>', 1, 1, 0, 'top-test-1', '2018-01-05 01:00:52', '2018-01-05 01:00:52'),
(3, 1, 'testsdf', '<p>testasdfasfd</p>', 1, 1, 0, 'top-testsdf', '2018-01-05 01:00:59', '2018-01-05 01:00:59'),
(4, 1, 'testsdf', '<p>testasdfasfd</p>', 1, 1, 0, 'top-testsdf-1', '2018-01-05 01:00:59', '2018-01-05 01:00:59'),
(5, 1, 'testsdf', '<p>testasdfasfd</p>', 1, 1, 0, 'top-testsdf-2', '2018-01-05 01:01:02', '2018-01-05 01:01:02'),
(6, 1, 'testsdf', '<p>testasdfasfd</p>', 1, 1, 0, 'top-testsdf-3', '2018-01-05 01:01:02', '2018-01-05 01:01:02'),
(7, 1, 'testsdf', '<p>testasdfasfd</p>', 1, 1, 0, 'top-testsdf-4', '2018-01-05 01:01:02', '2018-01-05 01:01:02'),
(8, 1, 'testsdf', '<p>testasdfasfd</p>', 1, 1, 0, 'top-testsdf-5', '2018-01-05 01:01:03', '2018-01-05 01:01:03'),
(9, 1, 'testsdf', '<p>testasdfasfd</p>\n\n<p>&nbsp;</p>', 1, 1, 0, 'top-testsdf-6', '2018-01-05 01:01:07', '2018-01-05 01:01:07'),
(10, 1, 'testsdf', '<p>testasdfasfd</p>\n\n<p>&nbsp;</p>', 1, 1, 0, 'top-testsdf-7', '2018-01-05 01:01:07', '2018-01-05 01:01:07'),
(11, 1, 'testsdf', '<p>testasdfasfd</p>\n\n<p>&nbsp;</p>', 1, 1, 0, 'top-testsdf-8', '2018-01-05 01:01:07', '2018-01-05 01:01:07'),
(12, 1, 'asdfasf', '<p>adfasfd</p>', 1, 1, 0, 'top-asdfasf', '2018-01-05 01:01:14', '2018-01-05 01:01:14'),
(13, 1, 'asdfasf', '<p>adfasfd</p>', 1, 1, 0, 'top-asdfasf-1', '2018-01-05 01:01:14', '2018-01-05 01:01:14'),
(14, 1, 'asdfasf', '<p>adfasfd</p>', 1, 1, 0, 'top-asdfasf-2', '2018-01-05 01:01:14', '2018-01-05 01:01:14'),
(15, 2, 'asdfasf', '<p>asdfasf</p>', 1, 1, 0, 'top-asdfasf-3', '2018-01-05 01:01:23', '2018-01-05 01:01:23'),
(16, 2, 'asdfasf', '<p>asdfasf</p>', 1, 1, 0, 'top-asdfasf-4', '2018-01-05 01:01:23', '2018-01-05 01:01:23'),
(17, 2, 'asdfasf', '<p>asdfasf</p>', 1, 1, 0, 'top-asdfasf-5', '2018-01-05 01:01:23', '2018-01-05 01:01:23'),
(18, 2, 'asdfasf', '<p>asdfasf</p>', 1, 1, 0, 'top-asdfasf-6', '2018-01-05 01:01:25', '2018-01-05 01:01:25'),
(19, 2, 'asdfasf', '<p>asdfasf</p>', 1, 1, 0, 'top-asdfasf-7', '2018-01-05 01:01:25', '2018-01-05 01:01:25'),
(20, 2, 'asdfasfasdf', '<p>asdfasf</p>', 1, 1, 0, 'top-asdfasfasdf', '2018-01-05 01:01:30', '2018-01-05 01:01:30'),
(21, 2, 'asdfasfasdf', '<p>asdfasf</p>', 1, 1, 0, 'top-asdfasfasdf-1', '2018-01-05 01:01:30', '2018-01-05 01:01:30'),
(22, 2, 'asdfasfasdf', '<p>asdfasf</p>', 1, 1, 0, 'top-asdfasfasdf-2', '2018-01-05 01:01:30', '2018-01-05 01:01:30'),
(23, 2, 'asdfasfasdf', '<p>asdfasf</p>', 1, 1, 0, 'top-asdfasfasdf-3', '2018-01-05 01:01:30', '2018-01-05 01:01:30'),
(24, 2, 'asdf', '<p>asdf</p>', 1, 1, 0, 'top-asdf', '2018-01-05 01:01:36', '2018-01-05 01:01:36'),
(25, 3, 'fsdfasf', '<p>asdfasdf</p>', 1, 1, 0, 'top-fsdfasf', '2018-01-05 01:02:27', '2018-01-05 01:02:27'),
(26, 4, 'fasdf', '<p>asdfasdf</p>', 1, 1, 0, 'top-fasdf', '2018-01-05 01:03:50', '2018-01-05 01:03:50'),
(27, 4, 'fasdf', '<p>asdfasdf</p>', 1, 1, 0, 'top-fasdf-1', '2018-01-05 01:03:53', '2018-01-05 01:03:53'),
(28, 4, 'fasdf', '<p>asdfasdf</p>', 1, 1, 0, 'top-fasdf-2', '2018-01-05 01:03:55', '2018-01-05 01:03:55'),
(29, 5, 'asdfas', '<p>asfdasdf</p>', 1, 1, 0, 'top-asdfas', '2018-01-06 09:34:21', '2018-01-06 09:34:21'),
(30, 5, 'asdfas', '<p>asfdasdf</p>', 1, 1, 0, 'top-asdfas-1', '2018-01-06 09:34:22', '2018-01-06 09:34:22'),
(31, 5, 'asdfas', '<p>asfdasdf<span class="math-tex">\\(x = {-b \\pm \\sqrt{b^2-4ac} \\over 2a}\\)</span></p>', 1, 1, 0, 'top-asdfas-2', '2018-01-06 09:34:23', '2018-01-06 12:36:41'),
(32, 5, 'asdfas', '<p>asfdasdf</p>', 1, 1, 0, 'top-asdfas-3', '2018-01-06 09:34:23', '2018-01-06 09:34:23'),
(33, 5, 'asdfas', '<p>asfdasdf</p>', 1, 1, 0, 'top-asdfas-4', '2018-01-06 09:34:24', '2018-01-06 09:34:24'),
(34, 5, 'asdfas', '<p>asfdasdf</p>', 1, 1, 0, 'top-asdfas-5', '2018-01-06 09:34:24', '2018-01-06 09:34:24'),
(35, 4, 'test', '<p>53w5</p>', 1, 2, 0, 'top-test-2', '2018-01-13 03:45:18', '2018-01-13 03:45:18'),
(36, 4, '1', '<p>1</p>', 1, 2, 0, 'top-1', '2018-01-13 03:45:31', '2018-01-13 03:45:31'),
(37, 4, '2', '<p>2</p>', 1, 2, 0, 'top-2', '2018-01-13 03:45:35', '2018-01-13 03:45:35'),
(38, 4, '3', '<p>3</p>', 1, 2, 0, 'top-3', '2018-01-13 03:45:39', '2018-01-13 03:45:39'),
(39, 4, '4', '<p>4</p>', 1, 2, 0, 'top-4', '2018-01-13 03:45:42', '2018-01-13 03:45:42'),
(40, 4, '5', '<p>5</p>', 1, 2, 0, 'top-5', '2018-01-13 03:45:45', '2018-01-13 03:45:45'),
(41, 4, '6', '<p>6</p>', 1, 2, 0, 'top-6', '2018-01-13 03:45:49', '2018-01-13 03:45:49'),
(42, 4, '7', '<p>7</p>', 1, 2, 0, 'top-7', '2018-01-13 03:46:00', '2018-01-13 03:46:00'),
(43, 4, '8', '<p>8</p>', 1, 2, 0, 'top-8', '2018-01-13 03:46:04', '2018-01-13 03:46:04'),
(44, 4, '9', '<p>9</p>', 1, 2, 0, 'top-9', '2018-01-13 03:48:40', '2018-01-13 03:48:40'),
(45, 4, '10', '<p>10</p>', 1, 2, 0, 'top-10', '2018-01-13 03:48:46', '2018-01-13 03:48:46'),
(46, 4, '11', '<p>11</p>', 1, 2, 0, 'top-11', '2018-01-13 03:48:51', '2018-01-13 03:48:51'),
(47, 4, '12', '<p>12</p>', 1, 2, 0, 'top-12', '2018-01-13 03:48:54', '2018-01-13 03:48:54'),
(48, 4, '13', '<p>13</p>', 1, 2, 0, 'top-13', '2018-01-13 03:48:57', '2018-01-13 03:48:57'),
(49, 4, '14', '<p>14</p>', 1, 2, 0, 'top-14', '2018-01-13 03:49:03', '2018-01-13 03:49:03'),
(50, 4, '15', '<p>15</p>', 1, 2, 0, 'top-15', '2018-01-13 03:49:06', '2018-01-13 03:49:06'),
(51, 4, '16', '<p>16</p>', 1, 2, 0, 'top-16', '2018-01-13 03:49:10', '2018-01-13 03:49:10'),
(52, 4, '17', '<p>17</p>', 1, 2, 0, 'top-17', '2018-01-13 03:49:13', '2018-01-13 03:49:13'),
(53, 4, '18', '<p>18</p>', 1, 2, 0, 'top-18', '2018-01-13 03:49:18', '2018-01-13 03:49:18'),
(54, 4, '19', '<p>19</p>', 1, 2, 0, 'top-19', '2018-01-13 03:49:23', '2018-01-13 03:49:23'),
(55, 4, '20', '<p>20</p>', 1, 2, 0, 'top-20', '2018-01-13 03:49:28', '2018-01-13 03:49:28'),
(56, 4, '21', '<p>21</p>', 1, 2, 0, 'top-21', '2018-01-13 03:49:31', '2018-01-13 03:49:31'),
(57, 4, '22', '<p>22</p>', 1, 2, 0, 'top-22', '2018-01-13 03:49:34', '2018-01-13 03:49:34'),
(58, 4, '23', '<p>23</p>', 1, 2, 0, 'top-23', '2018-01-13 03:49:37', '2018-01-13 03:49:37'),
(59, 4, '24', '<p>24</p>', 1, 2, 0, 'top-24', '2018-01-13 03:49:40', '2018-01-13 03:49:40'),
(60, 4, '25', '<p>25</p>', 1, 2, 0, 'top-25', '2018-01-13 03:49:43', '2018-01-13 03:49:43'),
(61, 4, '26', '<p>26</p>', 1, 2, 0, 'top-26', '2018-01-13 03:49:46', '2018-01-13 03:49:46'),
(62, 4, '27', '<p>27</p>', 1, 2, 0, 'top-27', '2018-01-13 03:49:50', '2018-01-13 03:49:50'),
(63, 4, '28', '<p>28</p>', 1, 2, 0, 'top-28', '2018-01-13 03:50:01', '2018-01-13 03:50:01'),
(64, 4, '28', '<p>28</p>', 1, 2, 0, 'top-28-1', '2018-01-13 03:50:01', '2018-01-13 03:50:01'),
(65, 4, '29', '<p>29</p>', 1, 2, 0, 'top-29', '2018-01-13 03:50:08', '2018-01-13 03:50:08'),
(66, 4, 'Title Example', '<p>30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf30asfdsfad asf</p>', 1, 2, 0, 'top-30', '2018-01-13 03:50:14', '2018-01-15 12:29:30'),
(67, 6, '1', '<p>2</p>', 1, 2, 0, 'top-1-1', '2018-01-13 03:51:34', '2018-01-13 03:51:34'),
(68, 4, 'Topic Title', '<p>Topic OP content&nbsp;Topic OP content&nbsp;Topic OP content&nbsp;Topic OP content&nbsp;Topic OP content&nbsp;Topic OP content&nbsp;Topic OP content&nbsp;Topic OP content&nbsp;Topic OP content&nbsp;Topic OP content&nbsp;Topic OP content&nbsp;</p>', 1, 3, 0, 'top-test-3', '2018-01-13 09:54:39', '2018-01-15 13:21:58'),
(69, 6, 'test', '<p>test</p>', 1, 1, 0, 'top-test-4', '2018-01-14 11:02:27', '2018-01-14 11:02:27'),
(70, 4, 'asdfas', '<p>asdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasfasdfasf</p>', 1, 1, 0, 'top-asdfas-6', '2018-01-23 21:48:05', '2018-01-23 21:48:05'),
(71, 4, 'TEST long', '<p>lkjsalkdfjslkadfjsaldkfjsaldkfjsadlfjsadlkfjsadlfkjsadlkfjsadlkfjsadlkfjsadlkfjasdlkfjasldkfjsadlkfjlsdkafjalskdfjlsadkfjasldkfjsdalkfjasldkfjsaldkfjsaldkfjasldkflkjsalkdfjslkadfjsaldkfjsaldkfjsadlfjsadlkfjsadlfkjsadlkfjsadlkfjsadlkfjsadlkfjasdlkfjasldkfjsadlkfjlsdkafjalskdfjlsadkfjasldkfjsdalkfjasldkfjsaldkfjsaldkfjasldkflkjsalkdfjslkadfjsaldkfjsaldkfjsadlfjsadlkfjsadlfkjsadlkfjsadlkfjsadlkfjsadlkfjasdlkfjasldkfjsadlkfjlsdkafjalskdfjlsadkfjasldkfjsdalkfjasldkfjsaldkfjsaldkfjasldkflkjsalkdfjslkadfjsaldkfjsaldkfjsadlfjsadlkfjsadlfkjsadlkfjsadlkfjsadlkfjsadlkfjasdlkfjasldkfjsadlkfjlsdkafjalskdfjlsadkfjasldkfjsdalkfjasldkfjsaldkfjsaldkfjasldkf</p>', 1, 1, 0, 'top-test-long', '2018-01-24 18:37:22', '2018-01-24 18:37:22'),
(72, 21, 'test', '<p>test</p>', 1, 1, 0, 'top-test-5', '2018-01-25 13:38:55', '2018-01-25 13:38:55'),
(73, 22, 'test2', '<p>test2</p>', 1, 1, 0, 'top-test2', '2018-01-25 13:39:01', '2018-01-25 13:39:01'),
(74, 21, 'test', '<p>test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2test2</p>', 1, 1, 0, 'top-test-6', '2018-01-25 13:43:38', '2018-01-25 13:43:38'),
(75, 26, 'test', '<p>test</p>', 1, 1, 0, 'top-test-7', '2018-01-25 20:20:53', '2018-01-25 20:20:53'),
(76, 23, 'F', NULL, 1, 1, 0, 'top-f', '2018-01-27 00:49:20', '2018-01-27 00:49:20'),
(77, 25, 'test', '<p>test</p>', 1, 1, 0, 'top-test-8', '2018-01-28 07:55:48', '2018-01-28 07:55:48'),
(78, 22, 'lkjlkj', '<p><span class="math-tex">\\(x = {-b \\pm \\sqrt{b^2-4ac} \\over 2a}\\)</span></p>', 1, 1, 0, 'top-lkjlkj', '2018-01-28 10:34:03', '2018-01-28 10:34:03');

-- --------------------------------------------------------

--
-- Table structure for table `user_levels`
--

CREATE TABLE `user_levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_levels`
--

INSERT INTO `user_levels` (`id`, `name`) VALUES
(1, 'Super Admin'),
(2, 'Admin'),
(3, 'Author'),
(4, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(65) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reg_token_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `level`, `status`, `remember_token`, `reg_token_id`, `created_at`, `updated_at`) VALUES
(1, 'Super', 'Admin', 'super@user.com', '$2y$10$oywnXi2VooYDW3oLV5gjJe/06rHjRm7LBYS8WAKX/XvvrxMqepsjK', 1, 0, 'XQCV4Uw0uoruinLh3Go0e8UL8bpCIWyXGWNBcJ3WhTD06LCTA6lzJsXwS8wt', '', NULL, NULL),
(4, 'User', 'user', 'user@user.com', '$2y$10$kkX9m7/iZSgpcjSCUhJLyu4weEdSqlI0ioBsYm4/jEdP0iRP6C7Cy', 4, 0, NULL, '', NULL, '2018-01-28 18:59:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_chapters`
--
ALTER TABLE `book_chapters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_sections`
--
ALTER TABLE `book_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_subsections`
--
ALTER TABLE `book_subsections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `registration_tokens`
--
ALTER TABLE `registration_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `replies`
--
ALTER TABLE `replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_levels`
--
ALTER TABLE `user_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_chapters`
--
ALTER TABLE `book_chapters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `book_sections`
--
ALTER TABLE `book_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `book_subsections`
--
ALTER TABLE `book_subsections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `registration_tokens`
--
ALTER TABLE `registration_tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `replies`
--
ALTER TABLE `replies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `user_levels`
--
ALTER TABLE `user_levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
