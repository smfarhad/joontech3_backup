
			
			function addClassToTree(nodeArray, data){
				for (var i = 0; i < nodeArray.length; i++) {
					if(nodeArray[i].title == data.title){
						data.addClass(nodeArray[i]["extra_class"]);
						$(data.li).attr('element_id', nodeArray[i]['element_id']);			
						break;
					} else {
						if(nodeArray[i]["children"] != undefined){
							addClassToTree(nodeArray[i]["children"], data);
						}
					}
				}
			}
