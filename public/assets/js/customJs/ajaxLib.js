function makeAjaxCall(url, data, method, successCallback, failureCallback){
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });

    $.ajax({
        url: url, // point to server-side PHP script
        data: data,
        type: method,
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        processData: false,
        success: successCallback,
        error: failureCallback
    });
}

function getFormData(data){
    var form_data = new FormData();
    form_data.append('data', JSON.stringify(data));
    return form_data;
}

function sendAjaxRequest(url, data, method, successCallback, failureCallback){
    var ajaxData = getFormData(data);
    makeAjaxCall(url, ajaxData, method, successCallback, failureCallback);
}
