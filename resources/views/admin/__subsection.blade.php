@extends('admin/base')

@section('title')
    Subsection
@stop

@section('css-links')

@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/touch.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script>
        $(function() {
            $subSectionIds = JSON.parse("{{ $section->subSections->pluck('id') }}");
            last_id = $subSectionIds.slice(-1).pop();
            $editor_id = ( last_id == 'undefined' ) ? 1 : last_id;
            
            $.each($subSectionIds, function(i,v){
                init_ckeditor('editor-'+v);
            });
            
            if ( CKEDITOR.env.ie && CKEDITOR.env.version == 8 ) {
                document.getElementById( 'ie8-warning' ).className = 'tip alert';
            }

            $('#subsection-add').click(function(){
                $editor_id++;
                ckeditor_field('editor-'+$editor_id);                
            });

            $('#subsection-submit').click(function(){
                $('#subsection-form').submit();
            });

            function init_ckeditor(element){
                CKEDITOR.replace( element, {
                    extraPlugins: 'mathjax',
                    mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML',
                    height: 320
                });
            }

            function ckeditor_field(element){
                $('#subsection-content').append(
                    `<div class="panel panel-white">
                        <div class="panel-heading">
                            <div class="title-editable">
                                <div class="title-display">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="title-input form-control" name="title[][new]" placeholder="Input title here">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                    <li><a data-action="move"></a></li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="panel-body">
                            <textarea class="ckclass" cols="10" id="`+element+`" name="editor[][new]" rows="10" data-sample="1" data-sample-short="">
                                
                            </textarea>
                        </div>
                    </div>`);
                init_ckeditor(element);
            }

            $(".row-sortable").sortable({
                connectWith: '.row-sortable',
                items: '.panel',
                helper: 'original',
                cursor: 'move',
                handle: '[data-action=move]',
                revert: 200,
                containment: '.content-wrapper',
                forceHelperSize: true,
                placeholder: 'sortable-placeholder',
                forcePlaceholderSize: true,
                tolerance: 'pointer',
                start: function(e, ui){
                    ui.placeholder.height(ui.item.outerHeight());
                }
            });
        });
    </script>
@stop

@section('header')
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><i class="icon-book position-left"></i> {{ $section->book->title }}</li>
                <li class="active">{{ $section->chapter->title }}</li>
                <li class="active">{{ $section->title }}</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->
@stop

@section('content')
<form id="subsection-form" method="POST" action="{{ route('subsectionAction') }}">
    {{ csrf_field() }}
    <input type="hidden" name="subsection_id" value="{{ $section->id }}">
    <div id="subsection-content" class="row row-sortable">

        @foreach ( $section->subSections as $subsection )
        <div class="panel panel-white">
            <div class="panel-heading">
                <div class="title-editable">
                    <div class="title-display">
                        <div class="row">
                            <div class="col-md-8">
                                <input type="text" class="title-input form-control" name="title[][{{ $subsection->id }}]" placeholder="Input title here" value="{{ $subsection->title }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="move"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <textarea cols="10" id="editor-{{ $subsection->id }}" name="editor[][{{ $subsection->id }}]" rows="10" data-sample="1" data-sample-short="">
                    {{ $subsection->data }}
                </textarea>
            </div>
        </div>
        @endforeach

        {{--@foreach ( $section->subSections as $subsection )
        <div class="panel panel-white">
            <div class="panel-heading">
                <div class="title-editable">
                    <div class="title-display">
                        <div class="row">
                            <div class="col-md-8">
                                <input type="text" class="title-input form-control" name="title[][{{ $subsection->id }}]" placeholder="Input title here" value="{{ $subsection->title }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        --}}{{--<li><a data-action="move"></a></li>--}}{{--
                    </ul>
                </div>
            </div>
            
            <div class="panel-body">
                <textarea cols="10" id="editor-{{ $subsection->id }}" name="editor[][{{ $subsection->id }}]" rows="10" data-sample="1" data-sample-short="">
                    {{ $subsection->data }}
                </textarea>
            </div>
        </div>
        @endforeach--}}
    </div>
</form>
@stop

@section('action-footer')
    <div class="action-footer">
        <div class="text-left">
            <button id="subsection-add" type="button" class="btn bg-teal-400 btn-labeled btn-labeled-right legitRipple"><b><i class="icon-add"></i></b> Add Subsection</button>
            <button id="subsection-submit" type="button" class="btn bg-primary btn-labeled btn-labeled-right legitRipple pull-right"><b><i class="icon-circle-right2"></i></b> Save Subsection</button>
        </div>
    </div>
@stop