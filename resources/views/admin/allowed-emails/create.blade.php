<?php
/**
 * Created by PhpStorm.
 * User: msherax
 * Date: 2/19/18
 * Time: 5:00 PM
 */
?>

@extends('admin/base');

@section('title')
    Add Email Patter
@stop

@section('content')
    <div class="panel panel-flat">

        <div class="panel-heading">
            <h5 class="panel-title">Add New Email Pattern<a class="heading-elements-toggle"><i
                            class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'route' => 'storeEmail']) !!}

            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <fieldset class="content-group">
                <div class="form-group">
                    <label class="control-label col-lg-2">Email Patttern</label>
                    <div class="col-lg-10">
                        <input class="form-control" type="text" name="email" id="email">
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                {!! Form::submit('submit', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('action-footer')
    {{-- Full width footer --}}
@stop