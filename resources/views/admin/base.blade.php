@include('admin.layouts.head')

<body class="navbar-bottom">

<!-- Page header -->
<div class="page-header page-header-inverse">
    @include('admin.layouts.navbar')

    @include('admin.layouts.top-navbar')
</div>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper p-relative">

        @yield('header')

        <!-- Content area -->
            <div class="content">
                @yield('content')
            </div>

        @yield('action-footer')

        <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
