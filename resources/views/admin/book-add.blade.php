@extends('admin/base')

@section('title')
    Book Management
@stop

@section('css-links')
    {{-- Stylesheet links --}}
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/core.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/extensions/cookie.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_childcounter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/uploaders/fileinput/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/uploader_bootstrap.js') }}"></script>
    <!-- /theme JS files -->

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // Drag and drop support
            $(".tree-drag").fancytree({
                extensions: ["dnd","edit"],
                source: [
                    {"title": "Chapter 1", "extraClasses": 'tree-chapter', "expanded": true, "folder": true, "children": [
                        {"key": "1_1", "title": "Section 1.1", "extraClasses": "tree-section", "expanded": true, "children": [
                            {"key": "1_1_1", "title": "Sub-section 1.1.1", "extraClasses": "tree-subSection", "children": [
                                {"key": "1_1_1_1", "title": "General", "extraClasses": "tree-forum"}
                            ]},
                            {"key": "1_1_2", "title": "Sub-section 1.1.2", "extraClasses": "tree-subSection", "children": [
                                {"key": "1_1_2_1", "title": "General", "extraClasses": "tree-forum"},
                            ]}
                        ]},
                    ]},
                    {"title": "Chapter 2", "extraClasses": 'tree-chapter', "expanded": true, "folder": true, "children": [
                        {"key": "2_1", "title": "Section 2.1", "extraClasses": "tree-section", "children": [
                            {"key": "2_1_1", "title": "Sub-section 2.1.1", "extraClasses": "tree-subSection", "children": [
                                {"key": "2_1_1_1", "title": "General Forum", "extraClasses": "tree-forum"}
                            ]}
                        ]}
                    ]},
                ],
                dnd: {
                    autoExpandMS: 400,
                    focusOnClick: true,
                    preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
                    preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
                    dragStart: function(node, data) {
                        return true;
                    },
                    dragEnter: function(node, data) {
                        return true;
                    },
                    dragDrop: function(node, data) {
                        var span = (data.hitMode == 'over') ? $(data.node.span) : $(data.node.parent.span);
                        console.log('node',node);
                        console.log('data',data);
                        if( span != 'undefined' ){
                            var parent_type = getType(span);
                            var item_type = getType(data.otherNode.span);
                            if( (item_type - parent_type) == 1 || (parent_type==0&&item_type==0) ){
                                // This function MUST be defined to enable dropping of items on the tree.
                                data.otherNode.moveTo(node, data.hitMode);
                            }
                        }


                        function getType(span){
                            span = $(span);
                            if( span.hasClass('tree-chapter') ){
                                return 1;
                            }else if( span.hasClass('tree-section') ){
                                return 2;
                            }else if( span.hasClass('tree-subSection') ){
                                return 3;
                            }else{
                                return 0;
                            }
                        }
                    }
                },
                init: function(event, data) {
                    $('.has-tooltip .fancytree-title').tooltip();
                },
                edit: {
                    adjustWidthOfs: 0,
                    inputCss: {minWidth: "0"},
                    triggerStart: ["f2", "dblclick", "shift+click", "mac+enter"]
                }
            });

            $('#add-subsection').click(function(){
                var node = $("#book-tree").fancytree("getActiveNode");
                var node_section = (node) ? $(node.span).hasClass('tree-section') : false;
                if( !node || !node_section ) {
                    swal("Opps!", "Please select or create a section first.", "error");
                    return;
                }
                node.editCreateNode("child", {
                    title: 'New subSection',
                    extraClasses: 'tree-subSection'
                });
            });

            $('#add-forum').click(function(){
                console.info('function called');
                var node = $("#book-tree").fancytree("getActiveNode");
                var node_section = (node) ? $(node.span).hasClass('tree-subSection') : false;
                if( !node || !node_section ) {
                    swal("Opps!", "Please select or create a subsection first.", "error");
                    return;
                }
                node.editCreateNode("child", {
                    title: 'New Forum',
                    extraClasses: 'tree-forum',
                    'key' : 'forum'
                });
            });


            $('#add-section').click(function(){
                var node = $("#book-tree").fancytree("getActiveNode");
                var node_chapter = (node) ? $(node.span).hasClass('tree-chapter') : false;
                if( !node || !node_chapter ) {
                    swal("Opps!", "Please select or create a chapter first.", "error");
                    return;
                }
                node.editCreateNode("child", {
                    title: 'New section',
                    extraClasses: 'tree-section'
                });
            });

            $('#add-chapter').click(function(){
                var node = $("#book-tree").fancytree("getActiveNode");
                var node_chapter = (node) ? $(node.span).hasClass('tree-chapter') : false;
                if( !node_chapter ) {
                    swal("Opps!", "Please select or create a chapter first.", "error");
                    return;
                }
                node.editCreateNode("before", {
                    title: 'New chapter',
                    extraClasses: 'tree-chapter'
                });
            });

            $('#create-book').click(function(){
                var tree = $("#book-tree").fancytree("getTree");
                var data = tree.toDict(true);
                data['title'] = $('#book-title').val();

                console.info('data: ', data);
                //return false;
                if( data['title'] == '' ){
                    swal("Hey", "You need a title for this book", "error");
                }else{

                    //console.info('data:', data);
                    let form_data = new FormData();
                    var file_data = $('#book_image').prop('files')[0];
                    form_data.append('file', file_data);
                    form_data.append('data', JSON.stringify(data));

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-Token': $('meta[name=_token]').attr('content')
                        }
                    });

                    $.ajax({
                        url: "{{ route('bookCreate') }}", // point to server-side PHP script
                        data: form_data,
                        type: 'POST',
                        contentType: false, // The content type used when sending data to the server.
                        cache: false, // To unable request pages to be cached
                        processData: false,
                        success: function(data) {
                            /*let _msg = $.parseJSON(data);
                            console.info('inside on success');
                            console.info('_msg: ', _msg);*/
                            $.jGrowl("Book Created Successfully", {
                                sticky: true,
                                theme : 'bg-success',
                                animateClose : { opacity: 'hide' },
                                life : 3000
                            });
                        },
                        error: function (request, status, error) {
                            console.info('inside on error');
                            let response = $.parseJSON(request.responseText);
                            let msg = response.errors.file;
                            msg = msg.join('<br>');
                            console.info(request.responseText, status, msg, response.errors.file);
                            $.jGrowl(msg, {
                                sticky: true,
                                theme : 'bg-danger',
                                animateClose : { opacity: 'hide' },
                                life : 3000
                            });
                        }
                    });
                }
            });

            $('#remove-node').click(function(){
                var node = $("#book-tree").fancytree("getActiveNode");
                node.remove();
            });
        });
    </script>
@stop

@section('header')
    <div class="page-header page-header-default page-header-xs">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Book</span> - Add new</h5>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="row">
                <div class="col-lg-8 mb-15">
                    <input id="book-title" name="title" type="text" class="form-control" placeholder="Enter book title">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 mb-15">
                    <input type="file" class="file-input" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-xs" data-remove-class="btn btn-default btn-xs" id="book_image" name="book_image">
                     <span class="help-block">Select Book image</span>
                </div>
            </div>
        </div>

        <div class="panel-body">
            <div id="book-tree" class="tree-drag well border-left-info border-left-lg"></div>
        </div>
    </div>


@stop

@section('action-footer')
    <div class="action-footer-fix">
        <div class="row">
            <div class="text-left col-sm-8 mt-10 mb-10">
                <div class="btn-group">
                    <button id="add-chapter" type="button" class="btn btn-success legitRipple">Chapter</button>
                    <button id="add-section" type="button" class="btn btn-success legitRipple">Section</button>
                    <button id="add-subsection" type="button" class="btn btn-success legitRipple">Subsection</button>
                    <button id="add-forum" type="button" class="btn btn-success legitRipple">Forum</button>

                </div>
                <button id="remove-node" type="button" class="btn btn-danger legitRipple">Remove</button>
            </div>
            <div class="text-right col-sm-4 mt-10 mb-10">
                <button id="create-book" type="button" class="btn bg-primary btn-labeled btn-labeled-right legitRipple pull-right"><b><i class="icon-circle-right2"></i></b> Create Book</button>
            </div>
        </div>
    </div>
@stop
