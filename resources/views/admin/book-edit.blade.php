@extends('admin/base')

@section('title')
    Book Edit
@stop

@section('css-links')
    {{-- Stylesheet links --}}
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/core.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/extensions/cookie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/custom/tree_helper.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_childcounter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>

    <!-- Theme JS files -->
    <script type="text/javascript"
            src="{{ asset('assets/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
    <script type="text/javascript"
            src="{{ asset('assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/uploaders/fileinput/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/uploader_bootstrap.js') }}"></script>
    <!-- /theme JS files -->

    <script>
        $(function () {
            let bookImage = '{{ asset('images/'.$book->image) }}';

            $("#book_image").fileinput({
                initialPreview: [bookImage],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                    {caption: "", downloadUrl: bookImage, size: 930321, width: "120px", key: 1}
                ],
                deleteUrl: "{{ route('deleteBook') }}",
                overwriteInitial: true,
                showUpload: false,
                showRemove: false,
                initialPreviewShowDelete: false,
                maxFileSize: 100,
                initialCaption: "{{$book->image}}"
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var arrayData = {!! $bookTree !!};
            loadFancyTree(arrayData);

            function loadFancyTree(arrayData){
            // Drag and drop support
		        $(".tree-drag").fancytree({
		            extensions: ["dnd", "edit"],
		            dnd: {
		                autoExpandMS: 400,
		                focusOnClick: true,
		                preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
		                preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
		                dragStart: function (node, data) {
		                    return true;
		                },
		                dragEnter: function (node, data) {
		                    return true;
		                },
		                dragDrop: function (node, data) {
		                    var span = (data.hitMode == 'over') ? $(data.node.span) : $(data.node.parent.span);
		                    console.log('node', node);
		                    console.log('data', data);
		                    if (span != 'undefined') {
		                        var parent_type = getType(span);
		                        var item_type = getType(data.otherNode.span);
		                        if ((item_type - parent_type) == 1) {
		                            // This function MUST be defined to enable dropping of items on the tree.
		                            data.otherNode.moveTo(node, data.hitMode);
		                        }
		                    }

		                    function getType(span) {
		                        span = $(span);
		                        if (span.hasClass('tree-chapter')) {
		                            return 1;
		                        } else if (span.hasClass('tree-section')) {
		                            return 2;
		                        } else if (span.hasClass('tree-subSection')) {
		                            return 3;
		                        }  else if (span.hasClass('tree-forum')) {
		                            return 4;
                                } else {
		                            return 0;
		                        }
		                    }
		                }
		            },
		            init: function (event, data) {
		                $('.has-tooltip .fancytree-title').tooltip();
		            },
		            edit: {
		                adjustWidthOfs: 0,
		                inputCss: {minWidth: "0"},
		                triggerStart: ["f2", "dblclick", "shift+click", "mac+enter"]
		            },
		            source: arrayData,
		            renderNode: function(event, data) {
					  setTimeout(function() {
					  		addClassToTree(arrayData, data.node);
					  }, 100);
				   }
		        });
            }

            $('#add-subsection').click(function () {
                var node = $("#book-tree").fancytree("getActiveNode");
                var node_section = (node) ? $(node.span).hasClass('tree-section') : false;
                if (!node || !node_section) {
                    swal("Opps!", "Please select or create a section first.", "error");
                    return;
                }
                var childNode = node.addChildren({
                    title: 'New subSection',
                    extraClasses: 'tree-subSection'
                });

                childNode.addChildren({
                    title: "General",
                    extraClasses: 'tree-forum',
                    'key': 'forum'
                });
            });

            $('#add-forum').click(function () {
                var node = $("#book-tree").fancytree("getActiveNode");
                var node_section = (node) ? $(node.span).hasClass('tree-subSection') : false;
                if (!node || !node_section) {
                    swal("Opps!", "Please select or create a subsection first.", "error");
                    return;
                }
                node.editCreateNode("child", {
                    title: 'General',
                    extraClasses: 'tree-forum',
                    'key': 'forum'
                });
            });

            $('#add-section').click(function () {
                var node = $("#book-tree").fancytree("getActiveNode");
                var node_chapter = (node) ? $(node.span).hasClass('tree-chapter') : false;
                if (!node || !node_chapter) {
                    swal("Opps!", "Please select or create a chapter first.", "error");
                    return;
                }
                node.editCreateNode("child", {
                    title: 'New section',
                    extraClasses: 'tree-section'
                });
            });

            $('#add-chapter').click(function () {
                var node = $("#book-tree").fancytree("getActiveNode");
                var node_chapter = (node) ? $(node.span).hasClass('tree-chapter') : false;
                if (!node_chapter) {
                    swal("Opps!", "Please select or create a chapter first.", "error");
                    return;
                }
                node.editCreateNode("after", {
                    title: 'New chapter',
                    extraClasses: 'tree-chapter'
                });
            });

            $('#update-book').click(function () {
                var tree = $("#book-tree").fancytree("getTree");
                var data = tree.toDict(true);
                data['key'] = '{{ $book->id }}';
                data['title'] = $('#book-title').val();

                /*$.post("{{ route('bookUpdate') }}",{data})
                 .done(function(res){
                 swal("Great", "Book is now updated", "success");
                 })
                 .fail(function() {
                 swal("Opps!", "Something went wrong, Please try again", "error");
                 })*/

                if (data['title'] == '') {
                    swal("Hey", "You need a title for this book", "error");
                } else {

                    //console.info('data:', data);
                    let form_data = new FormData();
                    var file_data = $('#book_image').prop('files')[0];
                    form_data.append('file', file_data);
                    form_data.append('data', JSON.stringify(data));

                    $.ajax({
                        url: "{{ route('bookUpdate') }}", // point to server-side PHP script
                        data: form_data,
                        type: 'POST',
                        contentType: false, // The content type used when sending data to the server.
                        cache: false, // To unable request pages to be cached
                        processData: false,
                        success: function (data) {
                            /*let _msg = $.parseJSON(data);
                             console.info('inside on success');
                             console.info('_msg: ', _msg);*/
                            $.jGrowl("Book Updated Successfully", {
                                sticky: true,
                                theme: 'bg-success',
                                animateClose: {opacity: 'hide'},
                                life: 3000
                            });

							location.reload();
                            //loadFancyTree(JSON.parse(data));
                            /*$("#tree").fancytree({
							  source: $.ajax({
								url: "/clubjudge/api/JSONClassTree2",
								dataType: "json"
							  })
							});*/
                        },
                        error: function (request, status, error) {
                            console.info('inside on error');
                            let response = $.parseJSON(request.responseText);
                            let msg = response.errors.file;
                            msg = msg.join('<br>');
                            console.info(request.responseText, status, msg, response.errors.file);
                            $.jGrowl(msg, {
                                sticky: true,
                                theme: 'bg-danger',
                                animateClose: {opacity: 'hide'},
                                life: 3000
                            });
                        }
                    });
                }

            });

            $('#remove-node').click(function () {
                var node = $("#book-tree").fancytree("getActiveNode");
                console.info('node: ', node, node.key);

                swal({
                    title: "Are you sure about Deleting it?",
                    text: "You will not be able to recover this Category!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel pls!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if(node.data.element_id === undefined){
                        node.remove();
                        return;
                    }
                    if (isConfirm) {
                        $.post("{{ route('deleteNode') }}", {id: node.data.element_id, extra_class: node.data.extra_class}, function (response) {
                            console.info('response: ', response);
                            node.remove();
                        });
                        $.jGrowl("Node Deleted Successfully", {
                            sticky: true,
                            theme: 'bg-success',
                            animateClose: {opacity: 'hide'},
                            life: 3000
                        });
                    }
                    else {
                        /*swal({
                            title: "Cancelled",
                            text: "Your Category is safe :)",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });*/
                    }
                });
            });

            $('#remove-book').on('click', function () {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this book!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel pls!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post("{{ route('deleteBook') }}", {id: "{{ $book->id }}"}, function () {
                            window.location = '{{ route('listBooks') }}';
                        });
                    }
                    else {
                        swal({
                            title: "Cancelled",
                            text: "Your book is safe :)",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
            });
        });
    </script>
@stop


@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="row">
                <div class="col-lg-8 mb-15">
                    <input
                            id="book-title"
                            name="title"
                            type="text"
                            class="form-control"
                            placeholder="Enter book title"
                            value="{{ $book->title }}">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 mb-15">
                    <input type="file" id="book_image" name="book_image">

                    <span class="help-block">Select Book image</span>
                </div>
            </div>
        </div>

        <div class="panel-body">

            <div id="book-tree" class="tree-drag well border-left-info border-left-lg">

            </div>
        </div>
    </div>

@stop

@section('action-footer')
    <div class="action-footer-fix">
        <div class="row">
            <div class="text-left col-sm-6 mt-10 mb-10">
                <div class="btn-group">
                    <button id="add-chapter" type="button" class="btn btn-success btn-xs legitRipple">Chapter</button>
                    <button id="add-section" type="button" class="btn btn-success btn-xs legitRipple">Section</button>
                    <button id="add-subsection" type="button" class="btn btn-success btn-xs legitRipple">Subsection
                    </button>
                    <button id="add-forum" type="button" class="btn btn-success btn-xs legitRipple">Forum</button>

                </div>
                <button id="remove-node" type="button" class="btn btn-danger btn-xs legitRipple">Remove</button>
            </div>
            <div class="text-right col-sm-6 mt-10 mb-10">
                <button id="update-book" type="button" class="btn bg-primary btn-labeled legitRipple pull-right btn-xs">
                    <b><i class="icon-circle-right2"></i></b> Update Book
                </button>
                <button id="remove-book" type="button"
                        class="btn bg-danger btn-labeled legitRipple pull-right btn-xs mr-15"><b><i
                                class="icon-trash"></i></b> Remove Book
                </button>
            </div>
        </div>
    </div>
@stop
