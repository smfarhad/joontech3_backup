@extends('admin/base')

@section('title')
    {{-- Title here --}}
@stop

@section('css-links')
    {{-- Stylesheet links --}}
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/core.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/extensions/cookie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_childcounter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_filter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>

    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var tree_data = $(".tree-default").fancytree({
                extensions: ['filter'],
                quicksearch: true,
                init: function (event, data) {
                    $('.has-tooltip .fancytree-title').tooltip();
                },
                activate: function (event, data) {
                    var link = data.node.data.link;
                    if (link) {
                        window.location = link;
                    }
                },
            });

            var tree = $("#book-tree").fancytree("getTree");

            $('#expand-collapse').on('click', function () {
                icon = $(this).find('i');
                if ($(icon).hasClass('icon-arrow-down5')) {
                    tree.visit(function (node) {
                        node.setExpanded();
                    });
                    $(icon).removeClass('icon-arrow-down5').addClass('icon-arrow-up5')
                } else {
                    tree.visit(function (node) {
                        node.setExpanded(false);
                    });
                    $(icon).removeClass('icon-arrow-up5').addClass('icon-arrow-down5')
                }
            });

            $("input#filter").keyup(function (e) {
                var n,
                        tree = $.ui.fancytree.getTree(),
                        args = "autoApply autoExpand fuzzy hideExpanders highlight leavesOnly nodata".split(" "),
                        filterFunc = $("#branchMode").is(":checked") ? tree.filterBranches : tree.filterNodes,
                        match = $(this).val();

                if (match != '') {
                    $('#btn-reset-filter').removeClass('hidden');
                }

                opts = {
                    autoApply: true,   // Re-apply last filter if lazy data is loaded
                    autoExpand: true, // Expand all branches that contain matches while filtered
                    counter: true,     // Show a badge with number of matching child nodes near parent icons
                    fuzzy: false,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                    hideExpandedCounter: true,  // Hide counter badge if parent is expanded
                    hideExpanders: false,       // Hide expanders if all child nodes are hidden by filter
                    highlight: true,   // Highlight matches by wrapping inside <mark> tags
                    leavesOnly: false, // Match end nodes only
                    nodata: true,      // Display a 'no data' status node if result is empty
                    mode: "hide"       // Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
                };

                if (e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === "") {
                    $("button#btnResetSearch").click();
                    return;
                }
                if ($("#regex").is(":checked")) {
                    // Pass function to perform match
                    n = filterFunc.call(tree, function (node) {
                        return new RegExp(match, "i").test(node.title);
                    }, opts);
                } else {
                    // Pass a string to perform case insensitive matching
                    n = filterFunc.call(tree, match, opts);
                }
                $("#btn-reset-filter").attr("disabled", false);
                $("span#matches").text(+n + " matches");
            }).focus();

            $("#btn-reset-filter").click(function (e) {
                $("input#filter").val("");
                $("span#matches").text("");
                tree.clearFilter();
                $(this).addClass('hidden');
            }).attr("disabled", true);

            $("fieldset input:checkbox").change(function (e) {
                var id = $(this).attr("id"),
                        flag = $(this).is(":checked");

                // Some options can only be set with general filter options (not method args):
                switch (id) {
                    case "counter":
                    case "hideExpandedCounter":
                        tree.options.filter[id] = flag;
                        break;
                }
                tree.clearFilter();
                $("input[name=search]").keyup();
            });

        });
    </script>
@stop

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="row">
                <div class="col-lg-8 mb-15">
                    <div class="filter-box">
                        <div class="input-group bootstrap-touchspin">
                            <input id="filter" autocomplete="off" name="title" type="text" class="form-control"
                                   placeholder="Filter...">
                            <span class="input-group-btn">
                                <button id="btn-reset-filter" type="button"
                                        class="btn border-danger text-danger-600 btn-flat btn-icon legitRipple btn-xs hidden"><i
                                            class="icon-x"></i></button>
                            </span>
                        </div>
                        <span id="matches" class="badge badge-flat border-teal text-teal-600"></span>
                    </div>
                </div>
            </div>
            <div class="heading-elements">
                <button id="expand-collapse" type="button" class="btn btn-primary btn-icon legitRipple btn-xs">
                    <i class="icon-arrow-down5"></i>
                </button>
            </div>

        </div>

        <div class="panel-body">
            <div id="book-tree" class="tree-default well border-left-teal border-left-lg">
                <ul>{!! $treeMenu !!}</ul>
            </div>
            <!-- {{--<div id="book-tree" class="tree-default well border-left-teal border-left-lg">
                <ul>
                @if( $book )
                @foreach ($book->chapters as $chapter)
                    <li class="folder expanded tree-chapter">
                        {{ $chapter->title }}
                    @if( $chapter->sections->isNotEmpty() )
                        <ul>
                        @foreach( $chapter->sections as $section )
                            <li data-link="{{ url('/admin/book/subsection').'/'.$section->id }}" id="{{ $section->id }}" class="tree-section">{{ $section->title }}
                            @if( $section->subSections->isNotEmpty() )
                                <ul>
                                @foreach( $section->subSections as $subsection )
                                    <li data-link="{{ url('/admin/book/subsection').'/'.$section->id }}" id="{{ $subsection->id }}" class="tree-subSection">{{ $subsection->title }}</li>
                                @endforeach
                                </ul>
                            @endif
                            </li>
                        @endforeach
                        </ul>
                    @endif
                    </li>
                @endforeach
                @endif
                </ul>
            </div>--}} -->
        </div>
    </div>
@stop

@section('action-footer')
    {{-- Full width footer --}}
@stop
