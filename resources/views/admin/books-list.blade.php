@extends('admin/base')

@section('title')
    {{-- Title here --}}
@stop

@section('css-links')
    {{-- Stylesheet links --}}
@stop

@section('javascript')
    {{-- Script Links and Javascript --}}
@stop

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            @if (Session::has('book-delete'))
                <div class="heading-elements">
                    <h5 class="text-success"> {{ Session::get('book-delete') }} </h5>
                </div>
            @endif
        </div>
    </div>
@stop

@section('content')
    <div class="books">
        <div class="row">
            @foreach ( $books as $book )
                <div class="col-lg-2 col-sm-2">
                    <div class="thumbnail">
                        <div class="thumb">
                            <img src="{{ asset('images/'.bookImage($book->image)) }}">
                            <div class="caption-overflow">
                        <span>
                            <a href="admin/book-edit/{{ $book->id }}"
                               class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i
                                        class="icon-pencil"></i></a>
                            <a href="admin/book-view/{{ $book->id }}"
                               class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i
                                        class="icon-link2"></i></a>
                        </span>
                            </div>
                        </div>

                        <div class="caption">
                            <h6 class="no-margin-top text-semibold"><a href="admin/book-view/{{ $book->id }}"
                                                                       class="text-default">{{ $book->title }}</a></h6>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-xs-12 text-center mt-20">
                {{ $books->links() }}
            </div>
        </div>

        <div class="row" style="margin-top:30px;">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"></h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="tabbable tab-content-bordered">
                            <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
                                <li class="active"><a href="#bordered-justified-tab1" data-toggle="tab">New Posts</a>
                                </li>
                                <li><a href="#bordered-justified-tab2" data-toggle="tab">Your Threads</a></li>
                                <li><a href="#bordered-justified-tab3" data-toggle="tab">Threads With Your Posts</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane has-padding active" id="bordered-justified-tab1">
                                    To use in tabs with equal widths add <code>.nav-justified</code> and <code>.tab-content-bordered</code>
                                    to the parent container.
                                </div>

                                <div class="tab-pane has-padding" id="bordered-justified-tab2">
                                    @if($userThreads)
                                        @foreach($userThreads as $thread)
                                            <div class="row">
                                                <div class="col-md-3">
                                                    {{ $thread->title }}
                                                </div>
                                            </div>
                                        @endforeach

                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ $userThreads->links() }}
                                            </div>
                                        </div>


                                    @endif
                                </div>

                                <div class="tab-pane has-padding" id="bordered-justified-tab3">
                                    DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork.
                                    Williamsburg whatever.
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@stop

@section('action-footer')
    {{-- Full width footer --}}
@stop