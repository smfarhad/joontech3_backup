<?php
/**
 * Created by PhpStorm.
 * User: msherax
 * Date: 2/21/18
 * Time: 12:17 PM
 */?>

@extends('admin/base')

@section('title')
    Books List
@stop

@section('css-links')
    {{-- Stylesheet links --}}
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>

    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Table setup
            // ------------------------------

            // Setting datatable defaults
            $.extend( $.fn.dataTable.defaults, {
                autoWidth: false,
                columnDefs: [
                    {
                        orderable: false,
                        width: '100px',
                        targets: [ 0 ],
                        className: 'text-center',
                        render: function ( data, type, row, meta ) {
                            //console.info(data, type, row, meta);
                            let image = '<img src="{{ asset('images') }}/'+data+'" width="50">';
                            return image;
                        }
                    },
                    {
                        orderable: false,
                        width: '100px',
                        targets: [ 3 ],
                        className: 'text-center',
                        render: function ( data, type, row, meta ) {
                            var actions = `<a class="action-delete" href="`+row.id+`"><i class="icon-trash text-danger pr-5" data-id="'+row.id+'"></i></a>
<a class="action-edit" href="{{ url('/admin/book-edit/') }}/`+row.id+`"><i class="icon-pencil5 text-success""></i></a>
<a class="action-edit" href="{{ url('/admin/book-view/') }}/`+row.id+`"><i class="icon-link2 text-info""></i></a> `;
                            return actions;
                        }
                    },
                ],
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
                },
                drawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                },
                preDrawCallback: function() {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                }
            });

            // Nested object data
            $('.datatable-nested').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('booksListDataTables') }}",
                    type: 'POST'
                },
                columns: [
                    {data: "image"},
                    {data: "title" },
                    {data: "created_at"}
                ]
            });

            var table = $('.datatable-nested').DataTable();

            // External table additions
            // ------------------------------

            // Enable Select2 select for the length option
            $('.dataTables_length select').select2({
                minimumResultsForSearch: Infinity,
                width: 'auto'
            });

            $('.datatable-nested').on('click','.action-delete',function(e){
                e.preventDefault();
                let id = $(this).attr("href");
                console.info('id: ', id);

                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this book!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel pls!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){
                    if (isConfirm) {
                        $.post(" {{ route('deleteBook') }} ",{id:id})
                            .done(function(res){
                                table.ajax.reload();
                                swal({
                                    title: "Deleted!",
                                    text: "Book is not part of your site anymore.",
                                    confirmButtonColor: "#66BB6A",
                                    type: "success"
                                });
                            })
                            .error(function(res){
                                swal({
                                    title: "Opps!",
                                    text: "Something went wrong. Please try again",
                                    confirmButtonColor: "#2196F3",
                                    type: "error"
                                });
                            });
                    }
                    else {
                        swal({
                            title: "Cancelled",
                            text: "This Book survive :)",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
            });

        });
    </script>
@stop

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-users position-left"></i> <span class="text-semibold">Books</span> - list</h4>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="panel panel-flat">

        <table class="table datatable-nested">
            <thead>
            <tr>
                <th>Image</th>
                <th>Title</th>
                <th>Created At</th>
                <th>Actions</th>
            </tr>
            </thead>
        </table>
    </div>

    {{-- ACTION MODAL --}}
    <div id="action_edit" class="modal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Update user info</h5>
                </div>

                <form id="form-update" action="{{ route('userUpdate') }}" method="POST">
                    <div class="modal-body"></div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button id="btn-update" type="button" class="btn btn-success">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('action-footer')
    {{-- Full width footer --}}
@stop
