@extends('admin/base')

@section('title')
    Book Management
@stop

@section('css-links')
    {{-- Stylesheet links --}}
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/core.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/extensions/cookie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_childcounter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js')
    }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>



    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // Drag and drop support
            $(".tree-drag").fancytree({
                extensions: ["dnd","edit"],
                source: [
                    {"title": "SubCategory", "extraClasses": 'tree-chapter', "expanded": true, "folder": true}
                ],
                dnd: {
                    autoExpandMS: 400,
                    focusOnClick: true,
                    preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
                    preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
                    dragStart: function(node, data) {
                        return true;
                    },
                    dragEnter: function(node, data) {
                        return true;
                    },
                    dragDrop: function(node, data) {
                        var span = (data.hitMode == 'over') ? $(data.node.span) : $(data.node.parent.span);
                        console.log('node',node);
                        console.log('data',data);
                        if( span != 'undefined' ){
                            var parent_type = getType(span);
                            var item_type = getType(data.otherNode.span);
                            if( (item_type - parent_type) == 1 || (parent_type==0&&item_type==0) ){
                                // This function MUST be defined to enable dropping of items on the tree.
                                data.otherNode.moveTo(node, data.hitMode);
                            }
                        }

                        function getType(span){
                            span = $(span);
                            if( span.hasClass('tree-chapter') ){
                                return 1;
                            }else if( span.hasClass('tree-section') ){
                                return 2;
                            }else if( span.hasClass('tree-subSection') ){
                                return 3;
                            }else{
                                return 0;
                            }
                        }
                    }
                },
                init: function(event, data) {
                    $('.has-tooltip .fancytree-title').tooltip();
                },
                edit: {
                    adjustWidthOfs: 0,
                    inputCss: {minWidth: "0"},
                    triggerStart: ["f2", "dblclick", "shift+click", "mac+enter"]
                }
            });

            $('#add-subsection').click(function(){
                let node = validateForumNode();

                var node_section = (node) ? $(node.span).hasClass('tree-section') : false;
                if(node){
                    node.editCreateNode("child", {
                        title: 'New subSection',
                        extraClasses: 'tree-subSection'
                    });
                }
            });


            $('#add-forum').click(function(){
                let node = validateForumNode();
                if(node){
                    node.editCreateNode("child", {
                        title: 'New Forum',
                        extraClasses: 'tree-forum',
                        'key' : 'forum'
                    });
                }
            });

            $('#add-section').click(function(){
                let node = validateForumNode();
                var node_chapter = (node) ? $(node.span).hasClass('tree-chapter') : false;
                if( !node || !node_chapter ) {
                    swal("Opps!", "Please select or create a chapter first.", "error");
                    return;
                }

                if(node){
                    node.editCreateNode("child", {
                        title: 'New section',
                        extraClasses: 'tree-section'
                    });
                }

            });

            $('#add-chapter').click(function(){
                let node = validateForumNode();
                var node_chapter = (node) ? $(node.span).hasClass('tree-chapter') : false;
                if( !node_chapter ) {
                    swal("Opps!", "Please select or create a chapter first.", "error");
                    return;
                }
                if(node) {
                    node.editCreateNode("before", {
                        title: 'New chapter',
                        extraClasses: 'tree-chapter',
                        folder: true

                    });
                }
            });

            $('#create-book').click(function(){
                var tree = $("#book-tree").fancytree("getTree");
                var data = tree.toDict(true);
                data['title'] = $('#cat_name').val();

                if( data['title'] == '' ){
                    swal("Hey", "You need to enter Category Name", "error");
                }else{
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-Token': $('meta[name=_token]').attr('content')
                        }
                    });

                    $.ajax({
                        url: "{{ route('saveCategory') }}", // point to server-side PHP script
                        data: data,
                        type: 'POST',
                        //contentType: false, // The content type used when sending data to the server.
                        cache: false, // To unable request pages to be cached
                        //processData: false,
                        success: function(data) {
                            $.jGrowl("Category Created Successfully", {
                                sticky: true,
                                theme : 'bg-success',
                                animateClose : { opacity: 'hide' },
                                life : 3000
                            });
                        },
                        error: function (request, status, error) {
                            console.info('inside on error');
                            let response = $.parseJSON(request.responseText);
                            let msg = response.errors.file;
                            msg = msg.join('<br>');
                            console.info(request.responseText, status, msg, response.errors.file);
                            $.jGrowl(msg, {
                                sticky: true,
                                theme : 'bg-danger',
                                animateClose : { opacity: 'hide' },
                                life : 3000
                            });
                        }
                    });
                }
            });

            $('#remove-node').click(function(){
                var node = $("#book-tree").fancytree("getActiveNode");
                node.remove();
            });


            function validateForumNode()
            {
                var node = $("#book-tree").fancytree("getActiveNode");
                //var node_section = (node) ? $(node.span).hasClass('tree-section') : false;
                if( node.extraClasses == 'tree-forum' ) {
                    swal("Opps!", "You cannot create any item under Forum", "error");
                    return;
                }

                return node;
            }
        });
    </script>
@stop

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="row">
                <div class="col-lg-8 mb-15">
                    <input id="cat_name" name="cat_name" type="text" class="form-control" placeholder="Enter Category Name">
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div id="book-tree" class="tree-drag well border-left-info border-left-lg"></div>
        </div>
    </div>
@stop

@section('action-footer')
    <div class="action-footer-fix">
        <div class="row">
            <div class="text-left col-sm-8 mt-10 mb-10">
                <div class="btn-group">
                    <button id="add-chapter" type="button" class="btn btn-success legitRipple">Subcategory</button>
                    <button id="add-section" type="button" class="btn btn-success legitRipple">Section</button>
                    <button id="add-subsection" type="button" class="btn btn-success legitRipple">Subsection</button>
                    <button id="add-forum" type="button" class="btn btn-success legitRipple">Forum</button>
                </div>
                <button id="remove-node" type="button" class="btn btn-danger legitRipple">Remove</button>
            </div>
            <div class="text-right col-sm-4 mt-10 mb-10">
                <button id="create-book" type="button" class="btn bg-primary btn-labeled btn-labeled-right legitRipple pull-right"><b><i class="icon-circle-right2"></i></b> Create Category</button>
            </div>
        </div>
    </div>
@stop