@extends('admin/base')

@section('title')
    Forum Management
@stop

@section('css-links')
    {{-- Stylesheet links --}}
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/core.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/extensions/cookie.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_childcounter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/uploaders/fileinput/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/uploader_bootstrap.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>

    <!-- <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/ckeditor/ckeditor.js') }}"></script> -->
    <!-- /theme JS files -->

    <!-- custom js files  -->
    <script type="text/javascript" src="{{ asset('assets/js/customJs/ajaxLib.js') }}"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var mySwitch = new Switchery($('#make_private')[0], {
                size:"small"
            });
            var mySwitchThread = new Switchery($('#enable_thread')[0], {
                size:"small"
            });
            //description of each element
            var elementDescription = {};
            //privacy/permission of each frum
            var elementPrivacy = {};
            //the active node selected
            var threadEnabled = {};
            var activeNode;
            var booksArray = {!!$books!!};
            var trash = {!!$trash!!};
            if(trash == "" || trash === null){
                trash = [];
            }
            // var additionalElements = [{"title": "Random Category", "extraClass": 'tree-top-layer, tree-category-0', "expanded": true, "folder": true, "children": [
            //     {"key": "1_1", "title": "Random Category", "extraClass": "tree-category", "expanded": true, "children": [
            //         {"key": "1_1_1", "title": "Random Forum", "extraClass": "tree-forum"}
            //     ]}
            // ]},{"title": "Random Category", "extraClass": 'tree-top-layer, tree-category-0', "expanded": true, "folder": true, "children": [
            //     {"key": "1_1", "title": "Random Category", "extraClass": "tree-category", "expanded": true, "children": [
            //         {"key": "1_1_1", "title": "Random Category", "extraClass": "tree-category", "expanded": true, "children": [
            //         {"key": "1_1_1_1", "title": "Random Forum", "extraClass": "tree-forum"},
            //         {"key": "1_1_1_2", "title": "Random Forum", "extraClass": "tree-forum"}]}
            //     ]}]}
            // ];
            // booksArray.push(additionalElements[0]);
            // booksArray.push(additionalElements[1]);

            $(".tree-recycle-bin").fancytree({
                init: function(event, data) {
                    $('.has-tooltip .fancytree-title').tooltip();
                },
                extensions: ["dnd","edit"],
                source: trash
            });

            $(".tree-drag").fancytree({
                extensions: ["dnd","edit"],
                source: booksArray,
                dnd: {
                    autoExpandMS: 400,
                    focusOnClick: true,
                    preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
                    preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
                    dragStart: function(node, data) {
                        // console.log(data.node.data.extraClass);
                        return true;
                    },
                    dragEnter: function(node, data) {
                        return true;
                    },
                    dragDrop: function(node, data) {
                        var parentClass = data.node.parent.data.extraClass;
                        // console.log(parentClass);
                        if(activeNode !== undefined){
                            if(!("extra_class" in data.node.parent.data) && ("extraClass" in activeNode.data)){
                                if(!(parentClass === undefined && activeNode.data.extraClass == "tree_forum")
                                    && activeNode !== undefined && isCategoryClass(activeNode.data.extraClass)){
                                    // if("extra_class" in data.node.parent.data){
                                        data.otherNode.moveTo(node, data.hitMode);
                                    // }
                                }
                            }
                        }
                    }
                },
                expand: function(event, data) {
                    //applicable only for books
    				if(("extra_class" in data.node.data) && !data.node.data.extra_class.includes("tree-book-0")){
                        console.log(data.node.data.extra_class);
                        data.node.visit(function(node){
                            node.setExpanded();
                        });
                    }
    			},
                activate: function(event, data){
                    //called when node selected. timer used to synchronise with text fetching from descriptikon box
                    setTimeout(function(){
                        // console.log(data.node.data.extraClass);
                        // fetchPropertiesAjax(data.node.data);
                        if(isCategoryClass(data.node.data.extraClass)){
                            activeNode = data.node;
                            console.log("node activated");
                            categoryClass = true;
                            fetchPropertiesAjax(activeNode.data);
                        } else {
                            activeNode = undefined;
                            if(data.node.data.extra_class == "tree-forum"){
                                fetchPropertiesAjaxBook(data.node.data);
                            }
                        }
                    }, 300);
                },
                init: function(event, data) {
                    $('.has-tooltip .fancytree-title').tooltip();
                },
                edit: {
                    adjustWidthOfs: 0,
                    inputCss: {minWidth: "0"},
                    triggerStart: ["f2", "dblclick", "shift+click", "mac+enter"]
                }
            });

            //expand category tree default
            $(".tree-drag").fancytree("getTree").visit(function(node){
                if(!("extra_class" in node.data)){
                    node.setExpanded();
                }
            });

            //expand trash tree default
            $(".tree-recycle-bin").fancytree("getTree").visit(function(node){
                node.setExpanded();
            });

            //setting description and title texts
            function setDescriptionData(description){
                console.log(description);
                $("#description").val(description);
                // CKEDITOR.instances["description"].setData(description);
                $("#book-title").val(activeNode.title);
            }

            function fetchPropertiesAjax(nodeData){
                if(isCategoryClass(nodeData.extraClass)){
                    // console.log(nodeData.extraClass);
                    // console.log(elementDescription[nodeData.element_id]);
                    if(nodeData.element_id !== undefined && elementDescription[nodeData.element_id] === undefined){
                        var successFunction = function(data) {
                            elementDescription[nodeData.element_id] = data["description"];
                            threadEnabled[nodeData.element_id] = data["enable_threads"] ? false : true;
                            setDescriptionData(data["description"]);
                            // if(nodeData.extraClass == "tree_forum"){
                            elementPrivacy[nodeData.element_id] = data["is_public"] ? false : true;
                                // console.log(elementPrivacy);
                            setMakePrivateButtonProperties(nodeData.element_id, elementPrivacy[nodeData.element_id]);
                            setMakeThreadButtonProperties(nodeData.element_id, threadEnabled[nodeData.element_id]);
                            // }
                        };
                        var failureFunction = function (request, status, error) {
                            console.log("could not fetch description");
                        };
                        sendAjaxRequest("{{ route('getCategoryProperties') }}", nodeData.element_id, 'POST', successFunction, failureFunction);
                    } else {
                        setDescriptionData(elementDescription[nodeData.element_id]);
                        // if(nodeData.extraClass == "tree_forum"){
                        setMakePrivateButtonProperties(nodeData.element_id, elementPrivacy[nodeData.element_id]);
                        setMakeThreadButtonProperties(nodeData.element_id, threadEnabled[nodeData.element_id]);
                        // }
                    }
                }
            }

            function fetchPropertiesAjaxBook(nodeData){
                if(threadEnabled[nodeData.element_id] === undefined){
                    var successFunction = function(data) {
                        threadEnabled[nodeData.element_id] = data["enable_threads"] ? false : true;
                        setMakeThreadButtonProperties(nodeData.element_id, threadEnabled[nodeData.element_id]);
                    };
                    var failureFunction = function (request, status, error) {
                        console.log("could not fetch description");
                    };
                    sendAjaxRequest("{{ route('getCategoryProperties') }}", nodeData.element_id, 'POST', successFunction, failureFunction);
                } else {
                    setMakeThreadButtonProperties(nodeData.element_id, threadEnabled[nodeData.element_id]);
                }
            }

            $('#add-category').click(function(){
                var node = $(".tree-drag").fancytree("getActiveNode");
                // var node_section = (node) ? $(node.span).hasClass('tree-section') : false;
                if(node == null){
                    var rootNode = $(".tree-drag").fancytree("getRootNode");
                    var childNode = rootNode.addChildren({
                        title: "Random Category",
                        // tooltip: "This folder and all child nodes were added programmatically.",
                        folder: true,
                        extraClass: 'tree-category-0'
                    });
                    // var childNode = childNode.addChildren({
                    //     title: "Random Category",
                    //     extraClass: 'tree-category'
                    // });
                    // childNode.addChildren({
                    //     title: "Random Forum",
                    //     extraClass: 'tree-forum'
                    // });
                } else {
                    // console.log(node.data.extraClass);
                    if(node.data.extraClass == "tree-category-0" || node.data.extraClass == "tree-category"){
                        node.addChildren({
                            title: 'Random Category',
                            folder: true,
                            extraClass: 'tree-category'
                        });
                    } else {
                        swal("Opps!", "Please select a category to create category under it.", "error");
                    }
                }
            });

            $('#add-forum').click(function(){
                // console.info('function called');
                var node = $(".tree-drag").fancytree("getActiveNode");
                if(node.data.extraClass == "tree-category-0" || node.data.extraClass == "tree-category"){
                    node.addChildren({
                        title: 'Random Forum',
                        extraClass: 'tree-forum'
                    });
                } else {
                    swal("Opps!", "Please select a category first.", "error");
                }
            });


            $('#make_private').click(function() {
                node = validateSwitchInput(mySwitch);
                //check first if the selected node is book or part of book hierarchy
                // if(node.data.extraClass == "tree_forum"){
                    var data_checked = {};
                    data_checked.id = node.data.element_id;
                    data_checked.checked = mySwitch.isChecked();
                    sendAjaxRequest("{{ route('forumPanelUpdatePermission') }}", data_checked, 'POST', function(data) {
                        $.jGrowl("Permission Updated", {
                            sticky: true,
                            theme : 'bg-success',
                            animateClose : { opacity: 'hide' },
                            life : 3000
                        });

                        //set button properties
                        setMakePrivateButtonProperties(data_checked.id, data_checked.checked);
                    }, function (request, status, error) {
                        $.jGrowl("Could Not Save.", {
                            sticky: true,
                            theme : 'bg-danger',
                            animateClose : { opacity: 'hide' },
                            life : 3000
                        });
                        setMakePrivateButtonProperties(data_checked.id, !data_checked.checked);
                    });
                // }
            });

            function validateSwitchInput(switchPressed){
                // console.log(mySwitch.isChecked());
                var node = $(".tree-drag").fancytree("getActiveNode");
                // console.log(node);
                if(node == null){
                    swal("Opps!", "Please select a node.", "error");
                    setSwitchery(switchPressed, !switchPressed.isChecked());
                    return;
                } else if(node.data.element_id === undefined){
                    swal("Opps!", "Please save before setting permission.", "error");
                    setSwitchery(switchPressed, !switchPressed.isChecked())
                    return;
                }

                return node;
            }

            $('#enable_thread').click(function() {
                node = validateSwitchInput(mySwitchThread);
                //check first if the selected node is book or part of book hierarchy
                // if(node.data.extraClass == "tree_forum" || node.data.extra_class == "tree-forum"){
                    var data_checked = {};
                    data_checked.id = node.data.element_id;
                    data_checked.checked = mySwitchThread.isChecked();
                    data_checked.class = node.data.extraClass !== undefined ? node.data.extraClass : node.data.extra_class;
                    sendAjaxRequest("{{ route('forumPanelUpdateThreadPermission') }}", data_checked, 'POST', function(data) {
                        $.jGrowl("Updated", {
                            sticky: true,
                            theme : 'bg-success',
                            animateClose : { opacity: 'hide' },
                            life : 3000
                        });

                        //set button properties
                        setMakeThreadButtonProperties(data_checked.id, data_checked.checked);
                    }, function (request, status, error) {
                        $.jGrowl("Could Not Save.", {
                            sticky: true,
                            theme : 'bg-danger',
                            animateClose : { opacity: 'hide' },
                            life : 3000
                        });
                        setMakeThreadButtonProperties(data_checked.id, !data_checked.checked);
                    });
                // } else {
                //     swal("Opps!", "Please select a forum.", "error");
                //     setSwitchery(mySwitchThread, !mySwitchThread.isChecked())
                // }
            });

            function setSwitchery(switchElement, checkedBool) {
                if((checkedBool && !switchElement.isChecked()) || (!checkedBool && switchElement.isChecked())) {
                    switchElement.setPosition(true);
                    // switchElement.handleOnchange(true);
                }
            }

            function setMakePrivateButtonProperties(id, isChecked){
                // console.log(isChecked);
                if(isChecked){
                    elementPrivacy[id] = true;
                    // $("#make_private").prop('checked', true);
                    document.getElementById("toggle_text").textContent="Is Private";
                    setSwitchery(mySwitch, true);
                    document.getElementById("btn_make_private").style.backgroundColor = "red";
                } else {
                    delete elementPrivacy[id];
                    // $("#make_private").prop('checked', false);
                    document.getElementById("toggle_text").textContent="Is Public";
                    setSwitchery(mySwitch, false);
                    document.getElementById("btn_make_private").style.backgroundColor = "green";
                }
            }

            function setMakeThreadButtonProperties(id, isChecked){
                // console.log(isChecked);
                if(isChecked){
                    threadEnabled[id] = true;
                    document.getElementById("thread_status_text").textContent="Threads Disabled";
                    setSwitchery(mySwitchThread, true);
                    document.getElementById("btn_thread_control").style.backgroundColor = "red";
                } else {
                    delete threadEnabled[id];
                    document.getElementById("thread_status_text").textContent="Threads Enabled";
                    setSwitchery(mySwitchThread, false);
                    document.getElementById("btn_thread_control").style.backgroundColor = "green";
                }
            }

            $('#create-book').click(function(){
                if(activeNode !== undefined){
                    // elementDescription[activeNode.data.element_id] = CKEDITOR.instances.description.getData();
                    elementDescription[activeNode.data.element_id] = $("#description").val();
                }
                var tree = $("#book-tree").fancytree("getTree");
                var treeTrash = $("#tree-recycle-bin").fancytree("getTree");
                var data = tree.toDict(true);
                var data_trash = treeTrash.toDict(true);
                data['description'] = elementDescription;
                // console.log(data['description']);
                data['trash'] = data_trash;
                data['is_private'] = elementPrivacy;
                sendAjaxRequest("{{ route('forumPanelSave') }}", data, 'POST', function(data) {
                    $.jGrowl("Categories Saved Successfully", {
                        sticky: true,
                        theme : 'bg-success',
                        animateClose : { opacity: 'hide' },
                        life : 3000
                    });
                    location.reload();
                }, function (request, status, error) {
                    $.jGrowl("Categories Not Saved", {
                        sticky: true,
                        theme : 'bg-danger',
                        animateClose : { opacity: 'hide' },
                        life : 3000
                    });
                });
            });

            function isCategoryClass(node){
                // console.log(node);
                if(node !== undefined){
                    var classes = node.split(",").map(function(item) {
                        return item.trim();
                    });

                    if(classes.includes("tree-book-0")|| classes.includes("tree-book") || classes.includes("tree-chapter")
                    || classes.includes("tree-section") || classes.includes("tree-subSection")){
                        return false;
                    }

                    return true;
                }

                return false;
            }

            $('#remove-node').click(function(){
                var node = $(".tree-drag").fancytree("getActiveNode");
                var classes = node.data.extraClass ? node.data.extraClass : node.data.extra_class;

                //check first if the selected node is book or part of book hierarchy
                if(isCategoryClass(classes)){
                    var d = node.toDict(true, function(dict){
                        // Remove keys, so they will be re-generated when this dict is
                        // passed to addChildren()
                        delete dict.key;
                    });

                    //add to recycle bin root node
                    var rootNode = $("#tree-recycle-bin").fancytree("getRootNode");
                    rootNode.addChildren([d]);
                    //remove original node
                    node.remove();

                    sendAjaxRequest("{{ route('forumPanelRemoveNode') }}", d, 'POST', function(data) {
                        $.jGrowl("Deleted Successfully", {
                            sticky: true,
                            theme : 'bg-success',
                            animateClose : { opacity: 'hide' },
                            life : 3000
                        });

                        updateTrashString(function(data) {
                            console.log("successfully updated trash");
                        });
                    });
                }
            });

            $('#delete_permanently').click(function(){
                var node = $(".tree-recycle-bin").fancytree("getActiveNode");
                // console.log(node);
                if(node == null){
                    swal("Opps!", "Please select a node.", "error");
                    return;
                }

                sendAjaxRequest("{{ route('forumPanelDeletePermanently') }}", node.toDict(true), 'POST', function(data) {
                    $.jGrowl("Deleted Successfully", {
                        sticky: true,
                        theme : 'bg-success',
                        animateClose : { opacity: 'hide' },
                        life : 3000
                    });
                    //remove original node
                    node.remove();

                    //update trash string
                    updateTrashString(function(data) {
                        console.log("successfully updated trash");
                    });
                });
            });

            function updateTrashString(positiveResponse){
                var treeTrash = $("#tree-recycle-bin").fancytree("getTree");
                var data_trash = treeTrash.toDict(true);
                sendAjaxRequest("{{ route('forumPanelUpdateTrash') }}", data_trash.children, 'POST', positiveResponse, function (request, status, error) {
                    console.log("trash update failed");
                });
            }

            $('#restore').click(function(){
                var node = $(".tree-recycle-bin").fancytree("getActiveNode");
                // console.log(node);
                if(node == null){
                    swal("Opps!", "Please select a node.", "error");
                    return;
                }

                sendAjaxRequest("{{ route('forumPanelRestoreNode') }}", node.toDict(true), 'POST', function(data) {
                    $.jGrowl("Restored Successfully", {
                        sticky: true,
                        theme : 'bg-success',
                        animateClose : { opacity: 'hide' },
                        life : 3000
                    });
                    //remove original node
                    node.remove();
                    updateTrashString(function(data) {
                        location.reload();
                    });
                });
            });

            // $(window).load(function () {
            //     CKEDITOR.replace( "description", {
            //         extraPlugins: 'mathjax',
            //         mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML',
            //         height: 320
            //     });
            //
            //     //to add description text to array once focus is removed
            //     CKEDITOR.instances.description.on('blur', function(){
            //         elementDescription[activeNode.data.element_id] = CKEDITOR.instances.description.getData();
            //     });
            // });
    });

    </script>
@stop

@section('header')
    <div class="page-header page-header-default page-header-xs">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">{{$pageTitle}}</h5>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="row">
            	<h5>Title<h5>
                <div class="col-lg-8 mb-15">
                    <input id="book-title" name="title" type="text" class="form-control" readonly placeholder="Enter title">
                </div>
            </div>

            <!-- <div class="row">
                <div class="col-lg-8 mb-15">
                    <input type="file" class="file-input" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-xs" data-remove-class="btn btn-default btn-xs" id="book_image" name="book_image">
                     <span class="help-block">Select Book image</span>
                </div>
            </div> -->

            <div class="panel-body">
	            <h5>Description<h5>
	            <input id="description" name="description" type="text" class="form-control" placeholder="Enter Description">
<!--                 <textarea id="" name="description" rows="10" data-sample="1" data-sample-short="" style="width: 100%;"> -->
                </textarea>
            </div>
        </div>

        <div class="panel-body">
            <div id="book-tree" class="tree-drag well border-left-info border-left-lg"></div>
        </div>
    </div>

@stop

@section('action-footer')
    <div class="action-footer-fix">
        <div class="row">
            <div class="text-left col-sm-8 mt-10 mb-10">
                <div class="btn-group">
                    <!-- <button id="add-chapter" type="button" class="btn btn-success legitRipple">Chapter</button> -->
                    <!-- <button id="edit-permission" type="button" class="btn btn-success legitRipple">Edit Permissions</button> -->
                    <button class="btn btn-success legitRipple" id="btn_make_private">
                        <!-- <input type="checkbox" id="make_private"/><span>Private<spam/> -->
                        <label class="checkbox-inline checkbox-switchery checkbox-right switchery-xs">
                            <input type="checkbox" class="switch" id="make_private"/>
                            <span id="toggle_text">Is Public</span>
                        </label>
                    </button>
                    <button class="btn btn-success legitRipple" id="btn_thread_control">
                        <label class="checkbox-inline checkbox-switchery checkbox-right switchery-xs">
                            <input type="checkbox" class="switch" id="enable_thread"/>
                            <span id="thread_status_text">Threads Enabled</span>
                        </label>
                    </button>
                </div>
                <button id="remove-node" type="button" class="btn btn-danger legitRipple">Remove</button>
            </div>
            <div class="text-right col-sm-4 mt-10 mb-10">
            	<button id="add-category" type="button" class="btn btn-success legitRipple">Add Category</button>
                <button id="add-forum" type="button" class="btn btn-success legitRipple">Add Forum</button>
                <button id="create-book" type="button" class="btn bg-primary btn-labeled btn-labeled-right legitRipple pull-right"><b><i class="icon-circle-right2"></i></b> Save</button>
            </div>
        </div>
        <div class="row">
            <div class="panel-heading">
                <h4>Recycle Bin</h4>
            </div>
            <div id="tree-recycle-bin" class="tree-recycle-bin well border-left-info border-left-lg"></div>
            <div class="text-right col-sm-12 mt-10 mb-10">
                <button id="restore" type="button" class="btn btn-success legitRipple">Restore</button>
                <button id="delete_permanently" type="button" class="btn btn-danger legitRipple">Permanently Delete</button>
            </div>
        </div>
    </div>
@stop
