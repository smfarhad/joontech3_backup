<?php
/**
 * Created by PhpStorm.
 * User: msherax
 * Date: 2/28/18
 * Time: 6:56 PM
 */
?>


@extends('admin/base')

@section('title')
    Books List
@stop

@section('css-links')
    {{-- Stylesheet links --}}
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>

    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Table setup
            // ------------------------------

            // Setting datatable defaults
            $.extend( $.fn.dataTable.defaults, {
                autoWidth: false,
                columnDefs: [
                    {
                        orderable: false,
                        width: '100px',
                        targets: [ 0 ],
                        className: 'text-center',
                        render: function ( data, type, row, meta ) {
                            //console.info(data, type, row, meta);
                            let image = '<img src="{{ asset('images') }}/'+data+'" width="50">';
                            return image;
                        }
                    },
                    {
                        orderable: false,
                        width: '100px',
                        targets: [ 3 ],
                        className: 'text-center',
                        render: function ( data, type, row, meta ) {
                            var actions = `<a class="action-delete" href="`+row.id+`"><i class="icon-trash text-danger pr-5" data-id="'+row.id+'"></i></a>
<a class="action-edit" href="{{ url('/admin/book-edit/') }}/`+row.id+`"><i class="icon-pencil5 text-success""></i></a>
<a class="action-edit" href="{{ url('/admin/book-view/') }}/`+row.id+`"><i class="icon-link2 text-info""></i></a> `;
                            return actions;
                        }
                    },
                ],
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
                },
                drawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                },
                preDrawCallback: function() {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                }
            });

            // Nested object data
            $('.datatable-nested').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('booksListDataTables') }}",
                    type: 'POST'
                },
                columns: [
                    {data: "image"},
                    {data: "title" },
                    {data: "created_at"}
                ]
            });

            var table = $('.datatable-nested').DataTable();

            // External table additions
            // ------------------------------

            // Enable Select2 select for the length option
            $('.dataTables_length select').select2({
                minimumResultsForSearch: Infinity,
                width: 'auto'
            });

            $('.datatable-nested').on('click','.action-delete',function(e){
                e.preventDefault();
                let id = $(this).attr("href");
                console.info('id: ', id);

                swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this book!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#EF5350",
                            confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function(isConfirm){
                            if (isConfirm) {
                                $.post(" {{ route('bookDelete') }} ",{id:id})
                                        .done(function(res){
                                            table.ajax.reload();
                                            swal({
                                                title: "Deleted!",
                                                text: "Book is not part of your site anymore.",
                                                confirmButtonColor: "#66BB6A",
                                                type: "success"
                                            });
                                        })
                                        .error(function(res){
                                            swal({
                                                title: "Opps!",
                                                text: "Something went wrong. Please try again",
                                                confirmButtonColor: "#2196F3",
                                                type: "error"
                                            });
                                        });
                            }
                            else {
                                swal({
                                    title: "Cancelled",
                                    text: "This Book survive :)",
                                    confirmButtonColor: "#2196F3",
                                    type: "error"
                                });
                            }
                        });
            });

        });
    </script>
@stop

@section('content')
    <!-- Dashboard content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Support tickets -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Discussions</h6>
                    <div class="heading-elements">
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table text-nowrap">

                        <tbody>
                        @if($categories)
                            @foreach($categories as $category)
                            <tr>
                                <td class="text-center">
                                    <h6 class="no-margin">16
                                        <small class="display-block text-size-small no-margin">hours</small>
                                    </h6>
                                </td>
                                <td>
                                    <div class="media-body">
                                        @if($category->is_forum)
                                            <a href="{{ url('forum/'. $category->id) }}"
                                               class="display-inline-block text-default text-semibold letter-icon-title">{{ $category->title }}</a>
                                        @else
                                            <a href="{{ url('admin/discussions/categories/'. $category->id) }}"
                                           class="display-inline-block text-default text-semibold letter-icon-title">{{ $category->title }}</a>
                                        @endif
                                    </div>
                                </td>

                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                                        class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-undo"></i> Quick reply</a></li>
                                                <li><a href="#"><i class="icon-history"></i> Full history</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-checkmark3 text-success"></i> Resolve
                                                        issue</a></li>
                                                <li><a href="#"><i class="icon-cross2 text-danger"></i> Close issue</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /support tickets -->

        </div>


    </div>
    <!-- /dashboard content -->
@stop

@section('action-footer')
    {{-- Full width footer --}}
@stop
