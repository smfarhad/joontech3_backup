<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-default sidebar-fixed">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="category-content">
                <div class="sidebar-user-material-content">
                    <a href="#"><img src="{{ asset('assets/images/user-profile.jpg') }}" class="img-circle img-responsive" alt=""></a>
                    <h6>{{ Auth::user()->fullName() }}</h6>
                </div>
                                            
                <div class="sidebar-user-material-menu">
                    <a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
                </div>
            </div>
            
            <div class="navigation-wrapper collapse" id="user-nav">
                <ul class="navigation">
                    <li><a href="#"><i class="icon-user-plus"></i> <span>My profile</span></a></li>
                    <li><a href="#"><i class="icon-coins"></i> <span>My balance</span></a></li>
                    <li><a href="#"><i class="icon-comment-discussion"></i> <span><span class="badge bg-teal-400 pull-right">58</span> Messages</span></a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-cog5"></i> <span>Account settings</span></a></li>
                    <li><a href="{{ route('logout') }}"><i class="icon-switch2"></i> <span>Logout</span></a></li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li>
                        <a href="{{ route('userList') }}"><i class="icon-users4"></i> <span>User List</span></a>
                    </li>
                    <li>
                        <a href="#"><i class=" icon-books"></i> <span>Books</span></a>
                        <ul>
                            <li><a href="{{ route('listBooks') }}"> Book List </a></li>
                            <li><a href="{{ route('bookAdd') }}"> Book Add </a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="{{ route('listAllowedEmails') }}"><i class="icon-envelop5"></i> <span>Allowed Emails</span></a>
                    </li>

                    {{--  <li>
                        <a href="{{ route('registrationKey') }}"><i class="icon-key"></i> <span>Registration Keys</span></a>
                    </li>  --}}
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->