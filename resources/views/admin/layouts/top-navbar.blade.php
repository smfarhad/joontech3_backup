<?php
/**
 * Created by PhpStorm.
 * User: msherax
 * Date: 2/25/18
 * Time: 11:44 PM
 */
?>

<!-- Page header content -->
<div class="page-header-content">
    <div class="page-title">
        <h4>{{ $pageTitle }}</h4>
    </div>

    <div class="heading-elements">
        <ul class="breadcrumb heading-text">
            <li>{{--<a href="index.html"><i class="icon-home2 position-left"></i> Home</a>--}}
                @include ('forum::partials.breadcrumbs')
            </li>
            <li class="active">{{ $pageTitle }}</li>
        </ul>
    </div>
</div>
<!-- /page header content -->

<!-- Second navbar -->
<div class="navbar navbar-inverse navbar-transparent" id="navbar-second">
    <ul class="nav navbar-nav visible-xs-block">
        <li>
            <a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle">
                <i class="icon-paragraph-justify3"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second-toggle">
        <ul class="nav navbar-nav navbar-nav-material">
            <li class="active"><a href="{{ route('bookList') }}">Home</a></li>

            <li class="dropdown mega-menu mega-menu-wide">
                <!-- <a href="{{ url('forum') }}">Discussions </a> -->
                <a href="{{ route('discussionIndex')  }}">Discussions </a>
            </li>

            <li>
                <a href="{{ route('userList') }}"><i class="icon-users4"></i> <span>User List</span></a>
            </li>

            <li>
                <a href="{{ route('listAllowedEmails') }}"><i class="icon-envelop5"></i> <span>Allowed Emails</span></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Books <span class="caret"></span>
                </a>

                <ul class="dropdown-menu width-200">
                    <li class="dropdown-header">Books Options</li>
                    <li><a href="{{ route('listBooks') }}"><i class="icon-books"></i> Book List </a></li>
                    <li><a href="{{ route('bookAdd') }}"><i class="icon-book3"></i> Book Add </a></li>
                </ul>
            </li>

            <!-- <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Categories <span class="caret"></span>
                </a>

                <ul class="dropdown-menu width-200">
                    <li class="dropdown-header">Categories Options</li>
                    <li><a href="{{ route('listCategories') }}"><i class="icon-books"></i> Categories List </a></li>
                    <li><a href="{{ route('createCategory') }}"><i class="icon-book3"></i> Create Category</a></li>
                </ul>
            </li> -->

            <li class="dropdown mega-menu mega-menu-wide">
                <a href="{{ route('forum-panel') }}">Forum Panel </a>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-nav-material navbar-right">
            <li>
                <a href="changelog.html">
                    <span class="status-mark status-mark-inline border-success-300 position-left"></span>
                    Changelog
                </a>
            </li>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-cog3"></i>
                    <span class="visible-xs-inline-block position-right">Settings</span>
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                    <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                    <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /second navbar -->
