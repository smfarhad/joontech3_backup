<input type="hidden" name="user_id" value="{{ $user->id }}"> 
<div class="form-group form-group-material">
    <label class="text-bold">Email Address</label>
    <input type="email" name="email" placeholder="Email" class="form-control" value="{{ $user->email }}" readonly>
</div>
<div class="form-group form-group-material">
    <label class="text-bold">First name</label>
    <input type="text" name="first_name" placeholder="First name" class="form-control" value="{{ $user->first_name }}">
</div>
<div class="form-group form-group-material">
    <label class="text-bold">Last name</label>
    <input type="text" name="last_name" placeholder="Last name" class="form-control" value="{{ $user->last_name }}">
</div>
</div>
<div class="form-group form-group-material">
    <label class="text-bold">Level</label>
    <select name="user_level" data-placeholder="User Level" class="select">
        {!! $user_level_dropdown !!}
    </select>
</div>
<div class="form-group form-group-material">
    <label class="text-bold">Password</label>
    <input type="password" name="password" class="form-control">
</div>