<script>
    $(function() {
        $(".tree-checkbox-hierarchical").fancytree({
            checkbox: true,
            selectMode: 3,
            autoScroll: true,
        });

        $('#form-search').on('keydown','input[type=text]', function(e) {
            if (e.which == 13) {
                doSearch();
                e.preventDefault();
            }
        });
    });
</script>

<div class="col-xs-12">
    <h6 class="text-teal-400">Search by Category filter:</h6>
    <div id="search-tree" class="tree-checkbox-hierarchical height-300 scroll well border-left-teal-400 border-left-lg">
        <ul>
            @if ( $books->isNotEmpty() )
            @foreach ($books as $book)
                <li id="books-{{ $book->id }}">
                    {{ $book->title }}
                @if ( $book->chapters->isNotEmpty() )
                    <ul>
                    @foreach ($book->chapters as $chapter)
                        <li id="chapters-{{ $chapter->id }}">
                            {{ $chapter->title }}
                        @if ( $chapter->sections->isNotEmpty() )
                            <ul>
                            @foreach ($chapter->sections as $section)
                                <li id="sections-{{ $section->id }}">
                                    {{ $section->title }}
                                @if ( $section->subSections->isNotEmpty() )
                                    <ul>
                                        @foreach ($section->subSections as $subSection)
                                            <li id="subsections-{{ $subSection->id }}">
                                                {{ $subSection->title }}
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                                </li>
                            @endforeach
                            </ul>
                        @endif
                        </li>
                    @endforeach
                    </ul>
                @endif 
                </li>
            @endforeach
            @endif
        </ul>
    </div>
</div>

<div class="col-xs-12 mt-30 mb-20">
    <h6 class="text-teal-400 no-margin-bottom">Search by User filter:</h6>
    <input name="username" type="text" class="form-control" placeholder="Username">
</div>