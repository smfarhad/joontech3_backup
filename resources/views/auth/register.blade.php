<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Register</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/components.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/colors.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/login.js') }}"></script>

	<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js') }}"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">


	<!-- Page container -->
	<div class="page-container login-cover">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Registration form -->
					<form method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
						<div class="row">
							<div class="col-lg-4 col-lg-offset-4">
								<div class="panel registration-form">
									<div class="panel-body">
										<div class="text-center">
											<div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
											<h5 class="content-group-lg">Create account <small class="display-block">All fields are required</small></h5>
										</div>
										
										<div class="row">
											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input id="email" type="text" placeholder="Email Address" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
													<div class="form-control-feedback">
														<i class="icon-user-plus text-muted"></i>
													</div>
													@if ($errors->has('email'))
														<span class="help-block text-warning">
															<strong>{{ $errors->first('email') }}</strong>
														</span>
													@endif
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input id="token" type="text" placeholder="Registration Key" class="form-control" name="token" value="{{ old('token') }}" required>
													<div class="form-control-feedback">
														<i class="icon-key text-muted"></i>
													</div>
													@if ($errors->has('token'))
														<span class="help-block text-warning">
															<strong>{{ $errors->first('token') }}</strong>
														</span>
													@endif
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-6">
												<div class="form-group has-feedback">
                                                    <input id="first_name" type="text" placeholder="First name" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>
													<div class="form-control-feedback">
														<i class="icon-user-check text-muted"></i>
													</div>
                                                    @if ($errors->has('first_name'))
                                                        <span class="help-block text-warning">
                                                            <strong>{{ $errors->first('first_name') }}</strong>
                                                        </span>
                                                    @endif
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input id="last_name" type="text" placeholder="Last name" class="form-control" name="last_name" value="{{ old('last_name') }}" autofocus>
													<div class="form-control-feedback">
														<i class="icon-user-check text-muted"></i>
													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input id="password" type="password" class="form-control" name="password" required placeholder="Create password">
													<div class="form-control-feedback">
														<i class="icon-user-lock text-muted"></i>
													</div>
                                                    @if ($errors->has('password'))
                                                        <span class="help-block text-warning">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Repeat password" required>
													<div class="form-control-feedback">
														<i class="icon-user-lock text-muted"></i>
													</div>
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="checkbox">
												<label>
													<input type="checkbox" class="styled" name="terms">
													Accept <a href="#">terms of service</a>
													@if ($errors->has('terms'))
                                                        <span class="help-block text-warning">
                                                            <strong>{{ $errors->first('terms') }}</strong>
                                                        </span>
                                                    @endif
												</label>
											</div>
										</div>

										<div class="text-right">
											<a href="{{ route('login') }}" class="btn btn-link"><i class="icon-arrow-left13 position-left"></i> Back to login form</a>
											<button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b> Create account</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
					<!-- /registration form -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
