@include('layouts.head')

<body id="main-body" class="navbar-bottom">

<!-- Page header -->
<div class="page-header page-header-inverse">
@include('layouts.navbar')

<!-- Page container -->

</div>

<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        @include('layouts.sidebar')
        <div class="content-wrapper">
            <div class="panel panel-flat">
            @yield('header')
            <div class="content">
                @yield('content')
            </div>
            @yield('action-footer')
            </div>

        </div>
        @include('layouts.sidebar-opposite')
    </div>
    <!-- /page content -->
</div>
<!-- /page container -->
</body>
</html>