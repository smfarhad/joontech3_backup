@extends('main')

@section('title')
    {{-- Title here --}}
@stop

@section('css-links')
    {{-- Stylesheet links --}}
@stop

@section('sidebar')
    @include('layouts.toc-sidebar')
@stop

@section('javascript')
    <script>
        $(function () {
            $('#expand-collapse').on('click', function () {
                icon = $(this).find('i');
                if ($(icon).hasClass('icon-plus22')) {
                    $('.navigation').find('li').not('.active .category-title').has('ul').children('ul').removeClass('hidden-ul').css('display', '');
                    $('.navigation').find('li').has('ul').children('ul').removeClass('active').addClass('active');
                    $(icon).removeClass('icon-plus22').addClass('icon-dash');
                } else {
                    $(icon).removeClass('icon-dash').addClass('icon-plus22');
                    $('.navigation').find('li').not('.active .category-title').has('ul').children('ul').addClass('hidden-ul');
                    $('.navigation').find('li').addClass('active').removeClass('active');
                }
            });
        })

    </script>
    @yield('innerJavascript')
@stop


@section('header')
    <div class="content-group">
        <div class="page-header page-header-default page-header-xs"
             style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
            <div class="page-header-content">
                <div class="page-title">
                    <h5>
                        <span class="text-semibold">Table of content</span> - {{ $book->title }}
                    </h5>
                    <div class="heading-elements">
                        <button id="expand-collapse" type="button" class="btn btn-primary btn-icon legitRipple btn-xs">
                            <i class="icon-plus22"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="content">
        <div class="row">
            
            <div class="col-xs-12">
                <div class="panel panel-flat">
                    <div class="panel-body max-height-500 scroll">
                        <div class="sidebar-toc sidebar-default">
                            <div class="sidebar-content">

                                <!-- Main navigation -->
                                <div class="sidebar-category sidebar-category-visible">
                                    <div class="category-content no-padding">
                                        <ul class="navigation navigation-main">
                                            @if( $book )

                                                {{--@foreach ($book->chapters as $chapter)
                                                    <li class="testme">
                                                        <a href="{{ url('/book').'/'.$book->slug.'?chapter='.$chapter->slug }}">
                                                            {{ $chapter->title }}
                                                        </a>
                                                        @if( $chapter->sections->isNotEmpty() )
                                                            <ul>
                                                            @foreach( $chapter->sections as $section )
                                                            <li>
                                                                <a href="{{ url('/book').'/'.$book->slug.'?section='.$section->slug }}">
                                                                    {{ $section->title }}
                                                                </a>
                                                                @if( $section->subSections->isNotEmpty() )
                                                                    <ul>
                                                                    @foreach( $section->subSections as $subsection )
                                                                    <li>
                                                                        <a href="{{ url('/book').'/'.$book->slug.'?subSection='.$subsection->slug }}">
                                                                            {{ $subsection->title }}
                                                                        </a>
                                                                    </li>
                                                                    @endforeach
                                                                    </ul>
                                                                @endif
                                                            </li>
                                                            @endforeach
                                                            </ul>
                                                        @endif
                                                    </li>
                                                @endforeach--}}

                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <!-- /main navigation -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-4 dddd hidden">
                <div class="panel panel-flat">
                    <div class="panel-body max-height-500 scroll">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive" id="forum-sidebar">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('action-footer')
    {{-- Full width footer --}}
@stop