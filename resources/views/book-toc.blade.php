@extends('main')

@section('title')
    {{-- Title here --}}
@stop

@section('css-links')
    {{-- Stylesheet links --}}
@stop

@section('sidebar')
    @include('layouts.toc-sidebar')
@stop

@section('javascript')
    <script>
        $(function () {
            $('#expand-collapse').on('click', function () {
                icon = $(this).find('i');
                if ($(icon).hasClass('icon-plus22')) {
                    $('.navigation').find('li').not('.active .category-title').has('ul').children('ul').removeClass('hidden-ul').css('display', '');
                    $('.navigation').find('li').has('ul').children('ul').removeClass('active').addClass('active');
                    $(icon).removeClass('icon-plus22').addClass('icon-dash');
                } else {
                    $(icon).removeClass('icon-dash').addClass('icon-plus22');
                    $('.navigation').find('li').not('.active .category-title').has('ul').children('ul').addClass('hidden-ul');
                    $('.navigation').find('li').addClass('active').removeClass('active');
                }
            });

            $('.btn-forum').on('click', function () {
                $.post('{{ route('getForums') }}', {id: $(this).data('subsection-id')}, function (response) {
                    console.info('response: ', response);
                    $('#forum-sidebar').parents('.col-xs-4').first().removeClass('hidden').prev().attr('class', 'col-xs-8');
                    $('#forum-sidebar').html(response);
                });
            });

            $(document).on('click', '.subsection-forum', function () {
                $.post('{{ route('getThreads') }}', {id: $(this).data('forum-id')}, function (response) {
                    $('#forum-sidebar').html(response);
                });
            });


            $(document).on('click', '.view-posts', function () {
               
                $.post('{{ route('getPosts') }}', {id: $(this).data('thread-id')}, function (response) {
                    $('#forum-sidebar').html(response);
                });
            });
        })

    </script>
    @yield('innerJavascript')
@stop

@section('header')
    <div class="content-group">
        <div class="page-header page-header-default page-header-xs"
             style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
            <div class="page-header-content">
                <div class="page-title">
                    <h5>
                        <span class="text-semibold">Table of content</span> - {{ $book->title }}
                    </h5>
                    <div class="heading-elements">
                        <button id="expand-collapse" type="button" class="btn btn-primary btn-icon legitRipple btn-xs">
                            <i class="icon-plus22"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-flat">
                    <div class="panel-body max-height-500 scroll">
                        <div class="sidebar-toc sidebar-default">
                            <div class="sidebar-content">

                                <!-- Main navigation -->
                                <div class="sidebar-category sidebar-category-visible">
                                    <div class="category-content no-padding">
                                        <ul class="navigation navigation-main">
                                            @if( $book )
                                                {!! $tocMenu !!}
                                            @endif

                                        </ul>
                                    </div>
                                </div>
                                <!-- /main navigation -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-4 hidden">
                <div class="panel panel-flat">
                    <div class="panel-body max-height-500 scroll">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive" id="forum-sidebar">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('action-footer')
    {{-- Full width footer --}}
@stop