@extends('three-col-layout')

@section('title')
    {{-- Title here --}}
@stop

@section('css-links')
    {{-- Stylesheet links --}}
    <style>
        .btn-forum {
            cursor: pointer;
        }
    </style>
@stop

@section('javascript')
    <script type="text/javascript" async
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/components_affix.js')}}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/core.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/extensions/cookie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_childcounter.js') }}"></script>
   
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="../assets/js/pages/layout_sidebar_sticky_custom.js"></script>


    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            /* Open Topics */
            $('.sidebar-opposite-open').on('click', function () {
                openTopics($(this).data('slug'));
            });

            var url = window.location.hash;
            var hash = url.substring(url.indexOf('#'));

            // Opens topic automatically when using topic permalink
            if (hash) {
                slug = '{{ $request->subSection }}';
                page = '{{ $request->page }}';
                openTopics(slug, page);
            }

            function openTopics(slug, page) {
                console.info('function called1');

                var page_link = page ? '&page=' + page : '';
                var page = page || 1;
                var subsection_slug = slug;
                var url = window.location.hash;
                var reply_page = '{{ $request->reply_page }}';
                var reply = reply_page ? '&reply_page=' + reply_page : '';
                var hash = url.substring(url.indexOf('#'));
                history.pushState(null, null, location.pathname + '?subSection=' + subsection_slug + page_link + reply + hash);
                $.post("{{ route('showTopics') }}", {slug: subsection_slug, page: page})
                        .done(function (res) {
                            $('#topics').html(res);
                            $('#topic-create-wrapper').remove();
                            console.info('function called23');
                            MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
                        })

                $('#main-body').addClass('sidebar-opposite-visible');
            }

            /* End Open Topics */
            // Smooth scroll effect

        });

        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var tree_data = $(".tree-default").fancytree({
                extensions: ['filter'],
                quicksearch: true,
                init: function (event, data) {
                    $('.has-tooltip .fancytree-title').tooltip();
                },
                icon: false,
                activate: function (event, data) {
                    var link = data.node.data.link;
                    window.open(link, '_blank');
                },
            });

            var tree = $("#book-tree").fancytree("getTree");


            $("input#filter").keyup(function (e) {
                var n,
                        tree = $.ui.fancytree.getTree(),
                        args = "autoApply autoExpand fuzzy hideExpanders highlight leavesOnly nodata".split(" "),
                        filterFunc = $("#branchMode").is(":checked") ? tree.filterBranches : tree.filterNodes,
                        match = $(this).val();

                if (match != '') {
                    $('#btn-reset-filter').removeClass('hidden');
                    $('#book-tree').removeClass('hidden');
                } else {
                    $('#btn-reset-filter').trigger("click");
                }

                opts = {
                    autoApply: true,   // Re-apply last filter if lazy data is loaded
                    autoExpand: true, // Expand all branches that contain matches while filtered
                    counter: true,     // Show a badge with number of matching child nodes near parent icons
                    fuzzy: false,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                    hideExpandedCounter: true,  // Hide counter badge if parent is expanded
                    hideExpanders: false,       // Hide expanders if all child nodes are hidden by filter
                    highlight: true,   // Highlight matches by wrapping inside <mark> tags
                    leavesOnly: false, // Match end nodes only
                    nodata: true,      // Display a 'no data' status node if result is empty
                    mode: "hide"       // Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
                };

                if (e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === "") {
                    $("button#btnResetSearch").click();
                    return;
                }
                if ($("#regex").is(":checked")) {
                    // Pass function to perform match
                    n = filterFunc.call(tree, function (node) {
                        return new RegExp(match, "i").test(node.title);
                    }, opts);
                } else {
                    // Pass a string to perform case insensitive matching
                    n = filterFunc.call(tree, match, opts);
                }
                $("#btn-reset-filter").attr("disabled", false);
                $("span#matches").text(+n + " matches");
            }).focus();

            $("#btn-reset-filter").click(function (e) {
                $("input#filter").val("");
                $("span#matches").text("");
                tree.clearFilter();
                $(this).addClass('hidden');

                $('#book-tree').addClass('hidden');
            }).attr("disabled", true);


            $("fieldset input:checkbox").change(function (e) {
                var id = $(this).attr("id"),
                        flag = $(this).is(":checked");

                // Some options can only be set with general filter options (not method args):
                switch (id) {
                    case "counter":
                    case "hideExpandedCounter":
                        tree.options.filter[id] = flag;
                        break;
                }
                tree.clearFilter();
                $("input[name=search]").keyup();
            });

            $('.btn-forum').on('click', function () {
                $('#subSectionId').val($(this).data('subsection-id'));
                renderThreads($(this).data('subsection-id'));
            });

            $(document).on('click', '.subsection-forum', function () {
                $.post('{{ route('getThreads') }}', {id: $(this).data('forum-id')}, function (response) {
                    console.info('repsonse: ', response);
                    if (response.status == true) {
                        $('#forum-sidebar').html(response);
                    } else {
                        $('#forum-sidebar').html('<div class="alert alert-danger">No Forum available</div>');
                    }
                });
            });

            function renderThreads(subSectionId)
            {
                $.post('{{ route('getThreads') }}', {id: subSectionId}, function (response) {
                    $('#forum-sidebar-container').removeClass('hidden');
                    $('#content-container').attr('class', 'col-md-7 col-xs-7');
                    $('#forum-sidebar').html(response);
                });
            }

            function renderPosts(threadId) {
                $.post('{{ route('getPosts') }}', {id: threadId}, function (response) {
                    $('#forum-sidebar').html(response);
                    CKEDITOR.replace('reply-comment');
                });
            }

            function scrollToEditor() {
                $('html, body').animate({
                    scrollTop: $("#post-quick-reply").offset().top
                }, 1000);
            }

            function hideReplyToSection() {
                //hidding reply To section
                $('#replying_to').html('');
                $('#reply-to-container').addClass('hidden');
            }

            $(document).on('click', '.view-posts', function (e) {
                e.preventDefault();
                renderPosts($(this).data('thread-id'));
            });

            $(document).on('click', '.view-posts-comments', function (e) {
                e.preventDefault();
                console.log($(this).data('thread-id'));
            })

            $(document).on('click', '#quick-reply-btn', function () {
                var replyContent = CKEDITOR.instances['reply-comment'].getData();
                $.post('{{ route('postReply')  }}', {
                    data: $(document).find('#post-quick-reply').serialize(),
                    reply: replyContent
                }, function (response) {
                    if (response.status == true) {
                        renderPosts(response.thread_id);
                    }
                });
            });

            $(document).on('click', '.edit-comment', function (e, response) {
                e.preventDefault();
                let comment = $(this).parents('ul').first().prev().html();
                let postId = $(this).data('comment-id');
                $(document).find('#reply-section-title').html('Edit Comment');
                $(document).find('#forum-post-id').val(postId);

                CKEDITOR.instances['reply-comment'].setData(comment);
                hideReplyToSection();
                scrollToEditor();
            });

            /*
             *  To be done later.
             */
            /**
             * @todo replies will be done later.
             */
            /*$(document).on('click', '.reply-comment', function (e) {
             e.preventDefault();
             let commentId = $(this).data('comment-id');
             $('#forum-comment-id').val(commentId);
             $('#replying_to').html($(this).parents('.media-body').find('.thread-comment').html());
             $('#reply-to-container').removeClass('hidden');
             $('#reply-section-title').html('Replying To');

             scrollToEditor();
             });*/


            $(document).on('click', '.delete-comment', function (e) {
                e.preventDefault();
                let _threadId = $(this).data('thread-id');
                let commentId = $(this).data('comment-id');
                let _action = $(this).data('action');

                $.post('{{ route('deletePost') }}', {threadId: _threadId, postId : commentId, action: _action}, function (response) {
                    if(_action == 'permadelete') {
                        renderThreads($('#subSectionId').val());
                    } else {
                        renderPosts(_threadId);
                    }
                });
            });

            $(document).on('click', '.btn-action', function (e) {
                e.preventDefault();
                let _action = $(this).data('action');
                let _threadId = $(this).data('thread-id');
                let _actionValue = $(this).data('value');

                $.post('{{ route('pinThread') }}', {
                    action: _action,
                    actionValue: _actionValue,
                    threadId: _threadId
                }, function (response) {
                    if (response.status == true) {
                        renderPosts(_threadId);
                    }
                });
            });

            $(document).on('click', '.new-thread', function () {
                let _forumId = $(this).data('forum-id');
                $.post('{{ route('createThread') }}', {forumId: _forumId}, function (response) {
                    $('#forum-sidebar').html(response);
                })
            });

            $(document).on('click', '#edit-thread', function () {
                let _threadId = $(this).data('thread-id');
                let _postId = $(this).data('post-id');
                let _comment = $('#post-content').html();
                $(document).find('#forum-post-id').val(_postId);

                console.info(_comment);

                $('#reply-section-title').html('Edit Post');
                $('#quick-reply-btn').html('Update');

                CKEDITOR.instances['reply-comment'].setData(_comment);
                scrollToEditor();
            });


            $(document).on('click', '.create-thread-btn', function () {
                $.post('{{route('storethread')}}', {data: $('#new-thread-form').serialize()}, function (response) {
                    if (response.status == true) {
                        renderThreads($('#subSectionId').val());
                    }
                })
            });

            $(document).on('click', '#reply-cancel-btn', function () {
                CKEDITOR.instances['reply-comment'].setData('');
                $(document).find('#reply-section-title').html('Quick reply');
                hideReplyToSection();
            });
        });

    </script>
@stop

@section('header')

@stop

@section('content')
    <div class="panel-body" id="book-content-container">
        <div class="row">
            <div class="book-chapter col-md-12 col-sm-12 col-xs-12"> 
          
                @if(isset($subSectionTree))
                    <ul class="media-list">
                        @foreach($subSectionTree as $subSection)
                            <li class="media panel-body stack-media-on-mobile">
                                <div class="media-body">
                                    @if(isset($subSection->title))
                                        <h6 class="media-heading text-semibold">
                                            {{$subSection->title}}
                                        </h6>
                                    @endif

                                    @if(isset($subSection->children) && !empty($subSection->children))
                                        <ul class="media-list">
                                            @foreach($subSection->children as $subSec)
                                                @if(isset($subSec->title))

                                                    <li class="media panel-body stack-media-on-mobile">
                                                        <div class="media-body">
                                                            <h6 class="media-heading text-semibold">
                                                                {{$subSec->title}}
                                                            </h6>

                                                            {!! $subSec->description !!}
                                                        </div>

                                                        <div class="media-right text-nowrap">
                                                            <span class="label bg-blue btn-forum"
                                                                  data-subsection-id="{{$subSec->element_id}}">
                                                                    {{\App\HelperClass::getCommentsCount($subSec->element_id)}}
                                                                Comments
                                                            </span>
                                                        </div>
                                                    </li>

                                                @endif
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </li>
                        @endforeach
                    </ul>
                @endif

            </div>

        </div>
    </div>

@stop

@section('action-footer')
    {{-- Full width footer --}}
@stop