@extends('errors/error_base')

@section('title')
    Page not found
@stop

@section('content')
    <!-- Error title -->
    <div class="text-center content-group">
        <h1 class="error-title">403</h1>
        <h5>Forbidden error</h5>
    </div>
    <!-- /error title -->


    <!-- Error content -->
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
            <form action="#" class="main-search panel panel-body">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control input-lg" placeholder="Search...">
                    <div class="form-control-feedback">
                        <i class="icon-search4 text-size-large text-muted"></i>
                    </div>
                </div>

                <div class="text-center">
                    <a href="#" class="btn bg-pink-400"><i class="icon-circle-left2 position-left"></i> Back to dashboard</a>
                </div>
            </form>
        </div>
    </div>
    <!-- /error wrapper -->
@stop