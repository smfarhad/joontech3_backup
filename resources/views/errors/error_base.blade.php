@include('admin.layouts.head')

<body class="navbar-top">

    @include('admin.layouts.navbar')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">


			<!-- Main content -->
			<div class="content-wrapper p-relative">

                @yield('header')            

				<!-- Content area -->
				<div class="content">
                    @yield('content')

                </div>

                <div class="col-xs-12 footer text-muted text-center">
                    &copy; 2017. <a href="#">Text Book</a> by <a href="#" target="_blank">Klaus Andrew</a>
                </div>

				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
