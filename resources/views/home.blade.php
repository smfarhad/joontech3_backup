@extends('main')

@section('title')
    {{-- Title here --}}
@stop

@section('css-links')
    {{-- Stylesheet links --}}
@stop

@section('javascript')
    {{-- Script Links and Javascript --}}
@stop

@section('header')
@stop

@section('content')
    <style>

    </style>
<div class="content box"> 
    <!-- Image grid -->
    <h6 class="content-group text-semibold">
        Books
    </h6>


    <div class="books">
        <div class="row">
            @foreach ( $books as $book )
            <div class="col-lg-2 col-sm-6">
                <div class="thumbnail">
                    <div class="thumb">
                        <a href="href="book/{{ $book->slug }}">
                        <img src="{{ asset('images/'.bookImage($book->image)) }}" class="">
                        </a>
                        <div class="caption-overflow">
                            <span>
                                <a href="book/{{ $book->slug }}" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
                            </span>
                        </div>
                    </div>

                    <div class="caption">
                        <h6 class="no-margin-top text-semibold"><a href="book/{{ $book->slug }}" class="text-default">{{ $book->title }}</a></h6>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="col-xs-12 text-center mt-20">
            {{ $books->links() }}
        </div>
    </div>
</div>
<!-- /image grid -->
<!-- Footer -->
<div class="footer text-muted">
    &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
</div>
<!-- /footer -->


@stop

@section('action-footer')
    {{-- Full width footer --}}
@stop