<!-- Opposite sidebar -->
<div class="sidebar forum sidebar-opposite sidebar-default hidden">
    <div class="sidebar-fixed afixed-top">
        <div class="sidebar-box">
            <div class="sidebar-content">

                <div id="topics">
                    <!-- Topic here -->
                </div>

                <div id="topic-wrapper" class="sidebar-category">
                    <!-- Create new Topic here -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /opposite sidebar -->
<input type="hidden" name="subSectionId" id="subSectionId" value="" />
<div id="forum-sidebar-container" class="hidden">
    <div class="row">
        <div class="col-xs-5">             
            <div class="table-responsive" id="forum-sidebar"></div>
        </div>

    </div>
</div>