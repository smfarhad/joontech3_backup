<!-- /main sidebar -->
{{--
<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-title h6">
                <span>Table of content</span>
                --}}
{{--<ul class="icons-list">
                    <li><a href="#" data-action="collapse"></a></li>
                </ul>--}}{{--

                --}}
{{--<ul class="navigation toc">
                    <li class="navigation-header"><span>Table of content</span> <a
                                class="sidebar-control sidebar-main-toggle hidden-xs legitRipple"><i
                                    class="icon-arrow-left52 icon-md"></i></a></li>
                </ul>--}}{{--

            </div>

            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion toc">
                    <li class="navigation-header"> <a
                                class="sidebar-control sidebar-main-toggle hidden-xs legitRipple"><i
                                    class="icon-arrow-left52 icon-md"></i></a></li>
                    {!! $tocMenu !!}
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>--}}


<!-- Main sidebar -->

<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-fixed">
        <div class="sidebar-content">

            <!-- Main navigation -->
            <div class="sidebar-category sidebar-category-visible">
                <div class="category-title h6">
                    <span>Main sidebar</span>
                    <ul class="icons-list">
                        <li><a href="#" data-action="collapse"></a></li>
                    </ul>
                </div>

                <div class="category-content no-padding">
                    <ul class="navigation navigation-main navigation-accordion">

                        <!-- Main -->
                        {{--<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                        <li><a href="../index.html"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                        <li>
                            <a href="#"><i class="icon-stack"></i> <span>Starter kit</span></a>
                            <ul>
                                <li><a href="1_col.html">1 column</a></li>
                                <li><a href="2_col.html">2 columns</a></li>
                                <li>
                                    <a href="#">3 columns</a>
                                    <ul>
                                        <li><a href="3_col_dual.html">Dual sidebars</a></li>
                                        <li><a href="3_col_double.html">Double sidebars</a></li>
                                    </ul>
                                </li>
                                <li><a href="4_col.html">4 columns</a></li>
                                <li><a href="layout_boxed.html">Boxed layout</a></li>
                                <li class="navigation-divider"></li>
                                <li><a href="layout_navbar_fixed_main.html">Fixed top navbar</a></li>
                                <li><a href="layout_navbar_fixed_secondary.html">Fixed secondary navbar</a></li>
                                <li><a href="layout_navbar_fixed_both.html">Both navbars fixed</a></li>
                                <li class="active"><a href="layout_sidebar_sticky.html">Sticky sidebar</a></li>
                            </ul>
                        </li>
                        <li><a href="../changelog.html"><i class="icon-list-unordered"></i> <span>Changelog</span></a></li>--}}
                        <!-- /main -->

                        {!! $tocMenu !!}

                    </ul>
                </div>
            </div>
            <!-- /main navigation -->

        </div>
    </div>
</div>
<!-- /main sidebar -->