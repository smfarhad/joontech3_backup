@include('layouts.head')

<body id="main-body" class="navbar-bottom  pace-done">

<div class="page-header page-header-inverse">

	@include('layouts.navbar')
</div>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
			@yield('sidebar')
            <div class="content-wrapper plain">
                 @yield('header')
                
                @yield('content')

                @yield('action-footer')
            </div>

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
