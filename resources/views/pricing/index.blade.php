<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>JoonTeach2 - Price Packages</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <!-- /theme JS files -->
</head>

<body>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Color variations -->
                <h6 class="content-group text-semibold">
                    Subscribe to a Package
                    <small class="display-block">Select a pricing Package Please</small>
                </h6>

                <div class="row pricing-table">
                    <div class="col-sm-6 col-lg-4">
                        <div class="panel text-center">
                            <div class="panel-body">
                                <h4>Trial</h4>
                                <h1 class="pricing-table-price"><span>$</span>0</h1>
                                <ul class="list-unstyled content-group">
                                    <li><strong>7 Days</strong> Access</li>
                                    <li><strong>Try</strong> before you Buy</li>
                                    <li><strong></strong></li>
                                    <li><strong></strong></li>
                                    <li><strong></strong></li>
                                    <li><strong></strong></li>
                                </ul>
                                <a href="#" class="btn bg-pink-400 btn-lg text-uppercase text-size-small text-semibold disabled">Coming Soon</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-4">
                        <div class="panel text-center">
                            <div class="panel-body">
                                <h4>Promotion</h4>
                                <h1 class="pricing-table-price"><span>$</span>0</h1>
                                <ul class="list-unstyled content-group">
                                    <li><strong>3 Month</strong> Access</li>
                                    <li><strong>Open To Certain Colleges</strong></li>
                                    <li><strong></strong></li>
                                    <li><strong></strong></li>
                                    <li><strong></strong></li>
                                    <li><strong></strong></li>
                                </ul>
                                <a href="#" class="btn bg-pink-400 btn-lg text-uppercase text-size-small text-semibold">I have a Promo Code</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-4">
                        <div class="panel text-center bg-pink-400">
                            <div class="panel-body">
                                <h4>Subscribe</h4>
                                <h1 class="pricing-table-price"><span>$10/month</span></h1>
                                <ul class="list-unstyled content-group">
                                    <li><strong>Support Our Publishing Team</strong></li>
                                    <li><strong>Support Our Software Development</strong></li>
                                    <li><strong>Create an Educational Revolution</strong></li>
                                    <li><strong>Get Pricing Help</strong></li>
                                </ul>
                                <a href="#" class="btn bg-pink-800 disabled btn-lg text-uppercase text-size-small text-semibold">Coming Soon</a>

                                <div class="ribbon-container">
                                    <div class="ribbon bg-white">Popular</div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /color variations -->

                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>