
<div id="reply-edit-wrap">
<script>
    $(function() {
        CKEDITOR.replace( 'reply-edit', {
            extraPlugins: 'mathjax',
            mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML',
            height: 220,
            toolbar :
            [
                { name: 'basicstyles', items : [ 'Bold','Italic' ] },
                { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
                { name: 'insert', items : [ 'Image','Table','Mathjax' ] },
            ]
        });
        
        
        if ( CKEDITOR.env.ie && CKEDITOR.env.version == 8 ) {
            document.getElementById( 'ie8-warning' ).className = 'tip alert';
        }

        $('#replyUpdate').on('submit',function(e){
            e.preventDefault();
            var form = $(this);
            var id = form.find('input[name=id]').val();
            var box = form.closest('.topic-reply-box');
            //var topic_page = getParameterByName('page');
            var reply = CKEDITOR.instances['reply-edit'].getData()
                $.post("{{ route('replyUpdate') }}",{reply:reply,id:id})
                    .done(function(res){
                        box.html(res);
                        $('#topic-create-wrapper').remove();
                        MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
                    });
        });

        // Cancel Button
        $('.btn-reply-cancel').on('click',function(){
            $(this).closest('.media-body').find('.reply-box').removeClass('hidden');
            $('#reply-edit-wrap').remove();
        });
    });

</script>

<div id="reply-create-wrapper">
    <form id="replyUpdate" method="POST" class="category-content">
        <input type="hidden" name="id" value="{{ $reply->id }}">
        <div class="form-group">
            <textarea cols="10" id="reply-edit" rows="10" data-sample="1" data-sample-short="">
                {!! $reply->reply !!}
            </textarea>
        </div>

        <div class="row mt-20">
            <div class="col-xs-12">
                    <button 
                    type="submit" 
                    class="btn btn-success btn-xs heading-btn pull-right legitRipple btn-reply-submit"
                    data-id="{{ $reply->id }}"
                    data-slug="{{ $reply->slug }}"
                    >Submit</button>

                    <button 
                    type="button" 
                    class="btn btn-danger btn-xs heading-btn pull-right legitRipple btn-reply-cancel mr-15"
                    data-id="{{ $reply->id }}"
                    >Cancel</button>
            </div>
        </div>
    </form>
</div>
</div>