<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML"> </script>

<div class="panel panel-body">
    <p class="text-muted text-size-small">Search Result:</p>

    <hr>
    @if( $results->isNotEmpty() )
    <ul class="media-list search-results-list content-group">
        
        @foreach( $results as $result )
        <li class="media">
            <div class="media-body">
                <h6 class="media-heading">
                    <a href="{{ url('/book').'/'.$result->book_slug.'?subSection='.$result->slug.'&search='.$search }}">{{ $result->book_title }}</a>
                </h6>
                <p><strong>{{ $result->chapter_title }}</strong> - {{ $result->section_title }}</p>
                <ul class="list-inline list-inline-separate text-muted">
                    <li><a href="{{ url('/book').'/'.$result->book_slug.'?subSection='.$result->slug.'&search='.$search }}" class="text-success">{{ url('/book').'/'.$result->book_slug.'?subSection='.$result->slug }}</a></li>
                </ul>

                {!! searchResult($result->data,'findme') !!}
            </div>
        </li>
        @endforeach()
        
    </ul>

    <div class="text-center pb-10 pt-10">
        {{ $results->render() }}
    </div>
    @else
        <h5 class="text-center text-muted col-xs-12">No result found</h5>
    @endif
</div>