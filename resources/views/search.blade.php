@extends('main')

@section('title')
    {{-- Title here --}}
@stop

@section('css-links')
    {{-- Stylesheet links --}}
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/core.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/extensions/cookie.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_childcounter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_filter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $advance_once = 1;
            $('#action_advance_search').on('click',function(){
                $( "#advance-filter-wrap" ).toggleClass( 'hidden' );
                $( "#search-footer" ).toggleClass( 'hidden' );

                if( $advance_once <= 1 ){
                    $.post("{{ route('advanceSearch') }}")
                        .done(function(res){
                            $('#advance-filter-wrap').html(res);
                        });
                }
                $advance_once++;
            });
            
            $('#form-search').on('keydown','input[type=text]', function(e) {
                if (e.which == 13) {
                    doSearch();
                    e.preventDefault();
                }
            });

            $('#action_search, #action_search2').on('click',function(){
                doSearch();
            });

            function doSearch(){
                var form = $('#form-search');
                var keyword = $('input[name=keyword]',form).val();
                var not_advance = $('#advance-filter-wrap').hasClass('hidden');
                var data = {};

                data['keyword'] = keyword;
                if( not_advance ){
                    if( keyword == '' ){
                        swal("Opps!", "You need to input something on the search field.", "error");
                        throw 'Search field is important.';
                    }
                }else{
                    var userName = $('input[name=username]',form).val();
                    data['user_name'] = userName;
                    data['advance'] = true;
                    if( keyword == '' && userName == '' ){
                        swal("Keyword and username are empty", "Please input one or both", "error");
                        throw 'Search field is important.';
                    }
                    
                    // Tree selected nodes
                    var selectedNodes = $("#search-tree").fancytree("getTree").getSelectedNodes();
                    var categories = {
                        books : [],
                        chapters : [],
                        sections : [],
                        subsections: []
                    };

                    var selKeys = $.map(selectedNodes, function(node){
                        key = node.key.split('-');
                        categories[key[0]].push(key[1]);
                    });
                    data['categories'] = categories;
                }

                $.post("{{ route('doSearch') }}",{formData:data})
                    .done(function(res){
                        $('#search-result').html(res);
                    });
            }

        });
    </script>
@stop

@section('header')
@stop

@section('content')
<div class="content box">
    <div class="books">
        <div class="row">

        <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Find Content</h5>
                </div>
            
                <div class="panel-body">
                    <form id="form-search" action="{{ route('doSearch') }}" method="GET" class="main-search">
                        <div class="col-xs-12">
                            <div class="input-group content-group">
                                <div class="has-feedback has-feedback-left">
                                    <input name="keyword" type="text" class="form-control input-xlg" placeholder="Search...">
                                    <div class="form-control-feedback">
                                        <i class="icon-search4 text-muted text-size-base"></i>
                                    </div>
                                </div>
                                <div class="input-group-btn">
                                    <button id="action_search" type="button" class="btn btn-primary btn-icon dropdown-toggle legitRipple" data-toggle="dropdown">
                                        <i class="icon-search4"></i> &nbsp; Search
                                    </button>
                                    <button id="action_advance_search" type="button" class="btn bg-teal-400 btn-icon dropdown-toggle legitRipple" data-toggle="dropdown">
                                        <i class="icon-menu7"></i> &nbsp; Advanced
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div id="advance-filter-wrap" class="hidden">

                        </div>
                    </form>

                    <div id="search-footer" class="hidden pull-right">
                        <button id="action_search2" type="button" class="btn btn-primary btn-icon dropdown-toggle legitRipple" data-toggle="dropdown">
                            <i class="icon-search4"></i> &nbsp; Search
                        </button>
                    </div>
                </div>
            </div>

            <div id="search-result">
                <!-- Search result Here -->
            </div>
        </div>
    </div>
</div>


@stop

@section('action-footer')
    {{-- Full width footer --}}
@stop