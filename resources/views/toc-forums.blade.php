<?php
/**
 * Created by PhpStorm.
 * User: msherax
 * Date: 3/21/18
 * Time: 1:06 PM
 */ ?>


<div class=>{{$parent}}</div>
<table class="table table-sm table-condensed">
    @if($forums)
        @foreach($forums as $forum)
            <tr>
                <td>
                    <a href="#" data-forum-id="{{$forum->id}}" class="subsection-forum">{{$forum->title}}</a>
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td>No Forums exists for this Section</td>
        </tr>
    @endif
</table>
