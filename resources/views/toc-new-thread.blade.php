<div class="content-wrapper">
<!-- Single mail -->
<div class="panel panel-white">
<form class="form-horizontal" action="#" id="new-thread-form" name="new-thread-form">
    <!-- Mail toolbar -->
    <div class="panel-toolbar panel-toolbar-inbox">
        <div class="navbar navbar-default navbar-component no-margin-bottom">
            <ul class="nav navbar-nav visible-xs-block no-border">
                <li>
                    <a class="text-center collapsed" data-toggle="collapse" data-target="#inbox-toolbar-toggle-single">
                        <i class="icon-circle-down2"></i>
                    </a>
                </li>
            </ul>

            <div class="navbar-collapse collapse" id="inbox-toolbar-toggle-single">
                <div class="btn-group navbar-btn">
                    <button type="button" class="btn btn-default btn-xs"><i class="icon-cross2"></i> <span class="hidden-xs position-right">Cancel</span></button>
                </div>
                <div class="btn-group navbar-btn pull-right">
                    <button type="button" class="btn btn-default btn-xs"><i class="icon-new-tab2"></i> <span class="hidden-xs position-right">Map</span></button>
				</div>
            </div>
        </div>
    </div>
    <!-- /mail toolbar -->

    <!-- Mail details -->
    <div class="table-responsive mail-details-write">
        <table class="table">
            <tbody>
                <tr>
                    <td style="width: 70px">Title:</td>
                    <td class="no-padding">
                        <input type="text" class="form-control" name="title" id="title" placeholder="Add Thread Title">
                    </td>    
                </tr>
            </tbody>
        </table>
    </div>
    <!-- Mail container -->
    <div class="mail-container-write">
        <textarea class="summernote" name="description" id="description"></textarea>
        <input type="hidden" name="forumId" id="forumId" value="{{$forumId}}" />
    </div>
    <div class="btn-group navbar-btn">
        <button type="button" class="btn btn-primary create-thread-btn"><i class="icon-checkmark3 position-left"></i> Submit</button>        
</form>
</div>
<!-- /single mail -->

</div>
<script type="text/javascript" src="{{ asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
<script>
    $(document).ready(function() {
        // Summernote editor
        $('.summernote').summernote({
            height: 350,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline']],
                ['fontsize', ['fontsize']]
            ]
        });
    });
</script>


