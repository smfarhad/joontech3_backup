<?php
/**
 * Created by PhpStorm.
 * User: msherax
 * Date: 3/21/18
 * Time: 2:25 PM
 */ ?>

<!-- /theme JS files -->
<div class="panel-body">
    <div class="panel-toolbar panel-toolbar-inbox">
        <div class="navbar navbar-default navbar-component no-margin-bottom">
            <ul class="nav navbar-nav visible-xs-block no-border">
                <li>
                    <a class="text-center collapsed" data-toggle="collapse" data-target="#inbox-toolbar-toggle-single">
                        <i class="icon-circle-down2"></i>
                    </a>
                </li>
            </ul>
            <div class="navbar-collapse collapse" id="inbox-toolbar-toggle-single">
            <!-- Btn custom start -->  

            @if($post[0]->thread->locked != 1)
            <p>@if($post[0]->thread->pinned == 1)
                        <a href="#" class="btn btn-info btn-action custom-btn" data-action="pinned" data-value="0"
                           data-thread-id="{{$threadId}}"><i class="icon-pushpin"></i>
                            <span class="hidden-xs position-right">Pin</span>
                        </a>
                    @else
                        <a href="#" class="btn btn-default btn-action custom-btn" data-action="pin" data-value="1"
                           data-thread-id="{{$threadId}}"><i class="icon-pushpin"></i>
                            <span class="hidden-xs position-right">Pin</span>
                        </a>
                    @endif

                    @if($post[0]->thread->locked == 1)
                        <a href="#" class="btn btn-info btn-action custom-btn" data-action="unlock" data-value="0"
                           data-thread-id="{{$threadId}}"><i class="icon-unlocked"></i>
                            <span class="hidden-xs position-right">Lock</span>
                        </a>
                    @else
                        <a href="#" class="btn btn-default btn-action custom-btn" data-action="lock" data-value="1"
                           data-thread-id="{{$threadId}}"><i class="icon-lock4"></i>
                            <span class="hidden-xs position-right">Lock</span>
                        </a>
                    @endif
                    
                    <a href="#quick-reply" class="btn btn-default custom-btn">
                            <i class="icon-reply-all"></i>
                            <span class="hidden-xs position-right">Reply</span>
                        </a>

                    </p>
                <span>
                    <a href="#" id="edit-thread" data-thread-id="{{$threadId}}" data-post-id="{{$post[0]->id}}" 
                    class="btn btn-warning custom-btn">
                        <i class="icon-pencil"></i> Edit</a>
                        @if(empty($post[0]->deleted_at))
                            <a href="#" class="delete-comment btn btn-danger btn-action custom-btn" data-thread-id="{{$threadId}}"
                            data-comment-id="{{$post[0]->id}}"
                            data-action="delete"><i class="icon-bin"></i> Delete</a>
                        @else
                            <a href="#" class="delete-comment btn btn-danger btn-action custom-btn" data-thread-id="{{$threadId}}"
                            data-comment-id="{{$post[0]->id}}"
                            data-action="restore"><i class="icon-bin"></i> Restore</a>

                            <a style="color:#fff;" href="#" class="delete-comment btn btn-danger btn-action custom-btn" data-thread-id="{{$threadId}}"
                            data-comment-id="{{$post[0]->id}}" data-action="permadelete"><i class="icon-bin" ></i> Delete Permanent</a>

                        @endif
                    
                        


            </span>
            <br>
            <div class="">
     
                    
                </div>
            @endif


            <!-- Btn custom end -->      

            </div>
        </div>
    </div>

    <div class="content-group-lg">
        <h3 class="text-semibold mb-5">
            <a href="#" class="text-default">{{$post[0]->title}}</a>
        </h3>
        <ul class="list-inline list-inline-separate text-muted content-group">
            <li>By <a href="#"
                      class="text-muted">{{$post[0]->author->first_name . ' ' . $post[0]->author->last_name}}</a></li>
            <li>
                {{ Carbon\Carbon::parse($post[0]->created_at)->format('M d, Y') }}
            </li>
            <li><a href="#" class="text-muted">{{ $totalComments }} Comments</a></li>
        </ul>
        <ul class="list-inline list-inline-separate text-muted content-group">
            <li>Posted : {{$post[0]->posted}}</li>
        </ul>

        <div class="content-group">
            <div id="post-content">
                {!! $post[0]->content !!}
            </div>

            
        </div>
    </div>
</div>

<div class="panel panel-flat">
   
    <div class="panel-heading">
        <h6 class="panel-title text-semiold">Discussion<a class="heading-elements-toggle"><i
                        class="icon-more"></i></a><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
        <div class="heading-elements">
            <ul class="list-inline list-inline-separate heading-text text-muted">
                <li>{{ $totalComments }} Comments</li>
                
            </ul>
        </div>
    </div>

    <div class="panel-body">
        @if($comments)
            <ul class="media-list stack-media-on-mobile">
                @foreach($comments as $comment)
                    <li class="media">
                        <div class="media-left">
                            <a href="#"><img src="/assets/images/user-profile.jpg"
                                             class="img-circle img-sm" alt=""></a>
                        </div>

                        <div class="media-body">
                            <div class="media-heading">
                                <a href="#"
                                   class="text-semibold">{{$comment->author->first_name . ' ' . $comment->author->last_name }}</a>
                                <span class="media-annotation dotted">{{$comment->posted}}</span>
                            </div>

                            <div class="thread-comment">{!! $comment->content !!}</div>

                            @if($post[0]->thread->locked == 0)
                                <ul class="list-inline list-inline-separate text-size-small">
                                    <li>
                                        <a href="#" class="edit-comment" data-thread-id="{{$threadId}}"
                                           data-comment-id="{{$comment->id}}">Edit</a>
                                    </li>

                                    @if($comment->deleted_at == '')
                                        <li>
                                            <a href="#" class="delete-comment" data-thread-id="{{$comment->thread_id}}"
                                               data-comment-id="{{$comment->id}}"
                                               data-action="delete">Delete</a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="#" class="delete-comment" data-thread-id="{{$comment->thread_id}}"
                                               data-comment-id="{{$comment->id}}"
                                               data-action="restore">Restore</a>
                                        </li>

                                        <li>
                                            <a href="#" class="delete-comment" data-thread-id="{{$comment->thread_id}}"
                                               data-comment-id="{{$comment->id}}"
                                               data-action="permadeletepost" style="color:red">Delete Permanent</a>
                                        </li>

                                    @endif
                                </ul>
                            @endif

                            {{--@if($comment->children)
                                @foreach($comment->children as $reply)
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#"><img src="assets/images/placeholder.jpg"
                                                             class="img-circle img-sm" alt=""></a>
                                        </div>
                                        <div class="media-body">
                                            <div class="media-heading">
                                                <a href="#"
                                                   class="text-semibold">{{$reply->author->first_name . ' ' . $reply->author->last_name}}</a>
                                                <span class="media-annotation dotted">{{$reply->posted}}</span>
                                            </div>

                                            {!! $reply->content !!}

                                            @if($post[0]->thread->locked == 0)
                                                <ul class="list-inline list-inline-separate text-size-small">
                                                    <li>
                                                        <a href="#" class="reply-comment"
                                                           data-comment-id="{{$comment->id}}">Reply</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="edit-comment"
                                                           data-comment-id="{{$reply->id}}">Edit</a>
                                                    </li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endif--}}
                        </div>
                    </li>
                @endforeach
            </ul>

        @endif
    </div>

    <hr class="no-margin">
    @if($post[0]->thread->locked == 0)
        <div class="panel-body">
            <form id="post-quick-reply" name="post-quick-reply" action="" method="POST">
                <h4 class="no-margin-top content-group" id="reply-section-title">Quick reply</h4>
                <div id="reply-to-container" class=" hidden">
                    <div id="replying_to" class="well border-top-lg border-top-danger"></div>
                </div>
                <div id="quick-reply" class="mt-20">
                    <div class="form-group">
                        <textarea name="content" class="form-control ckeditor" id="reply-comment"></textarea>
                    </div>

                    <div class="text-right">
                        <button type="button" class="btn btn-success pull-right" id="quick-reply-btn">Reply</button>
                        <button type="button" class="btn btn-default pull-left" id="reply-cancel-btn">Cancel</button>
                        <input type="hidden" name="thread_id" id="thread_id" value="{{$threadId}}"/>
                        <input type="hidden" name="post_id" id="forum-post-id" value="0"/>
                        <input type="hidden" name="comment_id" id="forum-comment-id" value="0"/>
                        <input type="hidden" name="mode" id="mode" value="0"/>
                    </div>
                </div>
            </form>

        </div>
    @endif
</div>
