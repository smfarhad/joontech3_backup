<?php
/**
 * Created by PhpStorm.
 * User: msherax
 * Date: 3/21/18
 * Time: 1:40 PM
 */ ?>

@if($threads)
    <table class="table table-condensed table-sm">
        <button class="btn btn-primary btn-sm new-thread" data-forum-id="{{$forumId}}">New Thread</button>
    </table>
    <table class="table table-condensed table-sm mt-20">
        <thead>
        <tr>
            <th>Subject</th>
            <th class="col-md-2 text-right">Pinned</th>
            <th class="col-md-2 text-right">Locked</th>
            <th class="col-md-2 text-right">Replies</th>
            <th class="col-md-1 text-center"></th>
        </tr>
        </thead>

        @if(count($threads))
            @foreach($threads as $thread)
                <tr>
                    <td><a href="#" data-thread-id="{{$thread->id}}"
                           class="forum-threads view-posts">{{$thread->title}}</a>
                    </td>
                    <td>
                        @if($thread->pinned == 1)
                            <h2>
                                <span class="label label-info">Pinned</span>
                            </h2>
                        @endif
                    </td>
                    <td>
                        @if($thread->locked == 1)
                            <h2>
                                <span class="label label-warning">Locked</span>
                            </h2>
                        @endif
                    </td>
                    <td>{{count($thread->posts) > 0 ? count($thread->posts) : 0}}</td>
                    <td>
                        <span class="icon-eye2 btn btn-xs btn-info view-posts" data-thread-id="{{$thread->id}}"></span>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5">No Threads available</td>
            </tr>
        @endif
    </table>
@else

    <table class="table table-condensed table-sm mt-20">
        <thead>
        <tr>
            <th>No Forums available</th>
        </tr>
        </thead>
    </table>

@endif
