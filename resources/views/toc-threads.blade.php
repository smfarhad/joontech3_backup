<div class="panel panel-white">
	<div class="panel-heading">
		<h6 class="panel-title">My Inbox (multiple lines)</h6>
		<div class="heading-elements not-collapsible">
			<!-- <span class="label bg-blue heading-text">259 new today</span> -->
		</div>
	</div>

	<div class="panel-toolbar panel-toolbar-inbox">
		<div class="btn-group navbar-btn">
			<button class="btn btn-default btn-sm new-thread" data-forum-id="{{$forumId}}" type="button" >
				<i class="icon-pencil7"></i> <span class="hidden-xs position-right">New Thread</span>
			</button>
			<button class="btn btn-sm btn-default" type="button">
					<i class="icon-new-tab2"></i> <span class="hidden-xs position-right">Map</span>
			</button>    
		</div>
	</div>

	<div class="table-responsive">
		<table class="table table-inbox">
			<tbody data-link="row" class="rowlink">
				@if(count($threads))
				@foreach($threads as $thread)
				<tr class="unread view-posts"  data-thread-id="{{$thread->id}}">	
					<td class="text-center" style="width:60px;padding:5px">
						<h6 class="no-margin"> 
						{{count($thread->posts) > 0 ? count($thread->posts) : 0}}
						<small class="display-block text-size-small no-margin">
							@if( count($thread->posts) == 1)
								{{ 'Reply' }}
							@endif

							@if( count($thread->posts) > 1)
								{{ 'Replies' }}
							@endif
						</small>
						</h6>
					</td>
					<td class="table-inbox-message" style="width:180px;">
						<div class="table-inbox-subject">

							@if($thread->pinned == 1)
								<span class="label bg-success">Pinned</span>
							@endif

							@if($thread->locked == 1)
								<span class="label bg-indigo-400">Locked</span>  
							@endif

							{{$thread->title}} 
						</div>
						<span class="table-inbox-preview">
						{{$thread->first_name . ' ' . $thread->last_name}} 
							
						</span>
					</td>
					<td class="table-inbox-time">
					{{ date ("F j", strtotime($thread->created_at))}} 
					</td>
				</tr>
				@endforeach
				@else
					<tr>
						<td colspan="3">No Threads available</td>
					</tr>
				@endif
				
			</tbody>
		</table>
	</div>
</div>