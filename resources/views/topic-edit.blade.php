
<script>
    $(function() {
        CKEDITOR.replace( 'topic-edit', {
            extraPlugins: 'mathjax',
            mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML',
            height: 220,
            toolbar :
            [
                { name: 'basicstyles', items : [ 'Bold','Italic' ] },
                { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
                { name: 'insert', items : [ 'Image','Table','Mathjax' ] },
            ]
        });
        
        
        if ( CKEDITOR.env.ie && CKEDITOR.env.version == 8 ) {
            document.getElementById( 'ie8-warning' ).className = 'tip alert';
        }

        $('#topicUpdate').on('submit',function(e){
            e.preventDefault();
            var form = $(this);
            var topic_page = getParameterByName('page');
            console.log(topic_page);
            var topic = CKEDITOR.instances['topic-edit'].getData()
                $.post("{{ route('topicUpdate') }}",{formData:form.serialize(),topic:topic,page:topic_page})
                    .done(function(res){
                        $('#topics').html(res);
                        $('#topic-create-wrapper').remove();
                        MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
                    });
        });

        // Cancel Button
        $('.action-edit-topic-cancel').on('click',function(){
            $('#topic-wrapper').html('');
            $('.topic-content').slideDown();
        });
    });

</script>

<div id="topic-create-wrapper">
    <div class="create-topic-title">
        <div class="media-annotation">Create new topic</div>
    </div>
    <form id="topicUpdate" method="POST" class="category-content">
        <input type="hidden" name="id" value="{{ $topic->id }}">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Topic Title" name="title" value="{{ $topic->title }}">
        </div>

        <div class="form-group">
            <textarea cols="10" id="topic-edit" rows="10" data-sample="1" data-sample-short="">
                {!! $topic->content !!}
            </textarea>
        </div>

        <div class="row mt-20">
            <div class="col-xs-6">
                <button type="button" class="btn btn-danger btn-block action-edit-topic-cancel">Cancel</button>
            </div>
            <div class="col-xs-6">
                <button type="submit" class="btn btn-info btn-block">Submit</button>
            </div>
        </div>
    </form>
</di>