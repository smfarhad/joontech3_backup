<script>
    $(function() {
        // Reply Button
        $('.btn-reply').on('click',function(){
            var button = $(this);
            var id = button.data('id');
            var parent = button.parent('.reply-actions');

            //Cancel open Edit reply box
            $('.btn-reply-cancel').trigger('click');

            parent.find('.reply-submit-action').removeClass('hidden'); // show submit actions
            button.addClass('hidden');

            $("[id^='topic-reply-']").remove(); // remove textarea
            $("div[id^='cke_topic-reply-']").remove(); // remove ck generated editor
            $('#reply-box-'+id).append('<textarea cols="10" id="topic-reply-'+id+'" name="reply" rows="10" data-sample="1" data-sample-short=""></textarea>');
            ckInit('topic-reply-'+id);
        });

        $('.btn-cancel').on('click',function(){
            var button = $(this);
            var parent = button.closest('.reply-actions');
            
            parent.find('.btn-reply').removeClass('hidden');
            parent.find('.reply-submit-action').addClass('hidden');

            $("[id^='topic-reply-']").remove(); // remove textarea
            $("div[id^='cke_topic-reply-']").remove(); // remove ck generated editor
        });

        $('.btn-submit').on('click',function(){
            var button = $(this);
            var id = button.data('id');
            var slug = button.data('slug');
            var reply = CKEDITOR.instances['topic-reply-'+id].getData();
            var subId = button.closest('.reply-actions').data('subid');
            var box = button.closest('.topic-reply-box');
            

            $.post("{{ route('addReply') }}",{ id:id,data:reply,sub_id:subId,page:$page })
                .done(function(res){
                    box.html(res);
                    $('#topic-create-wrapper').remove();
                    MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
                });
        });

        $('.action-edit-reply').on('click',function(e){
            e.preventDefault();

            // Cancel Open reply box
            $('.reply-submit-action .btn-cancel').trigger('click');

            id = $(this).attr('href');
            container = $(this).closest('.media-body');
            $.post("{{ route('replyEdit') }}",{id:id})
                .done(function(res){
                    container.find('.reply-box').addClass('hidden');
                    container.find('.reply-edit-box').html('');
                    container.find('.reply-edit-box').append(res);
                });
        });

        /* Pagination */
        $('#reply-pagination .pagination a').on('click',function (e) {
            var pagination = $(this);            
            page = pagination.attr('href').split('page=')[1];
            slug = pagination.closest('#reply-pagination').data('slug');
            getReply(page,slug);

            e.preventDefault();
            
        });

        $( ".toggle-permalink-reply" ).click(function(e) {
            var toggle = $(this).attr('href');
            $( toggle+'-toggle' ).slideToggle( "slow", function() {
                // Animation complete.
            });
            e.preventDefault();
        });

        $('#reply-pagination .pagination').addClass('pagination-flat pagination-xs xxs');

        function getReply(page,slug) {
            $.ajax({
                url : "{{ url('forum/show-replies/') }}?slug={{ $topic->slug }}&page=" + page,
                dataType: 'json',
                type: 'POST',
            }).done(function (res) {
                $('#'+slug).html(res);
                MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            }).fail(function () {
                alert('Posts could not be loaded.');
            });
        }
        /* End Pagination */

        function ckInit(element){
            CKEDITOR.replace( element, {
                extraPlugins: 'mathjax',
                mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML',
                height: 220,
                toolbar :
                [
                    { name: 'basicstyles', items : [ 'Bold','Italic' ] },
                    { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
                    { name: 'insert', items : [ 'Image','Table','Mathjax' ] },
                ]
            });
            
            
            if ( CKEDITOR.env.ie && CKEDITOR.env.version == 8 ) {
                document.getElementById( 'ie8-warning' ).className = 'tip alert';
            }
        }

        function openTopic(){
            var url = window.location.hash;
            var hash = url.substring(url.indexOf('#'));
            if( hash ){
                $(hash).collapse("show");
            }
        }
    });
</script>

<div class="panel-body">
    <p>{!! $topic->content !!}</p>

    <div class="panel-reply-body">
        <div class="media-annotation mt-10 mb-10">Replies: </div>
        <ul class="media-list">
            @foreach ( $replies as $reply )
            <li class="media">
                <a href="#" class="media-left"><img src="{{ asset('assets/images/face1.jpg') }}" class="img-sm img-circle" alt=""></a>
                <div class="media-body">
                    @if( Auth::user()->canEdit($reply->created_by) )
                    <div>
                        <a href="{{ $reply->id }}" class="action-edit-reply label label-flat border-teal-400 text-teal-600  pull-right">Edit</a>
                    </div>
                    @endif
                    <a href="#" class="media-heading text-semibold">{{ App\User::author($reply->createdBy) }}</a>
                    <span class="text-size-mini text-muted display-block">{{ dateNameFormat($reply->created_at) }} ago</span>
                    <div class="reply-box mt-10">{!! $reply->reply !!}</div>
                    <div class="reply-edit-box"></div>
                    <div class="mt-20">
                        <div class="permalink">
                            <a href="#{{ $reply->slug }}" class="text-info toggle-permalink-reply">Permalink <i class="icon-link2 icon-xs"></i></a>
                            <div id="{{ $reply->slug }}-toggle" class="permalink" style="display:none">
                                <?php
                                    $topic_page = ($request->topicPage > 1) ? '&page='.$request->topicPage : '';
                                    $reply_page = ($replies->currentPage() > 1 ) ? '&reply_page='.$replies->currentPage() : '';
                                ?>
                                <p class="label label-info">{{ url('/book/'.$subSection->book->slug.'?subSection='.$subSection->slug.$topic_page.$reply_page.'#'.$topic->slug) }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
        
        <div id="reply-box-{{ $topic->id }}">
            
        </div>
    </div>
    <div id="reply-pagination" data-slug="{{ $topic->slug }}" class="pagination-replies-wrap col-xs-12 text-right mt-20">
        {{ $replies->render() }}
    </div>
</div>
<div class="panel-footer"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
    <div class="heading-elements reply-actions" data-subid="{{ $subSection->id }}">
        <button 
            type="button" 
            class="btn btn-primary btn-xs heading-btn pull-right legitRipple btn-reply"
            data-id="{{ $topic->id }}"
            >Reply</button>

        <div class="reply-submit-action hidden">
            <button 
            type="button" 
            class="btn btn-success btn-xs heading-btn pull-right legitRipple btn-submit"
            data-id="{{ $topic->id }}"
            data-slug="{{ $topic->slug }}"
            >Submit</button>

            <button 
            type="button" 
            class="btn btn-danger btn-xs heading-btn pull-right legitRipple btn-cancel"
            data-id="{{ $topic->id }}"
            >Cancel</button>
        </div>
    </div>
</div>

