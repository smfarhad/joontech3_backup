<script type="text/javascript" src="{{ asset('assets/js/plugins/editors/ckeditor/ckeditor.js') }}"></script>

<script>
    $(function() {
        $page = '{{ $request->page }}';
        //openTopic();

        $( ".toggle-permalink" ).click(function(e) {
            var toggle = $(this).attr('href');
            $( toggle+'-toggle' ).slideToggle( "slow", function() {
                // Animation complete.
            });
            e.preventDefault();
        });


        $(document).on('click', '.action-createTopic', function(){
            console.info('acton create topic clicked');
            $('#topic-wrapper').html('');
            $('#topic-wrapper').append(`
                <div id="topic-create-wrapper">
                    <div class="create-topic-title">
                        <div class="media-annotation">Create new topic</div>
                    </div>
                    <form id="newTopic" method="POST" class="category-content">
                        <input type="hidden" name="id" value="{{ $subSection->id }}">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Topic Title" name="title" maxlength="70">
                        </div>

                        <div class="form-group">
                            <textarea cols="10" id="new-topic-{{ $subSection->id }}" rows="10" data-sample="1" data-sample-short=""></textarea>
                        </div>

                        <div class="row mt-20">
                            <div class="col-xs-6">
                                <button type="button" class="btn btn-danger btn-block action-topic-cancel">Cancel</button>
                            </div>
                            <div class="col-xs-6">
                                <button type="submit" class="btn btn-info btn-block">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            `);


            ckInit('new-topic-{{ $subSection->id }}');
            $('.topic-content').slideUp();

            // Submit Button
            $('#newTopic').on('submit',function(e){
                e.preventDefault();
                var form = $(this);
                var id = form.find('input[name=id]').val();
                var topic = CKEDITOR.instances['new-topic-'+id].getData()
                $.post("{{ route('addTopic') }}",{formData:form.serialize(),topic:topic})
                    .done(function(res){
                        $('#topics').html(res);
                        $('#topic-create-wrapper').remove();
                        MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
                    })
                    .error(function(res){
                        var errors = res.responseJSON;
                        ajaxValidationMessage(form,errors);
                    });
            });

            // Cancel Button
            $('.action-topic-cancel').on('click',function(){
                $('#topic-wrapper').html('');
                $('.topic-content').slideDown();
            });
        });

        $('.topic-close').on('click',function(){
            $('#main-body').removeClass('sidebar-opposite-visible');
        });

        $('.action-edit').on('click',function(e){
            e.preventDefault();
            slug = $(this).attr('href');
            $.post("{{ route('topicEdit') }}",{slug:slug})
                .done(function(res){
                    $('.topic-content').slideUp();

                    $('#topic-wrapper').html('');
                    $('#topic-wrapper').append(res);
                });
        });

        $("#accordion1").on("show.bs.collapse", function(data,test){
            var slug = $(data.target).attr('id');
            var reply_page = $(data.target).data('replypage');
            var topic_page = $(data.target).data('topicpage');
            openReplies(slug,reply_page,topic_page);
        });

        function openReplies(slug,page,topic_page){
            topic_page = topic_page ? '&topicPage='+topic_page : '';
            $.post("{{ url('forum/show-replies/') }}?slug="+ slug +"&page=" + page + topic_page)
                .done(function(res){
                    $('#'+slug).html(res);
                    window.location.hash = slug;
                    MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
                });
        }

        /* Pagination */
        $('#topic-pagination .pagination a').on('click',function (e) {
            getPosts($(this).attr('href').split('page=')[1]);
            e.preventDefault();
        });

        function getPosts(page) {
            $.ajax({
                url : "{{ url('forum/show-topics/') }}?slug={{ $subSection->slug }}&page=" + page,
                dataType: 'json',
                type: 'POST',
            }).done(function (res) {
                $('#topics').html(res);
                $('#topic-create-wrapper').remove();
                MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
                history.pushState(null, null, location.pathname + '?subSection={{ $subSection->slug }}&page=' + page);
            }).fail(function () {
                alert('Posts could not be loaded.');
            });
        }
        /* End Pagination */

        function ckInit(element){
            CKEDITOR.replace( element, {
                extraPlugins: 'mathjax',
                mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML',
                height: 220,
                toolbar :
                [
                    { name: 'basicstyles', items : [ 'Bold','Italic' ] },
                    { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
                    { name: 'insert', items : [ 'Image','Table','Mathjax' ] },
                ]
            });

            if ( CKEDITOR.env.ie && CKEDITOR.env.version == 8 ) {
                document.getElementById( 'ie8-warning' ).className = 'tip alert';
            }
        }

        function openTopic(){
            var url = window.location.hash;
            var hash = url.substring(url.indexOf('#'));
            var reply_page = getParameterByName('reply_page');
            if( hash ){
                openReplies(hash.replace('#',''),reply_page);
                $(hash).collapse("show");
            }
        }
    });

</script>

<!-- Sub navigation -->
<div id="topic-appended">
    <div class="sidebar-category">
        <ul class="navigation shadow navigation-alt navigation-accordion">
            <li class="navigation-header">{{ $subSection->title }} <i class="icon-x topic-close"></i></li>
            <li>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button
                            type="button"
                            class="btn btn-success btn-long action-createTopic"
                            data-id="{{ $subSection->id }}"
                            >Create New</button>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <!-- /sub navigation -->

    <div class="sidebar-category">
        <div class="category-title no-border">
            <span>Topics</span>
        </div>
        <div class="topics">
            <div class="category-content topic-content no-padding">
                <div class="panel-group" id="accordion1">
                    @foreach ( $topics as $topic )
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion1" href="#{{ $topic->slug }}" class="panel-title text-semibold">
                                <div class="title-wrapper">
                                    <div class="title text-underline"> {{ $topic->title }}
                                       {{--   @if( Auth::user()->canEdit($topic->created_by) )
                                            <a href="{{ $topic->slug }}" class="action-edit label label-flat border-success text-success-600  pull-right">Edit</a>
                                        @endif  --}}
                                    </div>
                                    <div class="media-heading text-semibold">{{ App\User::author($topic->createdBy) }}</div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="media-annotation">{{ dateNameFormat($topic->created_at) }} ago</div>
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <div class="media-annotation">{{ replies($topic->replies->count()) }}</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php
                            $p_replies = $topic->replies()->count();
                            $lastPage = intval(ceil($p_replies/3));
                        ?>
                        <div data-topicpage="{{ $topics->currentPage() }}" data-replypage="{{ $lastPage }}" id="{{ $topic->slug }}" class="topic-reply-box panel-collapse collapse">

                        </div>
                    </div>
                    @endforeach
                </div>
                <div id="topic-pagination" class="pagination-wrap col-xs-12 text-center mt-20">
                    {{ $topics->render() }}
                </div>
            </div>
        </div>
    </div>
</div>

