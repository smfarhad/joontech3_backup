{{-- $category is passed as NULL to the master layout view to prevent it from showing in the breadcrumbs --}}
@extends ('forum::master', ['category' => null])

@section ('content')
    {{--@can ('createCategories')
       @include ('forum::category.partials.form-create')
    @endcan--}}

    <h2>{{ trans('forum::general.index') }}</h2>
    <div class="row">
        <div class="col-lg-12">
            <!-- Support tickets -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Discussions<a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </h6>
                    <div class="heading-elements">

                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-index text-nowrap">
                        <thead>
                        <tr>
                            <th>{{ trans_choice('forum::categories.category', 1) }}</th>
                            <th class="col-md-2">{{ trans_choice('forum::threads.thread', 2) }}</th>
                            <th class="col-md-2">{{ trans_choice('forum::posts.post', 2) }}</th>
                            <th class="col-md-2">{{ trans('forum::threads.newest') }}</th>
                            <th class="col-md-2">{{ trans('forum::posts.last') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($categories as $category)
                            <tr class="category">
                                @include ('forum::category.partials.list', ['titleClass' => 'lead'])
                            </tr>
                            {{--@if (!$category->children->isEmpty())
                                <tr>
                                    <th colspan="5">{{ trans('forum::categories.subcategories') }}</th>
                                </tr>
                                @foreach ($category->children as $subcategory)
                                    @include ('forum::category.partials.list', ['category' => $subcategory])
                                @endforeach
                            @endif--}}

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /support tickets -->
        </div>
    </div>
@stop
