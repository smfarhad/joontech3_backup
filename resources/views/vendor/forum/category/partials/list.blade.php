<tr>
    @if ($category->is_deleted == 0 && $category->is_public ==1)
    <td {{ $category->threadsEnabled ? '' : 'colspan=5' }}>
        <div class="media-body">
            <!-- <a class="display-inline-block text-default text-semibold letter-icon-title" href="{{ Forum::route('category.show', $category) }}" >{{ $category->title }}</a> -->
            <a class="display-inline-block text-default text-semibold letter-icon-title" href="{{ route('discussionChildren', [$category->slug,$category->id]) }}" >{{ $category->title }}</a>
        </div>
        {{--<span class="text-muted">{{ $category->description }}</span>--}}
    </td>
    @endif
    @if ($category->threadsEnabled)
        <td>{{ $category->thread_count }}</td>
        <td>{{ $category->post_count }}</td>
        <td>
            @if ($category->newestThread)
                <a href="{{ Forum::route('thread.show', $category->newestThread) }}">
                    {{ $category->newestThread->title }}
                    ({{ $category->newestThread->authorName }})
                </a>
            @endif
        </td>
        <td>
            @if ($category->latestActiveThread)
                <a href="{{ Forum::route('thread.show', $category->latestActiveThread->lastPost) }}">
                    {{ $category->latestActiveThread->title }}
                    ({{ $category->latestActiveThread->lastPost->authorName }})
                </a>
            @endif
        </td>
    @endif
</tr>
