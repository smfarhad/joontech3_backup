@extends ('forum::master', ['breadcrumb_other' => trans('forum::general.new_reply')])

@section ('content')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/editor_ckeditor.js') }}"></script>
    <!-- /theme JS files -->

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">{{ trans('forum::general.new_reply') }} ({{ $thread->title }})</h5>
        </div>

        <div class="panel-body">
            <div id="create-post">
                @if (!is_null($post) && !$post->trashed())
                    <h3>{{ trans('forum::general.replying_to', ['item' => $post->authorName]) }}...</h3>

                    @include ('forum::post.partials.excerpt')
                @endif

                <form method="POST" action="{{ Forum::route('post.store', $thread) }}">
                    {!! csrf_field() !!}
                    @if (!is_null($post))
                        <input type="hidden" name="post" value="{{ $post->id }}">
                    @endif

                    <div class="form-group">
                        <label class="control-label col-lg-2">Reply</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <textarea name="content" class="form-control ckeditor">{{ old('content') }}</textarea>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success pull-right">{{ trans('forum::general.reply') }}</button>
                    <a href="{{ URL::previous() }}" class="btn btn-default">{{ trans('forum::general.cancel') }}</a>
                </form>
            </div>
        </div>
    </div>

@stop
