@extends ('forum::master', ['breadcrumb_other' => trans('forum::threads.new_thread')])

@section ('content')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/editor_ckeditor.js') }}"></script>
    <!-- /theme JS files -->

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">{{ trans('forum::threads.new_thread') }} ({{ $category->title }})<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="POST" action="{{ Forum::route('thread.store', $category) }}">
                {!! csrf_field() !!}
                {!! method_field('post') !!}

                <fieldset class="content-group">
                    <div class="form-group">
                        <label class="control-label col-lg-2">{{ trans('forum::general.title') }}</label>
                        <div class="col-lg-10">
                                <input type="text" name="title" value="{{ old('title') }}" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Description</label>
                        <div class="col-lg-10">
                                <textarea name="content" class="form-control ckeditor">{{ old('content') }}</textarea>
                        </div>
                    </div>
                </fieldset>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary pull-right">{{ trans('forum::general.create') }}</button>

                    <a href="{{ URL::previous() }}" class="btn btn-default">{{ trans('forum::general.cancel') }}</a>
                </div>
            </form>
        </div>
    </div>

    {{--<div id="create-thread">
        <h2>{{ trans('forum::threads.new_thread') }} ({{ $category->title }})</h2>

        <form method="POST" action="{{ Forum::route('thread.store', $category) }}">
            {!! csrf_field() !!}
            {!! method_field('post') !!}

            <div class="form-group">
                <label for="title">{{ trans('forum::general.title') }}</label>
                <input type="text" name="title" value="{{ old('title') }}" class="form-control">
            </div>

            <div class="form-group">
                <textarea name="content" class="form-control">{{ old('content') }}</textarea>
            </div>

            <button type="submit" class="btn btn-success pull-right">{{ trans('forum::general.create') }}</button>
            <a href="{{ URL::previous() }}" class="btn btn-default">{{ trans('forum::general.cancel') }}</a>
        </form>
    </div>--}}
@stop