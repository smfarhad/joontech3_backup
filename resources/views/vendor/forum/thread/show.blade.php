@extends ('forum::master')

@section ('content')
    <style>
        #post-content a {
            display: block;
        }

        #post-content span a {
            display: inline;;
        }
    </style>

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/ckeditor/ckeditor.js') }}"></script>
    <!-- /theme JS files -->

    <div id="thread">
        @can ('manageThreads', $category)
            <form action="{{ Forum::route('thread.update', $thread) }}" method="POST" id="thread-actions-form"
                  data-actions-form class="hidden">
                {!! csrf_field() !!}
                {!! method_field('patch') !!}

                @include ('forum::thread.partials.actions')
            </form>
        @endcan

        {{--@can ('deletePosts', $thread)
            <form action="{{ Forum::route('bulk.post.update') }}" method="POST" data-actions-form>
                {!! csrf_field() !!}
                {!! method_field('delete') !!}
                @endcan--}}

        <div class="row">
            <div class="col-xs-4">
                @can ('reply', $thread)
                    <div class="btn-group" role="group">
                        {{--<a href="{{ Forum::route('post.create', $thread) }}"
                           class="btn btn-primary">{{ trans('forum::general.new_reply') }}</a>--}}
                        {{--<a href="#quick-reply"
                           class="btn btn-primary">{{ trans('forum::general.quick_reply') }}</a>--}}
                    </div>
                @endcan
            </div>
            <div class="col-xs-8 text-right">
                {!! $posts->render() !!}
            </div>
        </div>

        <div class="content-wrapper">
            <!-- Post -->
            <div class="panel">
                <div class="panel-body">

                    <div class="panel-toolbar panel-toolbar-inbox">
                        <div class="navbar navbar-default navbar-component no-margin-bottom">
                            <ul class="nav navbar-nav visible-xs-block no-border">
                                <li>
                                    <a class="text-center collapsed" data-toggle="collapse"
                                       data-target="#inbox-toolbar-toggle-single">
                                        <i class="icon-circle-down2"></i>
                                    </a>
                                </li>
                            </ul>

                            <div class="navbar-collapse collapse" id="inbox-toolbar-toggle-single">
                                <div class="btn-group navbar-btn">

                                    @if(empty($thread->deleted_at))
                                        <a href="{{ Forum::route('post.create', $thread) }}"
                                           class="btn btn-default"><i class="icon-reply"></i> <span
                                                    class="hidden-xs position-right">{{ trans('forum::general.new_reply') }}</span></a>

                                        <a href="#quick-reply" class="btn btn-default"><i
                                                    class="icon-reply-all"></i> <span
                                                    class="hidden-xs position-right">
                                                    {{ trans('forum::general.quick_reply') }}
                                                </span></a>

                                        @can ('pinThreads', $category)
                                            @if ($thread->pinned)
                                                {{--<option value="unpin"></option>--}}
                                                <a href="#" class="btn btn-default btn-action" data-action="unpin"><i
                                                            class="icon-pushpin"></i>
                                                    <span class="hidden-xs position-right">{{ trans('forum::threads.unpin') }}</span></a>
                                            @else
                                                {{--<option value="pin">{{ trans('forum::threads.pin') }}</option>--}}
                                                <a href="#" class="btn btn-default btn-action" data-action="pin"><i
                                                            class="icon-pushpin"></i>
                                                    <span class="hidden-xs position-right">{{ trans('forum::threads.pin') }}</span></a>
                                            @endif
                                        @endcan


                                        @if ($thread->locked)
                                            <a href="#" class="btn btn-success btn-action" data-action="unlock"><i
                                                        class="icon-unlocked"></i>
                                                <span class="hidden-xs position-right">Unlock</span></a>
                                        @else
                                            <a href="#" class="btn btn-default btn-action" data-action="lock"><i
                                                        class="icon-lock"></i>
                                                <span class="hidden-xs position-right">Lock</span></a>
                                        @endif
                                    @endif


                                    @can('delete', $category)
                                        @if(empty($thread->deleted_at))
                                            <a href="#" class="btn btn-default btn-action" data-action="delete"><i
                                                        class="icon-bin"></i> <span class="hidden-xs position-right">Delete</span></a>

                                        @else
                                            <a href="#" class="btn btn-default btn-action" data-action="restore"><i
                                                        class="icon-bin"></i> <span class="hidden-xs position-right">Restore</span></a>

                                            <a href="#" style="color: red;" class="btn btn-default btn-action"
                                               data-action="permadelete"><i class="icon-bin"></i> <span
                                                        class="hidden-xs position-right">Delete Permanently</span></a>
                                        @endif
                                    @endcan
                                    {{--<div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle"
                                                data-toggle="dropdown">
                                            <i class="icon-menu7"></i>
                                            <span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li><a href="#">One more line</a></li>
                                        </ul>
                                    </div>--}}
                                </div>


                                <div class="pull-right-lg">
                                    <h2>
                                        @if ($thread->trashed())
                                            <span class="label label-danger">{{ trans('forum::general.deleted') }}</span>
                                        @endif
                                        @if ($thread->locked)
                                            <span class="label label-warning">{{ trans('forum::threads.locked') }}</span>
                                        @endif
                                        @if ($thread->pinned)
                                            <span class="label label-info">{{ trans('forum::threads.pinned') }}</span>
                                        @endif
                                    </h2>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="content-group-lg">
                        <h3 class="text-semibold mb-5">
                            <a href="#" class="text-default">{{ $thread->title }}</a>
                        </h3>
                        <ul class="list-inline list-inline-separate text-muted content-group">
                            <li>By <a href="#" class="text-muted">Eugene</a></li>
                            <li>
                                {{ Carbon\Carbon::parse($thread->created_at)->format('M d, Y') }}
                            </li>
                            <li><a href="#" class="text-muted">{{ count($posts) -1 }} comments</a></li>
                        </ul>

                        <div class="content-group">
                            <p id="post-content">
                                @foreach ($posts as $post)
                                    @if($loop->first)
                                        @include ('forum::post.partials.list', compact('post'))
                                    @endif
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /post -->

            <!-- About author -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">About the author<a class="heading-elements-toggle"><i
                                    class="icon-more"></i></a><a class="heading-elements-toggle"><i
                                    class="icon-more"></i></a></h6>

                </div>

                <div class="media panel-body no-margin">
                    <div class="media-left">
                        <a href="#">
                            <img src="{{ asset('assets/images/user-profile.jpg') }}" style="width: 68px; height: 68px;"
                                 class="img-circle" alt="">
                        </a>
                    </div>

                    <div class="media-body">
                        <h6 class="media-heading text-semibold">{{ $postAuthor }}</h6>
                    </div>
                </div>
            </div>
            <!-- /about author -->

            <!-- Comments -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title text-semiold">Discussion<a class="heading-elements-toggle"><i
                                    class="icon-more"></i></a></h6>
                    <div class="heading-elements">
                        <ul class="list-inline list-inline-separate heading-text text-muted">
                            <li>{{ count($posts) - 1  }} comments</li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <ul class="media-list stack-media-on-mobile">
                        @if($postComments)
                            @foreach($postComments as $post)
                                <li class="media">
                                    <div class="media-left">
                                        <a href="#"><img src="{{asset('assets/images/user-profile.jpg')}}"
                                                         class="img-circle img-sm" alt=""></a>
                                    </div>

                                    <div class="media-body">
                                        <div class="media-heading">
                                            <a href="#" class="text-semibold">{{$post['author']}}</a>
                                            <span class="media-annotation dotted">{{ trans('forum::general.posted') }} {{ $post['post']->posted }}</span>
                                        </div>

                                        <p>{!! $post['post']->content !!}</p>

                                        <ul class="list-inline list-inline-separate text-size-small">
                                            {{--<li>32 <a href="#"><i
                                                            class="icon-arrow-up22 text-success"></i></a><a
                                                        href="#"><i
                                                            class="icon-arrow-down22 text-danger"></i></a></li>--}}
                                            <li>
                                                @if (!$post['post']->trashed())
                                                    @can ('reply', $post['post']->thread)
                                                        -
                                                        <a href="{{ Forum::route('post.create', $post['post']) }}">{{ trans('forum::general.reply') }}</a>
                                                    @endcan
                                                @endif
                                            </li>
                                            <li>
                                                @if (!$post['post']->trashed())
                                                    @can ('edit', $post['post'])
                                                        <a href="{{ Forum::route('post.edit', $post['post']) }}">{{ trans('forum::general.edit') }}</a>
                                                    @endcan
                                                @endif
                                            </li>
                                        </ul>

                                        {{--@if($commentReplies)
                                            @foreach($commentReplies as $reply)
                                                @if($reply['comment_id'] == $post['post']->id)

                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="#"><img src="assets/images/placeholder.jpg"
                                                                             class="img-circle img-sm"
                                                                             alt=""></a>
                                                        </div>

                                                        <div class="media-body">
                                                            <div class="media-heading">
                                                                <a href="#"
                                                                   class="text-semibold">{{$reply['reply']->author->first_name . ' ' . $reply['reply']->author->first_name }}</a>
                                                                <span class="media-annotation dotted">{{ trans('forum::general.posted') }} {{ $reply['reply']->posted }}</span>
                                                            </div>

                                                            <p>{!! $reply['reply']->content !!}</p>

                                                            <ul class="list-inline list-inline-separate text-size-small">
                                                                --}}{{--<li>67 <a href="#"><i
                                                                                class="icon-arrow-up22 text-success"></i></a><a
                                                                            href="#"><i
                                                                                class="icon-arrow-down22 text-danger"></i></a>
                                                                </li>--}}{{--
                                                                <li>

                                                                    @if (!$post['post']->trashed())
                                                                        @can ('reply', $post['post']->thread)
                                                                            -
                                                                            <a href="{{ Forum::route('post.create', $post['post']) }}">{{ trans('forum::general.reply') }}</a>
                                                                        @endcan
                                                                    @endif

                                                                </li>
                                                                <li>
                                                                    @if (!$post['post']->trashed())
                                                                        @can ('edit', $post['post'])
                                                                            <a href="{{ Forum::route('post.edit', $reply['reply']) }}">{{ trans('forum::general.edit') }}</a>
                                                                        @endcan
                                                                    @endif
                                                                </li>
                                                            </ul>

                                                        </div>
                                                    </div>

                                                @endif
                                            @endforeach
                                        @endif--}}
                                    </div>
                                </li>

                            @endforeach

                        @else
                            No Discussions. Be the first to discuss this Post.
                        @endif
                    </ul>
                </div>

                <hr class="no-margin">
                <div class="panel-body">

                    @can ('reply', $thread)
                        <h4 class="no-margin-top content-group">{{ trans('forum::general.quick_reply') }}</h4>
                        <div id="quick-reply">
                            <form method="POST" action="{{ Forum::route('post.store', $thread) }}">
                                {!! csrf_field() !!}

                                <div class="form-group">
                                            <textarea name="content"
                                                      class="form-control ckeditor">{{ old('content') }}</textarea>
                                </div>

                                <div class="text-right">
                                    <button type="submit"
                                            class="btn btn-success pull-right">{{ trans('forum::general.reply') }}</button>
                                </div>
                            </form>
                        </div>
                    @endcan
                </div>
            </div>
            <!-- /comments -->

        </div>

        {{--<table class="table {{ $thread->trashed() ? 'deleted' : '' }}">
            <thead>
            <tr>
                <th class="col-md-2">
                    {{ trans('forum::general.author') }}
                </th>
                <th>
                    {{ trans_choice('forum::posts.post', 1) }}
                    @can ('deletePosts', $thread)
                        <span class="pull-right">
                        <input type="checkbox" data-toggle-all>
                    </span>
                    @endcan
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach ($posts as $post)
                @if(!$loop->first)
                    @include ('forum::post.partials.list', compact('post'))
                @endif

            @endforeach
            </tbody>
        </table>--}}

        @can ('deletePosts', $thread)
            @include ('forum::thread.partials.post-actions')
        @endcan
        {{--</form>--}}

        {!! $posts->render() !!}


        <input type="hidden" name="threadId" value="{{$thread->id}}" id="threadId"/>
    </div>
@stop

@section ('action-footer')
    <script>
        $('.btn-action').on('click', function (e) {
            e.preventDefault();
            let action = $(this).data('action');
            $('#action').val(action);

            if (action == 'delete' || action == 'permadelete') {
                $('input[name="_method"]').val('delete');
            } else {
                $('input[name="_method"]').val('patch');
            }

            $('.action-process').trigger('click');
        });

        $('tr input[type=checkbox]').change(function () {
            var postRow = $(this).closest('tr').prev('tr');
            $(this).is(':checked') ? postRow.addClass('active') : postRow.removeClass('active');
        });
    </script>
@stop