<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
Auth::routes();

Route::get('/pageNotFound', 'ErrorController@page_404')->name('pageNotFound');
Route::get('/forbiddenError', 'ErrorController@page_403')->name('forbiddenError');

Route::get('pricing', 'PricingController@pricing')->name('pricing');
Route::get('verify/{token}', 'VerifyEmailController@verify')->name('verify');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/book/{id}', 'BookController@index')->name('bookPage');
    //Route::get('/book/{id}/subsection/{subsection}', 'BookController@index')->name('bookContent');


    Route::post('/book/addTopic', 'BookController@addTopic')->name('addTopic');
    Route::post('/book/addReply', 'BookController@addReply')->name('addReply');
    Route::post('/forum/show-topics', 'ForumController@showTopics')->name('showTopics');
    Route::post('/forum/show-replies', 'ForumController@showReplies')->name('showReplies');
    Route::post('/forum/topic-edit', 'ForumController@topicEdit')->name('topicEdit');
    Route::post('/forum/topic-update', 'ForumController@topicUpdate')->name('topicUpdate');
    Route::post('/forum/reply-edit', 'ForumController@replyEdit')->name('replyEdit');
    Route::post('/forum/reply-update', 'ForumController@replyUpdate')->name('replyUpdate');

    Route::get('/search', 'SearchController@index')->name('search');
    Route::post('/do-search', 'SearchController@doSearch')->name('doSearch');
    Route::post('/advance-search', 'SearchController@advanceSearch')->name('advanceSearch');
    Route::post('/forum/reply-edit', 'ForumController@replyEdit')->name('replyEdit');
});

Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::get('/admin/registrationKey', 'RegistrationTokenController@index')->name('registrationKey');
    Route::post('/admin/registrationTokensDataTable', 'RegistrationTokenController@registrationTokensDataTable')->name('registrationTokensDataTable');
    Route::get('/admin/userList', 'AdminUserController@index')->name('userList');
    Route::post('/admin/activeUsersDataTable', 'AdminUserController@activeUsersDataTable')->name('activeUsersDataTable');
    Route::post('/admin/showUserInfoFields', 'AdminUserController@showUserInfoFields')->name('showUserInfoFields');
    Route::post('/admin/user-update', 'AdminUserController@update')->name('userUpdate');
    Route::post('/admin/user-delete', 'AdminUserController@delete')->name('userDelete');

    Route::post('/admin/user-enable', 'AdminUserController@enableUser')->name('enableUser');
    //Route::get('/admin/book/subsection/{id}', 'AdminSubsectionController@index')->name('subsection');

    Route::get('/admin/book/subsection-content/{id}', 'AdminBookManagementController@subsectionContent')->name('subsectionContent');

    /**
     * TOC Forum routes
     */
    Route::post('/admin/forum/get-forums', 'ForumController@getForums')->name('getForums');
    Route::post('/admin/forum/get-threads', 'ForumController@getThreads')->name('getThreads');
    Route::post('/admin/forum/get-posts', 'ForumController@getPosts')->name('getPosts');
    Route::post('/admin/forum/post-reply', 'ForumController@postReply')->name('postReply');
    Route::post('/admin/forum/delete-post', 'ForumController@deletePost')->name('deletePost');
    Route::post('/admin/forum/pin-thread', 'ForumController@pinThread')->name('pinThread');
    Route::post('/admin/forum/create-thread', 'ForumController@createThread')->name('createThread');
    Route::post('/admin/forum/store-thread', 'ForumController@storethread')->name('storethread');




    Route::get('/admin', 'AdminBookManagementController@bookList')->name('bookList');
    Route::get('/admin/book-view/{id}', 'AdminBookManagementController@bookView')->name('bookView');
    Route::get('/admin/book-add', 'AdminBookManagementController@bookAdd')->name('bookAdd');
    Route::post('/admin/book-create', 'AdminBookManagementController@create')->name('bookCreate');
    Route::get('/admin/books-list', 'AdminBookManagementController@books')->name('listBooks');
    Route::post('/admin/books-list-datatables', 'AdminBookManagementController@booksListDataTables')->name('booksListDataTables');

    Route::get('/admin/book-edit/{id}', 'AdminBookManagementController@bookEdit')->name('bookEdit');
    Route::post('/admin/book-update', 'AdminBookManagementController@updateBook')->name('bookUpdate');
    // Route::post('/admin/book-delete', 'AdminBookManagementController@deleteBook')->name('bookDelete');
    Route::post('/admin/book-delete-ajax', 'AdminBookManagementController@deleteAjax')->name('deleteBook');

    Route::post('/admin/book/subsectionAction', 'AdminSubsectionController@action')->name('subsectionAction');
    Route::post('/admin/book/save-subsection-content', 'AdminBookManagementController@saveSubSectionContent')->name('saveSubSectionContent');
    Route::post('/admin/book/deleteBook', 'AdminBookManagementController@deleteBook')->name('deleteBook');
    Route::post('/admin/book/deleteNode', 'AdminBookManagementController@deleteNode')->name('deleteNode');

    //resource routes
    Route::resource('/admin/allowed-emails', 'AllowedEmailsController');
    Route::get('/admin/allowed-emails', 'AllowedEmailsController@index')->name('listAllowedEmails');
    Route::post('/admin/allowed-emails-listing', 'AllowedEmailsController@allowedEmailsDataTable')->name('allowedEmailsDataTable');
    Route::post('/admin/delete-allowed-email/', 'AllowedEmailsController@delete')->name('deleteAllowedEmail');
    Route::post('/admin/allowed-emails/store', 'AllowedEmailsController@store')->name('storeEmail');

    //forum routes
    Route::get('/admin/forum-panel', 'ForumAdminController@manageForum')->name('forum-panel');
    Route::post('/admin/forum-panel-save', 'ForumAdminController@saveForumFromPanel')->name('forumPanelSave');
    Route::post('/admin/forum-panel-trash-update', 'ForumAdminController@updateTrashStringAjax')->name('forumPanelUpdateTrash');
    Route::post('/admin/forum-panel-delete', 'ForumAdminController@hardDeleteElement')->name('forumPanelDeletePermanently');
    Route::post('/admin/forum-panel-remove-node', 'ForumAdminController@moveNodeToRecycle')->name('forumPanelRemoveNode');
    Route::post('/admin/forum-panel-get-properties', 'ForumAdminController@getProperties')->name('getCategoryProperties');
    Route::post('/admin/forum-panel-restore-node', 'ForumAdminController@undeleteCategory')->name('forumPanelRestoreNode');
    Route::post('/admin/forum-panel-update-permission', 'ForumAdminController@updatePermission')->name('forumPanelUpdatePermission');
    Route::post('/admin/forum-panel-update-thread-permission', 'ForumAdminController@updateThreadPermission')->name('forumPanelUpdateThreadPermission');

    /**
     * discussion views
     */
    //Route::get('admin/discussions/books/{id}/{subcatid}', 'DiscussionController@books')->name('booksCategories');
    Route::get('admin/discussions/books/', 'DiscussionController@books')->name('booksCategories');
    Route::get('admin/discussions/categories', 'DiscussionController@categories')->name('discCategories');
    Route::get('admin/discussions/categories/{id}', 'DiscussionController@categories')->name('discCategories');
    Route::get('/admin/discussions', 'DiscussionController@index')->name('discussions');

    Route::get('/discussions', 'DiscussionViewController@index')->name('discussionIndex');
    Route::get('/discussions/{slug}/{id}', 'DiscussionViewController@show')->name('discussionChildren');

    Route::post('/admin/discussions/delete-forum-thread', 'DiscussionController@deleteForumThread')->name('deleteForumThread');


    Route::get('admin/category/create', 'CategoriesController@create')->name('createCategory');
    Route::post('admin/category/store', 'CategoriesController@store')->name('saveCategory');
    Route::get('admin/category/index', 'CategoriesController@index')->name('listCategories');

});
