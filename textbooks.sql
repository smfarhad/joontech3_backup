-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 18, 2018 at 02:52 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 5.6.34-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `textbooks`
--

-- --------------------------------------------------------

--
-- Table structure for table `allowed_emails`
--

CREATE TABLE `allowed_emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `image`, `created_by`, `updated_by`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(29, 'svdvs', '1523448954-jpg', 1, 1, 1, 'svdvs-1', '2018-04-11 06:15:54', '2018-04-15 15:16:55'),
(30, 'new book', '1523911178-jpg', 1, NULL, 1, 'books', '2018-04-16 14:39:38', '2018-04-16 14:39:38');

-- --------------------------------------------------------

--
-- Table structure for table `book_chapters`
--

CREATE TABLE `book_chapters` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `books_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_chapters`
--

INSERT INTO `book_chapters` (`id`, `title`, `books_id`, `sort`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(45, 'Chapter 1', 29, 1, 1, 'chapter-1', '2018-04-11 06:15:54', '2018-04-15 15:16:56'),
(46, 'Chapter 2', 29, 2, 1, 'chapter-2', '2018-04-11 06:15:54', '2018-04-15 15:16:56'),
(47, 'Chapter 1', 30, 1, 1, 'chapter-1-1', '2018-04-16 14:39:38', '2018-04-16 14:39:38'),
(48, 'Chapter 2', 30, 2, 1, 'chapter-2-1', '2018-04-16 14:39:38', '2018-04-16 14:39:38');

-- --------------------------------------------------------

--
-- Table structure for table `book_sections`
--

CREATE TABLE `book_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `books_id` int(11) NOT NULL,
  `chapters_id` int(11) NOT NULL,
  `sort` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_sections`
--

INSERT INTO `book_sections` (`id`, `title`, `books_id`, `chapters_id`, `sort`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(15, 'Section 1.1', 29, 45, '1', 1, 'section-1-1', '2018-04-11 06:15:54', '2018-04-15 15:16:56'),
(16, 'section 2.1', 29, 46, '1', 0, 'section-2-1-1', '2018-04-12 01:56:44', '2018-04-15 15:16:56'),
(17, 'Section 1.2', 29, 45, '2', 0, 'section-1-2', '2018-04-15 05:42:31', '2018-04-15 15:16:56'),
(18, 'Section 1.1', 30, 47, '1', 1, 'section-1-1-1', '2018-04-16 14:39:38', '2018-04-16 14:39:38'),
(19, 'Section 2.1', 30, 48, '1', 1, 'section-2-1', '2018-04-16 14:39:38', '2018-04-16 14:39:38');

-- --------------------------------------------------------

--
-- Table structure for table `book_subsections`
--

CREATE TABLE `book_subsections` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `books_id` tinyint(4) NOT NULL,
  `sections_id` tinyint(4) NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci,
  `sort` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_subsections`
--

INSERT INTO `book_subsections` (`id`, `title`, `books_id`, `sections_id`, `data`, `sort`, `created_by`, `updated_by`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(22, 'Sub-section 1.1.1', 29, 15, NULL, 1, 1, 1, 1, 'sub-section-1-1-1', '2018-04-11 06:15:54', '2018-04-15 15:16:56'),
(23, 'subsection 2.1.1', 29, 16, '<p>sdcsdc</p>', 2, 1, 1, 0, 'subsection-2-1-1-1', '2018-04-12 01:56:44', '2018-04-15 15:16:56'),
(24, 'sucsecion 2.1.2', 29, 16, '<p>zdcsc</p>', 3, 1, 1, 0, 'sucsecion-2-1-2-1', '2018-04-12 01:56:44', '2018-04-15 15:16:56'),
(25, 'sdc', 29, 16, '<p>adcsadcsdc</p>', 1, 1, 1, 1, 'sdc', '2018-04-15 14:02:51', '2018-04-15 15:16:56'),
(26, 'New subSection', 29, 17, NULL, 1, 1, NULL, 0, 'new-subsection', '2018-04-15 15:16:56', '2018-04-15 15:16:56'),
(27, 'New subSection', 29, 17, NULL, 2, 1, NULL, 0, 'new-subsection-1', '2018-04-15 15:16:56', '2018-04-15 15:16:56'),
(28, 'Sub-section 1.1.1', 30, 18, NULL, 1, 1, NULL, 1, 'sub-section-1-1-1-1', '2018-04-16 14:39:38', '2018-04-16 14:39:38'),
(29, 'Sub-section 1.1.2', 30, 18, NULL, 2, 1, NULL, 1, 'sub-section-1-1-2', '2018-04-16 14:39:38', '2018-04-16 14:39:38'),
(30, 'Sub-section 2.1.1', 30, 19, NULL, 1, 1, NULL, 1, 'sub-section-2-1-1', '2018-04-16 14:39:38', '2018-04-16 14:39:38');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `is_public` int(11) NOT NULL,
  `is_forum` int(11) NOT NULL,
  `is_sticky` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `forum_categories`
--

CREATE TABLE `forum_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_public` int(11) NOT NULL DEFAULT '0',
  `is_forum` int(11) NOT NULL DEFAULT '0',
  `weight` int(11) NOT NULL DEFAULT '0',
  `enable_threads` tinyint(1) NOT NULL DEFAULT '0',
  `thread_count` int(11) NOT NULL DEFAULT '0',
  `post_count` int(11) NOT NULL DEFAULT '0',
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `forum_categories`
--

INSERT INTO `forum_categories` (`id`, `category_id`, `title`, `description`, `slug`, `image`, `is_public`, `is_forum`, `weight`, `enable_threads`, `thread_count`, `post_count`, `private`, `created_at`, `updated_at`, `is_deleted`) VALUES
(10, 0, 'Random Category top1', NULL, 'random-category-top1-1', '', 1, 0, 0, 0, 0, 0, 0, '2018-04-18 06:13:32', '2018-04-18 08:44:32', 0),
(11, 10, 'Random Category1', NULL, 'random-category1-1', '', 1, 0, 0, 0, 0, 0, 0, '2018-04-18 06:13:32', '2018-04-18 08:44:33', 0),
(12, 11, 'Random Forum1', NULL, 'random-forum1', '', 1, 1, 0, 1, 0, 0, 0, '2018-04-18 06:13:32', '2018-04-18 08:44:33', 0),
(13, 0, 'Random Category top', NULL, 'random-category-top', '', 1, 0, 0, 0, 0, 0, 0, '2018-04-18 06:13:32', '2018-04-18 08:34:35', 1),
(14, 13, 'Random Category', NULL, 'random-category', '', 1, 0, 0, 0, 0, 0, 0, '2018-04-18 06:13:32', '2018-04-18 08:35:54', 1),
(15, 14, 'Random Category', NULL, 'random-category-1', '', 1, 0, 0, 0, 0, 0, 0, '2018-04-18 06:13:32', '2018-04-18 08:35:54', 1),
(16, 15, 'Random Forum', NULL, 'random-forum', '', 1, 1, 0, 1, 0, 0, 0, '2018-04-18 06:13:32', '2018-04-18 08:35:54', 1),
(17, 15, 'Random Forum', NULL, 'random-forum-1', '', 1, 1, 0, 1, 0, 0, 0, '2018-04-18 06:13:33', '2018-04-18 08:35:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `forum_posts`
--

CREATE TABLE `forum_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `thread_id` int(10) UNSIGNED NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` int(10) UNSIGNED DEFAULT NULL,
  `sequence` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `forum_threads`
--

CREATE TABLE `forum_threads` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinned` tinyint(1) DEFAULT '0',
  `locked` tinyint(1) DEFAULT '0',
  `reply_count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `forum_threads_read`
--

CREATE TABLE `forum_threads_read` (
  `thread_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_05_19_151759_create_forum_table_categories', 1),
(2, '2014_05_19_152425_create_forum_table_threads', 1),
(3, '2014_05_19_152611_create_forum_table_posts', 1),
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2015_04_14_180344_create_forum_table_threads_read', 1),
(7, '2015_07_22_181406_update_forum_table_categories', 1),
(8, '2015_07_22_181409_update_forum_table_threads', 1),
(9, '2015_07_22_181417_update_forum_table_posts', 1),
(10, '2016_05_24_114302_add_defaults_to_forum_table_threads_columns', 1),
(11, '2016_07_09_111441_add_counts_to_categories_table', 1),
(12, '2016_07_09_122706_add_counts_to_threads_table', 1),
(13, '2016_07_10_134700_add_sequence_to_posts_table', 1),
(14, '2017_11_23_125100_user_level', 1),
(15, '2017_11_24_073054_create_book_subsections_table', 1),
(16, '2017_11_25_161231_create_books_table', 1),
(17, '2017_11_25_161512_create_book_chapters_table', 1),
(18, '2017_11_25_161743_create_book_sections_table', 1),
(19, '2017_12_06_052146_create_replies_table', 1),
(20, '2017_12_06_235733_create_topics_table', 1),
(21, '2017_12_11_044515_UpdateBookChapterWithPermalink', 1),
(22, '2017_12_11_050753_UpdateBookSectionWithPermalink', 1),
(23, '2017_12_11_050939_UpdateBookSubSectionWithPermalink', 1),
(24, '2017_12_11_051449_UpdateTopicsWithPermalink', 1),
(25, '2017_12_11_051512_UpdateRepliesWithPermalink', 1),
(26, '2017_12_13_144627_UpdateBookInfo', 1),
(27, '2017_12_31_004719_UpdateUserWithStatus', 1),
(28, '2018_01_02_143417_create_registration_tokens_table', 1),
(29, '2018_01_02_145319_UpdateUserWithRegistrationKey', 1),
(30, '2018_02_15_125630_update_user_with_token', 1),
(31, '2018_02_19_071808_create_allowed_emails_table', 1),
(32, '2018_03_03_170645_create_categories_table', 1),
(33, '2018_03_07_061023_update_categories_table_with_image', 1),
(34, '2018_03_07_084128_update_forum_category_with_image', 1),
(35, '2018_03_08_082606_update_forum_categories_with_slug', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `registration_tokens`
--

CREATE TABLE `registration_tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(62) COLLATE utf8mb4_unicode_ci NOT NULL,
  `limit` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

CREATE TABLE `replies` (
  `id` int(10) UNSIGNED NOT NULL,
  `topics_id` int(11) NOT NULL,
  `reply` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `id` int(10) UNSIGNED NOT NULL,
  `subsections_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(65) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reg_token_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `token`, `level`, `status`, `remember_token`, `reg_token_id`, `created_at`, `updated_at`) VALUES
(1, 'Super', 'Admin', 'super@user.com', '$2y$10$4bjIBIkg6DNGNKa1xfar6OPNHJwQ8au8bNoF1xPzMUXNBMivEl51u', '', 1, 0, NULL, '', NULL, NULL),
(2, 'Admin', 'user', 'admin@user.com', '$2y$10$R5XdEX9I3Io5GCGbwxHzre1UBwax.0/yXqzJr/wnfYjpvrWTeRlpq', '', 2, 0, NULL, '', NULL, NULL),
(3, 'Author', 'user', 'author@user.com', '$2y$10$0Dpicu251qkap1GdKmu5UOR7SqsvRomiTCdwwZiidcufYe7H7Dcri', '', 3, 0, NULL, '', NULL, NULL),
(4, 'User', 'user', 'user@user.com', '$2y$10$UH.t/ZG9ItTN4apoqrvUxeWw/jbj5CiYkCPEZjNBcIO4hW5AfmQeK', '', 3, 0, NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_levels`
--

CREATE TABLE `user_levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_levels`
--

INSERT INTO `user_levels` (`id`, `name`) VALUES
(1, 'Super Admin'),
(2, 'Admin'),
(3, 'Author'),
(4, 'User');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allowed_emails`
--
ALTER TABLE `allowed_emails`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `allowed_emails_email_unique` (`email`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_chapters`
--
ALTER TABLE `book_chapters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_sections`
--
ALTER TABLE `book_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_subsections`
--
ALTER TABLE `book_subsections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forum_categories`
--
ALTER TABLE `forum_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forum_posts`
--
ALTER TABLE `forum_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forum_threads`
--
ALTER TABLE `forum_threads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `registration_tokens`
--
ALTER TABLE `registration_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `replies`
--
ALTER TABLE `replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_levels`
--
ALTER TABLE `user_levels`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allowed_emails`
--
ALTER TABLE `allowed_emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `book_chapters`
--
ALTER TABLE `book_chapters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `book_sections`
--
ALTER TABLE `book_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `book_subsections`
--
ALTER TABLE `book_subsections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forum_categories`
--
ALTER TABLE `forum_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `forum_posts`
--
ALTER TABLE `forum_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forum_threads`
--
ALTER TABLE `forum_threads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `registration_tokens`
--
ALTER TABLE `registration_tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `replies`
--
ALTER TABLE `replies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_levels`
--
ALTER TABLE `user_levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
